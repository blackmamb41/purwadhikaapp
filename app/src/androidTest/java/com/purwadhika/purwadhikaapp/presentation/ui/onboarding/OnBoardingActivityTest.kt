package com.purwadhika.purwadhikaapp.presentation.ui.onboarding

import androidx.test.core.app.ActivityScenario
import androidx.test.espresso.Espresso
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.assertion.ViewAssertions
import androidx.test.espresso.matcher.ViewMatchers
import com.purwadhika.purwadhikaapp.R
import org.junit.Before
import org.junit.Test

class OnBoardingActivityTest {
    @Before
    fun setup() {
        ActivityScenario.launch(OnBoardingActivity::class.java)
    }

    @Test
    fun test_login_btn() {
        Espresso.onView(ViewMatchers.withId(R.id.btn_sign_in))
            .perform(ViewActions.click())
        Espresso.onView(ViewMatchers.withId(R.id.login_toolbar))
            .check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
    }

    @Test
    fun test_register_btn() {
        Espresso.onView(ViewMatchers.withId(R.id.btn_register))
            .perform(ViewActions.click())
        Espresso.onView(ViewMatchers.withId(R.id.toolbar_register_info))
            .check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
    }
}