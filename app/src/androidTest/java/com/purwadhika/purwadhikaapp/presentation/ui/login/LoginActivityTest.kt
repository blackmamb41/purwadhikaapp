package com.purwadhika.purwadhikaapp.presentation.ui.login

import androidx.test.core.app.ActivityScenario
import androidx.test.espresso.Espresso
import androidx.test.espresso.IdlingRegistry
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.assertion.ViewAssertions
import androidx.test.espresso.matcher.ViewMatchers
import com.purwadhika.purwadhikaapp.R
import com.purwadhika.purwadhikaapp.utils.EspressoIdlingResource
import org.junit.After
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test

class LoginActivityTest {
    @Before
    fun setup() {
        ActivityScenario.launch(LoginActivity::class.java)
        IdlingRegistry.getInstance().register(EspressoIdlingResource.idlingResource)
    }

    @After
    fun tearDown() {
        IdlingRegistry.getInstance().unregister(EspressoIdlingResource.idlingResource)
    }

    @Test
    fun login_success() {
        Espresso.onView(ViewMatchers.withId(R.id.email_form))
            .perform(ViewActions.typeText("halo@dunia.com"))
        Espresso.onView(ViewMatchers.withId(R.id.pass_form))
            .perform(ViewActions.typeText("abc123"))
        Espresso.onView(ViewMatchers.withId(R.id.btn_sign_in))
            .perform(ViewActions.click())
        Espresso.onView(ViewMatchers.withId(R.id.main_toolbar))
            .check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
    }

    @Test
    fun login_error() {
        Espresso.onView(ViewMatchers.withId(R.id.email_form))
            .perform(ViewActions.typeText("email@email.com"), ViewActions.closeSoftKeyboard())
        Espresso.onView(ViewMatchers.withId(R.id.pass_form))
            .perform(ViewActions.typeText(""), ViewActions.closeSoftKeyboard())
        Espresso.onView(ViewMatchers.withId(R.id.btn_sign_in))
            .perform(ViewActions.click())
        Espresso.closeSoftKeyboard()
        Espresso.onView(ViewMatchers.withId(com.google.android.material.R.id.snackbar_text))
            .check(ViewAssertions.matches(ViewMatchers.isCompletelyDisplayed()))
    }

    @Test
    fun forgot_password_click() {
        Espresso.onView(ViewMatchers.withText(R.string.forgot_your_password))
            .perform(ViewActions.click())
        Espresso.onView(ViewMatchers.withId(R.id.toolbar2))
            .check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
    }
}