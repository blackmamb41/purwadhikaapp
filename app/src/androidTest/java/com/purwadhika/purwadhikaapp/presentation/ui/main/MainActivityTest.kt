package com.purwadhika.purwadhikaapp.presentation.ui.main

import androidx.navigation.testing.TestNavHostController
import androidx.recyclerview.widget.RecyclerView
import androidx.test.core.app.ActivityScenario
import androidx.test.core.app.ApplicationProvider
import androidx.test.espresso.Espresso
import androidx.test.espresso.IdlingRegistry
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.assertion.ViewAssertions
import androidx.test.espresso.contrib.NavigationViewActions
import androidx.test.espresso.contrib.RecyclerViewActions
import androidx.test.espresso.matcher.ViewMatchers
import com.google.android.material.internal.NavigationMenuItemView
import com.google.android.material.internal.NavigationMenuView
import com.google.android.material.navigation.NavigationView
import com.purwadhika.purwadhikaapp.R
import com.purwadhika.purwadhikaapp.utils.EspressoIdlingResource
import org.junit.After
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test

class MainActivityTest {
    private lateinit var navController: TestNavHostController
    @Before
    fun setup() {
        ActivityScenario.launch(MainActivity::class.java)
        IdlingRegistry.getInstance().register(EspressoIdlingResource.idlingResource)
        navController = TestNavHostController(ApplicationProvider.getApplicationContext())
    }

    @After
    fun tearDown() {
        IdlingRegistry.getInstance().unregister(EspressoIdlingResource.idlingResource)
    }

    @Test
    fun test_bottom_navigation() {
        Espresso.onView(ViewMatchers.withContentDescription(R.string.home))
            .perform(ViewActions.click())
        navController.currentDestination?.id?.let {
            Espresso.onView(ViewMatchers.withId(it)).check(ViewAssertions.matches(ViewMatchers.withId(R.id.homeFragment)))
        }
        Espresso.onView(ViewMatchers.withContentDescription(R.string.profile))
            .perform(ViewActions.click())
        navController.currentDestination?.id?.let {
            Espresso.onView(ViewMatchers.withId(it)).check(ViewAssertions.matches(ViewMatchers.withId(R.id.profileFragment)))
        }
    }

    @Test
    fun test_home_header_nav() {
        Espresso.onView(ViewMatchers.withContentDescription(R.string.profile_header_menu))
            .perform(ViewActions.click())
        navController.currentDestination?.id?.let {
            Espresso.onView(ViewMatchers.withId(it)).check(ViewAssertions.matches(ViewMatchers.withId(R.id.profileFragment)))
        }
        Espresso.pressBack()
        Espresso.onView(ViewMatchers.withContentDescription(R.string.notification))
            .perform(ViewActions.click())
        navController.currentDestination?.id?.let {
            Espresso.onView(ViewMatchers.withId(it)).check(ViewAssertions.matches(ViewMatchers.withId(R.id.notificationFragment)))
        }
    }

    @Test
    fun test_topic_item_click() {
        Espresso.onView(ViewMatchers.withId(R.id.rv_topics))
            .perform(RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(0, ViewActions.click()))
    }

    @Test
    fun test_program_item_click() {
        Espresso.onView(ViewMatchers.withId(R.id.rv_program))
            .perform(RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(0, ViewActions.click()))
    }

    @Test
    fun test_go_to_qna() {
        Espresso.onView(ViewMatchers.withId(R.id.qna))
            .perform(ViewActions.click())
        Espresso.onView(ViewMatchers.withId(R.id.toolbar_qna))
            .check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
    }
}