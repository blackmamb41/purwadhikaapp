package com.purwadhika.purwadhikaapp.presentation.ui.profile

import androidx.test.core.app.ActivityScenario
import androidx.test.espresso.Espresso
import androidx.test.espresso.IdlingRegistry
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.assertion.ViewAssertions
import androidx.test.espresso.matcher.ViewMatchers
import com.purwadhika.purwadhikaapp.R
import com.purwadhika.purwadhikaapp.utils.EspressoIdlingResource
import org.junit.After
import org.junit.Before
import org.junit.Test

class ProfileActivityTest {
    @Before
    fun setup() {
        ActivityScenario.launch(ProfileActivity::class.java)
        IdlingRegistry.getInstance().register(EspressoIdlingResource.idlingResource)
    }

    @After
    fun tearDown() {
        IdlingRegistry.getInstance().unregister(EspressoIdlingResource.idlingResource)
    }

    @Test
    fun test_click_function_on_profile() {
        // Test image profile isDisplayed ?
        Espresso.onView(ViewMatchers.withId(R.id.image_profile))
            .check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
        // Test OnClick All Program
        Espresso.onView(ViewMatchers.withId(R.id.text_semua_program))
            .perform(ViewActions.click())
        Espresso.onView(ViewMatchers.withText("Your Programs"))
            .check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
        Espresso.pressBack()

        //Test Onlick BirthDay Dialog
        Espresso.onView(ViewMatchers.withId(R.id.form_birthday))
            .perform(ViewActions.click())
        Espresso.onView(ViewMatchers.withId(R.id.calendar_body))
            .check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
        Espresso.pressBack()

        // Test OnClick Gender
        Espresso.onView(ViewMatchers.withId(R.id.form_gender))
            .perform(ViewActions.click())
        Espresso.onView(ViewMatchers.withText(R.string.info_gender))
            .check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
        Espresso.pressBack()
    }
}