package com.purwadhika.purwadhikaapp.presentation.ui.login

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import com.purwadhika.purwadhikaapp.data.models.response.LoginResponse
import com.purwadhika.purwadhikaapp.domain.repositories.AuthenticationRepository
import com.purwadhika.purwadhikaapp.domain.repositories.AuthenticationRepositoryImp
import com.purwadhika.purwadhikaapp.domain.services.AuthenticationServices
import com.purwadhika.purwadhikaapp.domain.usecases.LoginUseCase
import com.purwadhika.purwadhikaapp.domain.usecases.LoginUseCaseImp
import com.purwadhika.purwadhikaapp.utils.DummyData
import junit.framework.TestCase
import org.junit.Assert.*
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class LoginVMTest: TestCase() {
    @get:Rule val instantTaskExecuteRule = InstantTaskExecutorRule()

    @Mock
    lateinit var loginVM: LoginVM

    @Mock
    lateinit var authenticationRepository: AuthenticationRepository

    @Mock
    lateinit var observeDataLogin: Observer<LoginResponse>

    @Before
    fun setup() {
        val loginUseCase = LoginUseCaseImp(authenticationRepository)
        loginVM = LoginVM(loginUseCase)
    }

    @Test
    fun test_login() {
        val loginLiveData = MutableLiveData<LoginResponse>()
        loginLiveData.value = DummyData.loginDataResponse()

        Mockito.`when`(loginVM.getLoginLiveData()).thenReturn(loginLiveData)
        loginVM.doLogin(DummyData.loginRequestData())

        loginVM.getLoginLiveData().observeForever(observeDataLogin)
        Mockito.verify(observeDataLogin).onChanged(DummyData.loginDataResponse())
    }

}