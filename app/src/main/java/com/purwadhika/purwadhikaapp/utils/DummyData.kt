package com.purwadhika.purwadhikaapp.utils

import com.purwadhika.purwadhikaapp.data.models.request.LoginRequest
import com.purwadhika.purwadhikaapp.data.models.response.LoginResponse
import com.purwadhika.purwadhikaapp.data.models.response.Status
import com.purwadhika.purwadhikaapp.data.models.response.Token

object DummyData {
    fun loginDataResponse(): LoginResponse {
        return LoginResponse(
            Status.SUCCESS,
            null,
            Token(
                token = "",
                1
            )
        )
    }

    fun loginRequestData(): LoginRequest {
        return LoginRequest(
            email = "halo@dunia.com",
            password = "abc123"
        )
    }
}