package com.purwadhika.purwadhikaapp.utils

import androidx.test.espresso.idling.CountingIdlingResource

object EspressoIdlingResource {
    private const val RESOURCE = "GLOBAL"
    val idlingResource = CountingIdlingResource(RESOURCE)

    fun idling() {
        idlingResource.increment()
    }

    fun execute() {
        idlingResource.decrement()
    }
}