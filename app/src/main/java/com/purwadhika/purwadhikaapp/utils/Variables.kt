package com.purwadhika.purwadhikaapp.utils

object Variables {
    const val REGISTER = "REGISTER"
    const val FORGOT_PASSWORD = "FORGOT_PASSWORD"
    const val FIRST_NAME = "FIRST_NAME"
    const val LAST_NAME = "LAST_NAME"
    const val EMAIL = "EMAIL"
    const val PHONE_NUMBER = "PHONE_NUMBER"
    const val SUCCESS = "SUCCESS"
    const val LOADING = "LOADING"
    const val ERROR = "ERROR"
    const val PASSWORD = "PASSWORD"
    val MONTH: List<String> = listOf(
        "January",
        "February",
        "March",
        "April",
        "May",
        "June",
        "July",
        "August",
        "September",
        "October",
        "November",
        "December"
    )
    const val LOCATION_REQUEST = 99
    const val PROGRAM_TOPIC = "PROGRAM_TOPIC"
    const val PROGRAM_CATEGORY_ID = "PROGRAM_CATEGORY_ID"
    const val PROGRAM_TOPIC_ID = "PROGRAM_TOPIC_ID"
    const val PROGRAM_DATA = "PROGRAM_DATA"
    const val IS_CATEGORY = "IS_CATEGORY"
    const val PROGRAM_CATEGORY_DATA = "PROGRAM_CATEGORY_DATA"
}