package com.purwadhika.purwadhikaapp.utils

import android.content.Context
import android.content.SharedPreferences

object Config {
    const val BASE_URL = "https://finalprojectapi.purwadhika.com"
    const val CLIENT_KEY = "PWDK"
    const val TOKEN = "token"
    const val ID_USER = "id"
    fun getToken(context: Context): String? {
        val prefs: SharedPreferences = context.getSharedPreferences(TOKEN, Context.MODE_PRIVATE)
        return prefs.getString(TOKEN, null)
    }
    fun getIdUser(context: Context): Int {
        val prefs: SharedPreferences = context.getSharedPreferences(TOKEN, Context.MODE_PRIVATE)
        return prefs.getInt(ID_USER, 0)
    }
    fun setToken(context: Context, token: String, id: Int) {
        val prefs: SharedPreferences = context.getSharedPreferences(TOKEN, Context.MODE_PRIVATE)
        prefs.edit().putString(TOKEN, token).apply()
        prefs.edit().putInt(ID_USER, id).apply()
    }
    fun deleteToken(context: Context) {
        val prefs: SharedPreferences = context.getSharedPreferences(TOKEN, Context.MODE_PRIVATE)
        prefs.edit().clear().apply()
    }
}