package com.purwadhika.purwadhikaapp

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class PurwadhikaApplication: Application()