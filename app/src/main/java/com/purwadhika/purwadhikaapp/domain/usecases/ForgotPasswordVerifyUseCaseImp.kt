package com.purwadhika.purwadhikaapp.domain.usecases

import com.purwadhika.purwadhikaapp.data.models.request.VerifyForgotPasswordRequest
import com.purwadhika.purwadhikaapp.data.models.response.VerifyResponse
import com.purwadhika.purwadhikaapp.domain.repositories.AuthenticationRepository
import retrofit2.Call
import javax.inject.Inject

class ForgotPasswordVerifyUseCaseImp @Inject constructor(var authenticationRepository: AuthenticationRepository): ForgetPasswordVerifyUseCase {
    override suspend fun verifyForgotPassword(verifyForgotPasswordRequest: VerifyForgotPasswordRequest): Call<VerifyResponse> = authenticationRepository.verifyForgotPassword(verifyForgotPasswordRequest)
}