package com.purwadhika.purwadhikaapp.domain.services

import com.purwadhika.purwadhikaapp.data.models.request.QnaRequest
import com.purwadhika.purwadhikaapp.data.models.response.QnaResponse
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.POST

interface QnaService {

    @POST("/question")
    fun createQuestion(@Body qnaRequest: QnaRequest): Call<QnaResponse>
}