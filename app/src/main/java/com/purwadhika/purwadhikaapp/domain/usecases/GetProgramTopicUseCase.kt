package com.purwadhika.purwadhikaapp.domain.usecases

import com.purwadhika.purwadhikaapp.data.models.response.ProgramTopicResponse
import retrofit2.Call

interface GetProgramTopicUseCase {
    suspend fun getListProgramTopic(): Call<ProgramTopicResponse>
}