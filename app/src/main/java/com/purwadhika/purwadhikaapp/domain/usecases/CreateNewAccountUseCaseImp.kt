package com.purwadhika.purwadhikaapp.domain.usecases

import com.purwadhika.purwadhikaapp.data.models.request.RegisterRequest
import com.purwadhika.purwadhikaapp.data.models.response.RegisterResponse
import com.purwadhika.purwadhikaapp.domain.repositories.AuthenticationRepository
import retrofit2.Call
import javax.inject.Inject

class CreateNewAccountUseCaseImp @Inject constructor(var authenticationRepository: AuthenticationRepository): CreateNewAccountUseCase {
    override suspend fun createAccount(registerRequest: RegisterRequest): Call<RegisterResponse> = authenticationRepository.createAccount(registerRequest)
}