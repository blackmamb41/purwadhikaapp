package com.purwadhika.purwadhikaapp.domain.repositories

import androidx.lifecycle.LiveData
import com.purwadhika.purwadhikaapp.data.models.response.*
import retrofit2.Call

interface ProgramRepository {
    fun getProgramData(): Call<ProgramResponse>
    fun getProgramCategory(): Call<ProgramCategoryResponse>
    fun getProgramTopic(): Call<ProgramTopicResponse>
    fun getProgramByFilter(programCategoryId: Int, programTopicId: Int): Call<ProgramResponse>
    fun getProgramsFromDB(): LiveData<List<Program>>
    fun getProgramCategoryFromDB(): LiveData<List<ProgramCategoryData>>
    fun getProgramTopicFromDB(): LiveData<List<ProgramTopicData>>
    suspend fun saveProgramToDB(program: Program)
    suspend fun saveCategoryProgramToDB(programCategoryData: ProgramCategoryData)
    suspend fun saveTopicProgramToDB(programTopicData: ProgramTopicData)
}