package com.purwadhika.purwadhikaapp.domain.usecases

import com.purwadhika.purwadhikaapp.data.models.response.Program

interface SaveProgramUseCase {
    suspend fun invokeInsertProgram(program: Program)
}