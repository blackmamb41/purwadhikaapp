package com.purwadhika.purwadhikaapp.domain.usecases

import com.purwadhika.purwadhikaapp.data.models.request.LoginRequest
import com.purwadhika.purwadhikaapp.data.models.response.LoginResponse
import retrofit2.Call

interface LoginUseCase {
    suspend fun invokeLogin(loginRequest: LoginRequest): Call<LoginResponse>
}