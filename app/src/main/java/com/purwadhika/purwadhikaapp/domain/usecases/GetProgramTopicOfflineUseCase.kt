package com.purwadhika.purwadhikaapp.domain.usecases

import androidx.lifecycle.LiveData
import com.purwadhika.purwadhikaapp.data.models.response.ProgramTopicData

interface GetProgramTopicOfflineUseCase {
    fun invokeGetProgramTopicOffline(): LiveData<List<ProgramTopicData>>
}