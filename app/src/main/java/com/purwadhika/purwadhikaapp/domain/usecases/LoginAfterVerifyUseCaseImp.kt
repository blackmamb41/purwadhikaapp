package com.purwadhika.purwadhikaapp.domain.usecases

import com.purwadhika.purwadhikaapp.data.models.request.LoginRequest
import com.purwadhika.purwadhikaapp.data.models.response.LoginResponse
import com.purwadhika.purwadhikaapp.domain.repositories.AuthenticationRepository
import retrofit2.Call
import javax.inject.Inject

class LoginAfterVerifyUseCaseImp @Inject constructor(var authenticationRepository: AuthenticationRepository): LoginAfterVerifyUseCase {
    override suspend fun invokeLoginAfterVerify(loginRequest: LoginRequest): Call<LoginResponse> = authenticationRepository.loginAccount(loginRequest)
}