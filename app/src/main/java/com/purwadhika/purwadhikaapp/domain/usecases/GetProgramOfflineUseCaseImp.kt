package com.purwadhika.purwadhikaapp.domain.usecases

import androidx.lifecycle.LiveData
import com.purwadhika.purwadhikaapp.data.models.response.Program
import com.purwadhika.purwadhikaapp.domain.repositories.ProgramRepository
import javax.inject.Inject

class GetProgramOfflineUseCaseImp @Inject constructor(var programRepository: ProgramRepository): GetProgramOfflineUseCase {
    override fun getPrograms(): LiveData<List<Program>> = programRepository.getProgramsFromDB()
}