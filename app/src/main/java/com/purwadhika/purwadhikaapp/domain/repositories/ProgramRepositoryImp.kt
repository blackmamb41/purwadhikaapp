package com.purwadhika.purwadhikaapp.domain.repositories

import androidx.lifecycle.LiveData
import com.purwadhika.purwadhikaapp.data.db.ProgramDao
import com.purwadhika.purwadhikaapp.data.models.response.*
import com.purwadhika.purwadhikaapp.domain.services.ProfileService
import com.purwadhika.purwadhikaapp.domain.services.ProgramService
import retrofit2.Call

class ProgramRepositoryImp(var programService: ProgramService, var programDao: ProgramDao): ProgramRepository {
    override fun getProgramData(): Call<ProgramResponse> = programService.getPrograms()
    override fun getProgramCategory(): Call<ProgramCategoryResponse> = programService.getProgramCategory()
    override fun getProgramTopic(): Call<ProgramTopicResponse> = programService.getProgramTopic()
    override fun getProgramByFilter(
        programCategoryId: Int,
        programTopicId: Int
    ): Call<ProgramResponse> = programService.getProgramByFilter(programCategoryId, programTopicId)
    override fun getProgramsFromDB(): LiveData<List<Program>> = programDao.getAllProgramsFromDB()
    override fun getProgramCategoryFromDB(): LiveData<List<ProgramCategoryData>> = programDao.getAllProgramCategoryFromDB()
    override fun getProgramTopicFromDB(): LiveData<List<ProgramTopicData>> = programDao.getAllProgramTopicFromDB()
    override suspend fun saveProgramToDB(program: Program) = programDao.saveProgramToDB(program)
    override suspend fun saveCategoryProgramToDB(programCategoryData: ProgramCategoryData) = programDao.saveCategoryToDB(programCategoryData)
    override suspend fun saveTopicProgramToDB(programTopicData: ProgramTopicData) = programDao.saveTopicToDB(programTopicData)
}