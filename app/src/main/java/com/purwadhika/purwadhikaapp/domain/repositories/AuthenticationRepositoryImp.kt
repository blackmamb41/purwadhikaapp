package com.purwadhika.purwadhikaapp.domain.repositories

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.purwadhika.purwadhikaapp.data.models.request.*
import com.purwadhika.purwadhikaapp.data.models.response.*
import com.purwadhika.purwadhikaapp.domain.services.AuthenticationServices
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class AuthenticationRepositoryImp(var authenticationServices: AuthenticationServices): AuthenticationRepository {
    override fun loginAccount(loginRequest: LoginRequest): Call<LoginResponse> = authenticationServices.doLogin(loginRequest)
    override fun createAccount(registerRequest: RegisterRequest): Call<RegisterResponse> = authenticationServices.doRegister(registerRequest)
    override fun verifyAccount(verifyRequest: VerifyRequest): Call<VerifyResponse> = authenticationServices.doVerify(verifyRequest)
    override fun forgotPassword(forgotPasswordRequest: ForgotPasswordRequest): Call<ForgetPasswordResponse> = authenticationServices.doForgotPassword(forgotPasswordRequest)
    override fun verifyForgotPassword(verifyForgotPasswordRequest: VerifyForgotPasswordRequest): Call<VerifyResponse> = authenticationServices.doVerifyForgotPassword(verifyForgotPasswordRequest)
}