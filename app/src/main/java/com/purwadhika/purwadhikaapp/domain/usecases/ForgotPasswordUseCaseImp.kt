package com.purwadhika.purwadhikaapp.domain.usecases

import com.purwadhika.purwadhikaapp.data.models.request.ForgotPasswordRequest
import com.purwadhika.purwadhikaapp.data.models.response.ForgetPasswordResponse
import com.purwadhika.purwadhikaapp.domain.repositories.AuthenticationRepository
import retrofit2.Call
import javax.inject.Inject

class ForgotPasswordUseCaseImp @Inject constructor(var authenticationRepository: AuthenticationRepository): ForgotPasswordUseCase {
    override suspend fun invokeForgotPassword(forgotPasswordRequest: ForgotPasswordRequest): Call<ForgetPasswordResponse> = authenticationRepository.forgotPassword(forgotPasswordRequest)
}