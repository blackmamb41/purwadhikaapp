package com.purwadhika.purwadhikaapp.domain.usecases

import com.purwadhika.purwadhikaapp.data.models.response.ProfileDetailResponse
import com.purwadhika.purwadhikaapp.data.models.response.ProgramResponse
import com.purwadhika.purwadhikaapp.domain.repositories.ProgramRepository
import retrofit2.Call
import javax.inject.Inject

class GetProgramUseCaseImp @Inject constructor(var programRepository: ProgramRepository): GetProgramsUseCase {
    override fun getProgramList(): Call<ProgramResponse> = programRepository.getProgramData()
}