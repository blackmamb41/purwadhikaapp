package com.purwadhika.purwadhikaapp.domain.repositories

import android.util.Log
import com.purwadhika.purwadhikaapp.data.models.request.ProfileRequest
import com.purwadhika.purwadhikaapp.data.models.response.ProfileDetailResponse
import com.purwadhika.purwadhikaapp.data.models.response.ProfileResponse
import com.purwadhika.purwadhikaapp.domain.services.ProfileService
import okhttp3.MediaType
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody
import okhttp3.RequestBody.Companion.asRequestBody
import okhttp3.RequestBody.Companion.toRequestBody
import retrofit2.Call
import retrofit2.http.Multipart
import java.io.File

class ProfileRepositoryImp(var profileService: ProfileService): ProfileRepository {
    override suspend fun updateProfile(profileRequest: ProfileRequest): Call<ProfileResponse> = profileService.updateProfile(profileRequest)
    override fun updateGenderProfile(id: Int, gender: String): Call<ProfileResponse> = profileService.updateGenderProfile(
        id,
        gender.toRequestBody("text/plain".toMediaTypeOrNull())
    )
    override fun updateBirthDateProfile(id: Int, birthDate: String): Call<ProfileResponse> = profileService.updateBirthDateProfile(
        id,
        birthDate.toRequestBody("text/plain".toMediaTypeOrNull())
    )
    override fun updateAddress(
        id: Int,
        address: String,
        addressLat: String,
        addressLon: String
    ): Call<ProfileResponse> = profileService.updateLocationProfile(
        id,
        address.toRequestBody("text/plain".toMediaTypeOrNull()),
        addressLat.toRequestBody("text/plain".toMediaTypeOrNull()),
        addressLon.toRequestBody("text/plain".toMediaTypeOrNull())
    )
    override fun getProfile(id: Int): Call<ProfileDetailResponse> = profileService.getProfileDetail(id)
    override fun updateAvatarProfile(id: Int, pictureProfile: RequestBody): Call<ProfileResponse> = profileService.updateAvatar(id, pictureProfile)
}