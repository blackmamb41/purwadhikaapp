package com.purwadhika.purwadhikaapp.domain.usecases

import com.purwadhika.purwadhikaapp.data.models.response.ProgramResponse
import com.purwadhika.purwadhikaapp.domain.repositories.ProgramRepository
import retrofit2.Call
import javax.inject.Inject

class GetProgramByFilterUseCaseImp @Inject constructor(var programRepository: ProgramRepository): GetProgramByFilterUseCase {
    override fun getListProgramFilter(
        programCategoryId: Int,
        programTopicId: Int
    ): Call<ProgramResponse> = programRepository.getProgramByFilter(programCategoryId, programTopicId)
}