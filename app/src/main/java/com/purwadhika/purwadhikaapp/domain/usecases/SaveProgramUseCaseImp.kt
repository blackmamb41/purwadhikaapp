package com.purwadhika.purwadhikaapp.domain.usecases

import com.purwadhika.purwadhikaapp.data.models.response.Program
import com.purwadhika.purwadhikaapp.domain.repositories.ProgramRepository
import javax.inject.Inject

class SaveProgramUseCaseImp @Inject constructor(var programRepository: ProgramRepository): SaveProgramUseCase {
    override suspend fun invokeInsertProgram(program: Program) = programRepository.saveProgramToDB(program)
}