package com.purwadhika.purwadhikaapp.domain.usecases

import com.purwadhika.purwadhikaapp.data.models.response.ProfileResponse
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import java.io.File

interface UpdateAvatarProfileUseCase {
    fun uploadAvatarImg(id: Int, pictureProfile: RequestBody): Call<ProfileResponse>
}