package com.purwadhika.purwadhikaapp.domain.usecases

import com.purwadhika.purwadhikaapp.data.models.response.ProgramTopicData

interface SaveTopicProgramUseCase {
    suspend fun invokeSaveTopicProgram(programTopicData: ProgramTopicData)
}