package com.purwadhika.purwadhikaapp.domain.usecases

import com.purwadhika.purwadhikaapp.data.models.request.ProfileRequest
import com.purwadhika.purwadhikaapp.data.models.response.ProfileResponse
import com.purwadhika.purwadhikaapp.domain.repositories.ProfileRepository
import retrofit2.Call
import javax.inject.Inject

class UpdateGenderUseCaseImp @Inject constructor(var profileRepository: ProfileRepository): UpdateGenderUseCase {
    override suspend fun updateGenderProfile(id: Int, gender: String): Call<ProfileResponse> = profileRepository.updateGenderProfile(id, gender)
}