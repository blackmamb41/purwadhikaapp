package com.purwadhika.purwadhikaapp.domain.usecases

import com.purwadhika.purwadhikaapp.data.models.response.ProgramResponse
import retrofit2.Call

interface GetProgramsUseCase {
    fun getProgramList(): Call<ProgramResponse>
}