package com.purwadhika.purwadhikaapp.domain.usecases

import com.purwadhika.purwadhikaapp.data.models.response.ProfileResponse
import com.purwadhika.purwadhikaapp.domain.repositories.ProfileRepository
import retrofit2.Call
import javax.inject.Inject

class UpdateMapLocationUseCaseImp @Inject constructor(var profileRepository: ProfileRepository): UpdateMapLocationUseCase {
    override suspend fun updateLocationProfile(
        id: Int,
        address: String,
        addressLat: String,
        addressLon: String
    ): Call<ProfileResponse> = profileRepository.updateAddress(id, address, addressLat, addressLon)
}