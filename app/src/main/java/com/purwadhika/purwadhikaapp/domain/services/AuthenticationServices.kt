package com.purwadhika.purwadhikaapp.domain.services

import com.purwadhika.purwadhikaapp.data.models.request.*
import com.purwadhika.purwadhikaapp.data.models.response.ForgetPasswordResponse
import com.purwadhika.purwadhikaapp.data.models.response.LoginResponse
import com.purwadhika.purwadhikaapp.data.models.response.RegisterResponse
import com.purwadhika.purwadhikaapp.data.models.response.VerifyResponse
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.POST

interface AuthenticationServices {
    @POST("/user/login")
    fun doLogin(
        @Body loginRequest: LoginRequest
    ): Call<LoginResponse>

    @POST("/user/register")
    fun doRegister(
        @Body registerRequest: RegisterRequest
    ): Call<RegisterResponse>

    @POST("/user/register/verify")
    fun doVerify(
        @Body verifyRequest: VerifyRequest
    ): Call<VerifyResponse>

    @POST("/user/forgot-password")
    fun doForgotPassword(
        @Body forgotPasswordRequest: ForgotPasswordRequest
    ): Call<ForgetPasswordResponse>

    @POST("/user/forgot-password/verify")
    fun doVerifyForgotPassword(
        @Body verifyForgotPasswordRequest: VerifyForgotPasswordRequest
    ): Call<VerifyResponse>
}