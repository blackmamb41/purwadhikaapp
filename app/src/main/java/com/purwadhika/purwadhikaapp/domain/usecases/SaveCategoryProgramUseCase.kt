package com.purwadhika.purwadhikaapp.domain.usecases

import com.purwadhika.purwadhikaapp.data.models.response.ProgramCategoryData

interface SaveCategoryProgramUseCase {
    suspend fun invokeSaveCategoryProgram(programCategoryData: ProgramCategoryData)
}