package com.purwadhika.purwadhikaapp.domain.usecases

import androidx.lifecycle.LiveData
import com.purwadhika.purwadhikaapp.data.models.response.Program

interface GetProgramOfflineUseCase {
    fun getPrograms(): LiveData<List<Program>>
}