package com.purwadhika.purwadhikaapp.domain.usecases

import androidx.lifecycle.LiveData
import com.purwadhika.purwadhikaapp.data.models.response.ProgramCategoryData

interface GetProgramCategoryOfflineUseCase {
    fun invokeGetProgramCategoryOffline(): LiveData<List<ProgramCategoryData>>
}