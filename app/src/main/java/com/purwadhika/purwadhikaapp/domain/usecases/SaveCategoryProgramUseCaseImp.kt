package com.purwadhika.purwadhikaapp.domain.usecases

import com.purwadhika.purwadhikaapp.data.models.response.ProgramCategoryData
import com.purwadhika.purwadhikaapp.domain.repositories.ProgramRepository

class SaveCategoryProgramUseCaseImp constructor(var programRepository: ProgramRepository): SaveCategoryProgramUseCase {
    override suspend fun invokeSaveCategoryProgram(programCategoryData: ProgramCategoryData) = programRepository.saveCategoryProgramToDB(programCategoryData)
}