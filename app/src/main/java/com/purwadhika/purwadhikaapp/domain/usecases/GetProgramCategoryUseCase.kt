package com.purwadhika.purwadhikaapp.domain.usecases

import com.purwadhika.purwadhikaapp.data.models.response.ProgramCategoryResponse
import retrofit2.Call

interface GetProgramCategoryUseCase {
    suspend fun getListProgramCategory(): Call<ProgramCategoryResponse>
}