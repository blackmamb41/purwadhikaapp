package com.purwadhika.purwadhikaapp.domain.usecases

import com.purwadhika.purwadhikaapp.data.models.request.LoginRequest
import com.purwadhika.purwadhikaapp.data.models.response.LoginResponse
import com.purwadhika.purwadhikaapp.domain.repositories.AuthenticationRepository
import retrofit2.Call
import javax.inject.Inject

class LoginUseCaseImp @Inject constructor(var authenticationRepository: AuthenticationRepository): LoginUseCase {
    override suspend fun invokeLogin(loginRequest: LoginRequest): Call<LoginResponse> = authenticationRepository.loginAccount(loginRequest)
}