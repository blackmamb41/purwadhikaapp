package com.purwadhika.purwadhikaapp.domain.usecases

import com.purwadhika.purwadhikaapp.data.models.request.ProfileRequest
import com.purwadhika.purwadhikaapp.data.models.response.ProfileResponse
import retrofit2.Call

interface UpdateGenderUseCase {
    suspend fun updateGenderProfile(id: Int, gender: String): Call<ProfileResponse>
}