package com.purwadhika.purwadhikaapp.domain.usecases

import com.purwadhika.purwadhikaapp.data.models.response.ProfileDetailResponse
import retrofit2.Call

interface GetProfileDetailUseCase {
    suspend fun invokeGetProfileDetail(id: Int): Call<ProfileDetailResponse>
}