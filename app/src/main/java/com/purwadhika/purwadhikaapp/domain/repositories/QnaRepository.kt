package com.purwadhika.purwadhikaapp.domain.repositories

import com.purwadhika.purwadhikaapp.data.models.request.QnaRequest
import com.purwadhika.purwadhikaapp.data.models.response.QnaResponse
import retrofit2.Call

interface QnaRepository {
    fun sendQna(qnaRequest: QnaRequest): Call<QnaResponse>
}