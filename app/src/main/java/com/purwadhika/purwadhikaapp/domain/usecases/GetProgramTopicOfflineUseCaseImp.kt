package com.purwadhika.purwadhikaapp.domain.usecases

import androidx.lifecycle.LiveData
import com.purwadhika.purwadhikaapp.data.models.response.ProgramTopicData
import com.purwadhika.purwadhikaapp.domain.repositories.ProgramRepository
import javax.inject.Inject

class GetProgramTopicOfflineUseCaseImp @Inject constructor(var programRepository: ProgramRepository): GetProgramTopicOfflineUseCase {
    override fun invokeGetProgramTopicOffline(): LiveData<List<ProgramTopicData>> = programRepository.getProgramTopicFromDB()
}