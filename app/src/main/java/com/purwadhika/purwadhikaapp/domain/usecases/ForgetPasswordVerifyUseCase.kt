package com.purwadhika.purwadhikaapp.domain.usecases

import com.purwadhika.purwadhikaapp.data.models.request.VerifyForgotPasswordRequest
import com.purwadhika.purwadhikaapp.data.models.response.VerifyResponse
import retrofit2.Call

interface ForgetPasswordVerifyUseCase {
    suspend fun verifyForgotPassword(verifyForgotPasswordRequest: VerifyForgotPasswordRequest): Call<VerifyResponse>
}