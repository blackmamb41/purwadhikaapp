package com.purwadhika.purwadhikaapp.domain.usecases

import com.purwadhika.purwadhikaapp.data.models.response.ProgramCategoryResponse
import com.purwadhika.purwadhikaapp.data.models.response.ProgramResponse
import com.purwadhika.purwadhikaapp.domain.repositories.ProgramRepository
import retrofit2.Call
import javax.inject.Inject

class GetProgramCategoryUseCaseImp @Inject constructor(var programRepository: ProgramRepository): GetProgramCategoryUseCase {
    override suspend fun getListProgramCategory(): Call<ProgramCategoryResponse> = programRepository.getProgramCategory()
}