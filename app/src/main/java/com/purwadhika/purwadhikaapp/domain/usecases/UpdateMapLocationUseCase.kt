package com.purwadhika.purwadhikaapp.domain.usecases

import com.purwadhika.purwadhikaapp.data.models.response.ProfileResponse
import retrofit2.Call

interface UpdateMapLocationUseCase {
    suspend fun updateLocationProfile(id: Int, address: String, addressLat: String, addressLon: String): Call<ProfileResponse>
}