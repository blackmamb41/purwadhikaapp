package com.purwadhika.purwadhikaapp.domain.usecases

import com.purwadhika.purwadhikaapp.data.models.response.ProgramResponse
import retrofit2.Call

interface GetProgramByFilterUseCase {
    fun getListProgramFilter(programCategoryId: Int, programTopicId: Int): Call<ProgramResponse>
}