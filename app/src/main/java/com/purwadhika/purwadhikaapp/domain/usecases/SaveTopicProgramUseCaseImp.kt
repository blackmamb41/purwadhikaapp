package com.purwadhika.purwadhikaapp.domain.usecases

import com.purwadhika.purwadhikaapp.data.models.response.ProgramTopicData
import com.purwadhika.purwadhikaapp.domain.repositories.ProgramRepository

class SaveTopicProgramUseCaseImp constructor(var programRepository: ProgramRepository): SaveTopicProgramUseCase {
    override suspend fun invokeSaveTopicProgram(programTopicData: ProgramTopicData) = programRepository.saveTopicProgramToDB(programTopicData)
}