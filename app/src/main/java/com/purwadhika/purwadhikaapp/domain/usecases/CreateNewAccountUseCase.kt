package com.purwadhika.purwadhikaapp.domain.usecases

import com.purwadhika.purwadhikaapp.data.models.request.RegisterRequest
import com.purwadhika.purwadhikaapp.data.models.response.RegisterResponse
import retrofit2.Call

interface CreateNewAccountUseCase {
    suspend fun createAccount(registerRequest: RegisterRequest): Call<RegisterResponse>
}