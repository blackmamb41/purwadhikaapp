package com.purwadhika.purwadhikaapp.domain.usecases

import com.purwadhika.purwadhikaapp.data.models.response.ProfileResponse
import retrofit2.Call

interface UpdateBirthDateUseCase {
    suspend fun updateBirthDate(id: Int, dateBirth: String): Call<ProfileResponse>
}