package com.purwadhika.purwadhikaapp.domain.services

import com.purwadhika.purwadhikaapp.data.models.response.ProgramCategoryResponse
import com.purwadhika.purwadhikaapp.data.models.response.ProgramResponse
import com.purwadhika.purwadhikaapp.data.models.response.ProgramTopicResponse
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface ProgramService {
    @GET("/program")
    fun getPrograms(): Call<ProgramResponse>

    @GET("/program-category")
    fun getProgramCategory(): Call<ProgramCategoryResponse>

    @GET("/program-topic")
    fun getProgramTopic(): Call<ProgramTopicResponse>

    @GET("/program")
    fun getProgramByFilter(
        @Query("programCategoryId") programCategoryId: Int,
        @Query("programTopicId") programTopicId: Int
    ): Call<ProgramResponse>
}