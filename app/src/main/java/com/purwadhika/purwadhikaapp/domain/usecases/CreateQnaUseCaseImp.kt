package com.purwadhika.purwadhikaapp.domain.usecases

import com.purwadhika.purwadhikaapp.data.models.request.QnaRequest
import com.purwadhika.purwadhikaapp.data.models.response.QnaResponse
import com.purwadhika.purwadhikaapp.domain.repositories.QnaRepository
import retrofit2.Call
import javax.inject.Inject

class CreateQnaUseCaseImp @Inject constructor(var qnaRepository: QnaRepository): CreateQnaUseCase {
    override fun createQna(qnaRequest: QnaRequest): Call<QnaResponse> = qnaRepository.sendQna(qnaRequest)
}