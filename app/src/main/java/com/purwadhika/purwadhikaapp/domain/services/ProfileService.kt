package com.purwadhika.purwadhikaapp.domain.services

import com.purwadhika.purwadhikaapp.data.models.request.ProfileRequest
import com.purwadhika.purwadhikaapp.data.models.response.ProfileDetailResponse
import com.purwadhika.purwadhikaapp.data.models.response.ProfileResponse
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.http.*
import java.io.File

interface ProfileService {

    @FormUrlEncoded
    @Multipart
    @POST("/user/profile/{id}")
    fun updateProfile(
        profileRequest: ProfileRequest
    ): Call<ProfileResponse>

    @Multipart
    @PUT("/user/profile/{id}")
    fun updateGenderProfile(
        @Path("id") id: Int,
        @Part("gender") gender: RequestBody
    ): Call<ProfileResponse>

    @Multipart
    @PUT("/user/profile/{id}")
    fun updateBirthDateProfile(
        @Path("id") id: Int,
        @Part("birthDate") birthDate: RequestBody
    ): Call<ProfileResponse>

    @Multipart
    @PUT("/user/profile/{id}")
    fun updateLocationProfile(
        @Path("id") id: Int,
        @Part("address") address: RequestBody,
        @Part("addressLat") addressLat: RequestBody,
        @Part("addressLon") addressLon: RequestBody
    ): Call<ProfileResponse>

    @Multipart
    @PUT("/user/profile/{id}")
    fun updateAvatar(
        @Path("id") id: Int,
        @Part("profilePicture") pictureProfile: RequestBody
    ): Call<ProfileResponse>

    @GET("/user/profile/{id}")
    fun getProfileDetail(
        @Path("id") id: Int
    ): Call<ProfileDetailResponse>
}