package com.purwadhika.purwadhikaapp.domain.repositories

import androidx.lifecycle.LiveData
import com.purwadhika.purwadhikaapp.data.models.request.*
import com.purwadhika.purwadhikaapp.data.models.response.ForgetPasswordResponse
import com.purwadhika.purwadhikaapp.data.models.response.LoginResponse
import com.purwadhika.purwadhikaapp.data.models.response.RegisterResponse
import com.purwadhika.purwadhikaapp.data.models.response.VerifyResponse
import retrofit2.Call

interface AuthenticationRepository {
    fun loginAccount(loginRequest: LoginRequest): Call<LoginResponse>
    fun createAccount(registerRequest: RegisterRequest): Call<RegisterResponse>
    fun verifyAccount(verifyRequest: VerifyRequest): Call<VerifyResponse>
    fun forgotPassword(forgotPasswordRequest: ForgotPasswordRequest): Call<ForgetPasswordResponse>
    fun verifyForgotPassword(verifyForgotPasswordRequest: VerifyForgotPasswordRequest): Call<VerifyResponse>
}