package com.purwadhika.purwadhikaapp.domain.usecases

import androidx.lifecycle.LiveData
import com.purwadhika.purwadhikaapp.data.models.response.ProgramCategoryData
import com.purwadhika.purwadhikaapp.domain.repositories.ProgramRepository
import javax.inject.Inject

class GetProgramCategoryOfflineUseCaseImp @Inject constructor(var programRepository: ProgramRepository): GetProgramCategoryOfflineUseCase {
    override fun invokeGetProgramCategoryOffline(): LiveData<List<ProgramCategoryData>> = programRepository.getProgramCategoryFromDB()
}