package com.purwadhika.purwadhikaapp.domain.usecases

import com.purwadhika.purwadhikaapp.data.models.response.ProfileResponse
import com.purwadhika.purwadhikaapp.domain.repositories.ProfileRepository
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import java.io.File
import javax.inject.Inject

class UpdateAvatarProfileUseCaseImp @Inject constructor(var profileRepository: ProfileRepository): UpdateAvatarProfileUseCase {
    override fun uploadAvatarImg(id: Int, pictureProfile: RequestBody): Call<ProfileResponse> = profileRepository.updateAvatarProfile(id, pictureProfile)
}