package com.purwadhika.purwadhikaapp.domain.usecases

import com.purwadhika.purwadhikaapp.data.models.request.QnaRequest
import com.purwadhika.purwadhikaapp.data.models.response.QnaResponse
import retrofit2.Call

interface CreateQnaUseCase {
    fun createQna(qnaRequest: QnaRequest): Call<QnaResponse>
}