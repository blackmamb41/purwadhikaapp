package com.purwadhika.purwadhikaapp.domain.repositories

import androidx.lifecycle.LiveData
import com.purwadhika.purwadhikaapp.data.models.request.ProfileRequest
import com.purwadhika.purwadhikaapp.data.models.response.ProfileDetailResponse
import com.purwadhika.purwadhikaapp.data.models.response.ProfileResponse
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import java.io.File

interface ProfileRepository {
    suspend fun updateProfile(profileRequest: ProfileRequest): Call<ProfileResponse>
    fun updateGenderProfile(id: Int, gender: String): Call<ProfileResponse>
    fun updateBirthDateProfile(id: Int, birthDate: String): Call<ProfileResponse>
    fun updateAddress(id: Int, address: String, addressLat: String, addressLon: String): Call<ProfileResponse>
    fun getProfile(id: Int): Call<ProfileDetailResponse>
    fun updateAvatarProfile(id: Int, pictureProfile: RequestBody): Call<ProfileResponse>
}