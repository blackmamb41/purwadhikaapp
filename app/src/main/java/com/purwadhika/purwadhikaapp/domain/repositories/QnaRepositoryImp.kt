package com.purwadhika.purwadhikaapp.domain.repositories

import com.purwadhika.purwadhikaapp.data.models.request.QnaRequest
import com.purwadhika.purwadhikaapp.data.models.response.QnaResponse
import com.purwadhika.purwadhikaapp.domain.services.QnaService
import retrofit2.Call

class QnaRepositoryImp(var qnaService: QnaService): QnaRepository {
    override fun sendQna(qnaRequest: QnaRequest): Call<QnaResponse> = qnaService.createQuestion(qnaRequest)
}