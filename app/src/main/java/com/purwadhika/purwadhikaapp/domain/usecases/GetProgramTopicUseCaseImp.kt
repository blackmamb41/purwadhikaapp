package com.purwadhika.purwadhikaapp.domain.usecases

import com.purwadhika.purwadhikaapp.data.models.response.ProgramTopicResponse
import com.purwadhika.purwadhikaapp.domain.repositories.ProgramRepository
import retrofit2.Call
import javax.inject.Inject

class GetProgramTopicUseCaseImp @Inject constructor(var programRepository: ProgramRepository): GetProgramTopicUseCase {
    override suspend fun getListProgramTopic(): Call<ProgramTopicResponse> = programRepository.getProgramTopic()
}