package com.purwadhika.purwadhikaapp.domain.usecases

import com.purwadhika.purwadhikaapp.data.models.response.ProfileResponse
import com.purwadhika.purwadhikaapp.domain.repositories.ProfileRepository
import retrofit2.Call
import javax.inject.Inject

class UpdateBirtDateUseCaseImp @Inject constructor(var profileRepository: ProfileRepository): UpdateBirthDateUseCase {
    override suspend fun updateBirthDate(id: Int, dateBirth: String): Call<ProfileResponse> = profileRepository.updateBirthDateProfile(id, dateBirth)
}