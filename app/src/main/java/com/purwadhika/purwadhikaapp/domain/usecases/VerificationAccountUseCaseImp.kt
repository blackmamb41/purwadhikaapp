package com.purwadhika.purwadhikaapp.domain.usecases

import com.purwadhika.purwadhikaapp.data.models.request.VerifyRequest
import com.purwadhika.purwadhikaapp.data.models.response.VerifyResponse
import com.purwadhika.purwadhikaapp.domain.repositories.AuthenticationRepository
import retrofit2.Call
import javax.inject.Inject

class VerificationAccountUseCaseImp @Inject constructor(var authenticationRepository: AuthenticationRepository): VerificationAccountUseCase {
    override suspend fun invokeVerification(verifyRequest: VerifyRequest): Call<VerifyResponse> = authenticationRepository.verifyAccount(verifyRequest)
}