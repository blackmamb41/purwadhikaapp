package com.purwadhika.purwadhikaapp.domain.usecases

import com.purwadhika.purwadhikaapp.data.models.request.VerifyRequest
import com.purwadhika.purwadhikaapp.data.models.response.VerifyResponse
import retrofit2.Call

interface VerificationAccountUseCase {
    suspend fun invokeVerification(verifyRequest: VerifyRequest): Call<VerifyResponse>
}