package com.purwadhika.purwadhikaapp.domain.usecases

import com.purwadhika.purwadhikaapp.data.models.request.ForgotPasswordRequest
import com.purwadhika.purwadhikaapp.data.models.response.ForgetPasswordResponse
import retrofit2.Call

interface ForgotPasswordUseCase {
    suspend fun invokeForgotPassword(forgotPasswordRequest: ForgotPasswordRequest): Call<ForgetPasswordResponse>
}