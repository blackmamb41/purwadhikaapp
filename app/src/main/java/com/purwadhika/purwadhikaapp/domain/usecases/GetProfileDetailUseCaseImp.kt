package com.purwadhika.purwadhikaapp.domain.usecases

import com.purwadhika.purwadhikaapp.data.models.response.ProfileDetailResponse
import com.purwadhika.purwadhikaapp.domain.repositories.ProfileRepository
import retrofit2.Call
import javax.inject.Inject

class GetProfileDetailUseCaseImp @Inject constructor(var profileRepository: ProfileRepository): GetProfileDetailUseCase {
    override suspend fun invokeGetProfileDetail(id: Int): Call<ProfileDetailResponse> = profileRepository.getProfile(id)
}