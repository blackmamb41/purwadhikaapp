package com.purwadhika.purwadhikaapp.data.di

import android.content.Context
import com.purwadhika.purwadhikaapp.domain.repositories.ProfileRepository
import com.purwadhika.purwadhikaapp.domain.repositories.ProfileRepositoryImp
import com.purwadhika.purwadhikaapp.domain.services.ProfileService
import com.purwadhika.purwadhikaapp.domain.usecases.*
import com.purwadhika.purwadhikaapp.utils.Config
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

@Module
@InstallIn(SingletonComponent::class)
object ProfileModule {

    private val okHttpClient: OkHttpClient.Builder = OkHttpClient.Builder()
    private val interceptor: HttpLoggingInterceptor = HttpLoggingInterceptor()

    private fun interceptHttp(context: Context) {
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY)
        okHttpClient.addInterceptor(interceptor)
        okHttpClient.addInterceptor(Interceptor {
            val origin: Request = it.request()
            val request: Request = origin.newBuilder()
                .header("client-key", Config.CLIENT_KEY)
                .header("Authorization", "Bearer ${Config.getToken(context)}")
                .header("Accept-Encoding", "gzip, deflate, br")
                .header("Accept", "*/*")
                .header("Content-Type", "multipart/form-data")
                .build()
            return@Interceptor it.proceed(request)
        })
    }

    @Provides
    fun bindAPIService(@ApplicationContext context: Context): ProfileService {
        interceptHttp(context)
        val client: OkHttpClient = okHttpClient.build()
        val retrofit = Retrofit.Builder()
            .baseUrl(Config.BASE_URL)
            .client(client)
            .addConverterFactory(GsonConverterFactory.create())
            .build()

        return retrofit.create(ProfileService::class.java)
    }

    @Provides
    fun provideProfileRepository(profileService: ProfileService): ProfileRepository {
        return ProfileRepositoryImp(profileService)
    }

    @Provides
    fun provideGetProfileUseCase(profileRepository: ProfileRepository): GetProfileDetailUseCase {
        return GetProfileDetailUseCaseImp(profileRepository)
    }

    @Provides
    fun provideUpdateGenderProfileUseCase(profileRepository: ProfileRepository): UpdateGenderUseCase {
        return UpdateGenderUseCaseImp(profileRepository)
    }

    @Provides
    fun provideUpdateBirthDateUseCase(profileRepository: ProfileRepository): UpdateBirthDateUseCase {
        return UpdateBirtDateUseCaseImp(profileRepository)
    }

    @Provides
    fun provideUpdateMapLocationUseCase(profileRepository: ProfileRepository): UpdateMapLocationUseCase {
        return UpdateMapLocationUseCaseImp(profileRepository)
    }

    @Provides
    fun provideUpdateAvatarProfileUseCase(profileRepository: ProfileRepository): UpdateAvatarProfileUseCase {
        return UpdateAvatarProfileUseCaseImp(profileRepository)
    }
}