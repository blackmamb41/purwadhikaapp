package com.purwadhika.purwadhikaapp.data.models.response

data class RegisterResponse(
    var status: Status,
    var message: String?,
    var data: DataRegister?
)

data class DataRegister(
    var id: Int,
    var email: String,
    var firstName: String,
    var lastName: String,
    var phoneNumber: String
)
