package com.purwadhika.purwadhikaapp.data.models.response

data class ProfileResponse(
    var status: Status,
    var message: String?,
    var error: Map<String, String>?
)