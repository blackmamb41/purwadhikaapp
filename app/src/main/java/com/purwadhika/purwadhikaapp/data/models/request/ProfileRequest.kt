package com.purwadhika.purwadhikaapp.data.models.request

import okhttp3.RequestBody
import retrofit2.http.Field
import retrofit2.http.Part

data class ProfileRequest(
    @Field("firstName") var firstName: String?,
    @Field("lastname") var lastName: String?,
    @Field("password") var password: String?,
    @Field("gender") var gender: String?,
    @Field("birthPlace") var birthPlace: String?,
    @Field("birthDate") var birtDate: String?,
    @Field("address") var address: String?,
    @Field("addressLat") var addressLat: String?,
    @Field("addressLon") var addressLon: String,
    @Part("profilePicture") var profilePicture: RequestBody
)