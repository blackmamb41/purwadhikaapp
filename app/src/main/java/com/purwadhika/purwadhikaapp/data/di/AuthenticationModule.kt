package com.purwadhika.purwadhikaapp.data.di

import com.purwadhika.purwadhikaapp.utils.Config
import com.purwadhika.purwadhikaapp.domain.repositories.AuthenticationRepository
import com.purwadhika.purwadhikaapp.domain.repositories.AuthenticationRepositoryImp
import com.purwadhika.purwadhikaapp.domain.services.AuthenticationServices
import com.purwadhika.purwadhikaapp.domain.usecases.*
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object AuthenticationModule {

    private val okHttpClient: OkHttpClient.Builder = OkHttpClient.Builder()
    private val interceptor: HttpLoggingInterceptor = HttpLoggingInterceptor()

    private fun interceptHttp() {
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY)
        okHttpClient.addInterceptor(interceptor)
        okHttpClient.addInterceptor(Interceptor {
            val original: Request = it.request()
            val request: Request = original.newBuilder()
                .header("client-key", Config.CLIENT_KEY)
                .build()
            return@Interceptor it.proceed(request)
        })
    }

    @Provides // Sifat Nya Global, bikin submodule
    fun bindAuthenticationService(): AuthenticationServices {
        interceptHttp()
        val client: OkHttpClient = okHttpClient.build()
        val retrofit = Retrofit.Builder()
            .baseUrl(Config.BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .client(client)
            .build()

        return retrofit.create(AuthenticationServices::class.java)
    }

    // Repository Provider

    @Provides
    fun provideAuthenticationRepository(authenticationServices: AuthenticationServices): AuthenticationRepository{
        return AuthenticationRepositoryImp(authenticationServices)
    }

    // Use Case Provider

    @Provides
    fun provideCreateNewAccountUseCase(authenticationRepository: AuthenticationRepository): CreateNewAccountUseCase {
        return CreateNewAccountUseCaseImp(authenticationRepository)
    }

    @Provides
    fun provideVerificationAccountUseCase(authenticationRepository: AuthenticationRepository): VerificationAccountUseCase{
        return VerificationAccountUseCaseImp(authenticationRepository)
    }

    @Provides
    fun provideLoginAfterVerificationUseCase(authenticationRepository: AuthenticationRepository): LoginAfterVerifyUseCase {
        return LoginAfterVerifyUseCaseImp(authenticationRepository)
    }

    @Provides
    fun provideVerifyForgotPasswordUseCase(authenticationRepository: AuthenticationRepository): ForgetPasswordVerifyUseCase {
        return ForgotPasswordVerifyUseCaseImp(authenticationRepository)
    }

    @Provides
    fun provideForgotPasswordUseCase(authenticationRepository: AuthenticationRepository): ForgotPasswordUseCase {
        return ForgotPasswordUseCaseImp(authenticationRepository)
    }

    @Provides
    fun provideLoginUseCase(authenticationRepository: AuthenticationRepository): LoginUseCase {
        return LoginUseCaseImp(authenticationRepository)
    }
}