package com.purwadhika.purwadhikaapp.data.di

import android.content.Context
import com.purwadhika.purwadhikaapp.domain.repositories.QnaRepository
import com.purwadhika.purwadhikaapp.domain.repositories.QnaRepositoryImp
import com.purwadhika.purwadhikaapp.domain.services.QnaService
import com.purwadhika.purwadhikaapp.domain.usecases.CreateQnaUseCase
import com.purwadhika.purwadhikaapp.domain.usecases.CreateQnaUseCaseImp
import com.purwadhika.purwadhikaapp.utils.Config
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Request
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

@Module
@InstallIn(SingletonComponent::class)
object QnaModule {
    private val okHttpClient: OkHttpClient.Builder = OkHttpClient.Builder()

    private fun interceptHttp(context: Context) {
        okHttpClient.addInterceptor(Interceptor {
            val origin: Request = it.request()
            val request: Request = origin.newBuilder()
                .header("client-key", Config.CLIENT_KEY)
                .header("Authorization", "Bearer ${Config.getToken(context)}")
                .build()
            return@Interceptor it.proceed(request)
        })
    }

    @Provides
    fun provideBindAPIService(@ApplicationContext context: Context): QnaService {
        interceptHttp(context)
        val client: OkHttpClient = okHttpClient.build()
        val retrofit = Retrofit.Builder()
            .baseUrl(Config.BASE_URL)
            .client(client)
            .addConverterFactory(GsonConverterFactory.create())
            .build()

        return retrofit.create(QnaService::class.java)
    }

    @Provides
    fun provideQnaRepository(qnaService: QnaService): QnaRepository {
        return QnaRepositoryImp(qnaService)
    }

    @Provides
    fun provideCreateQnaUseCase(qnaRepository: QnaRepository): CreateQnaUseCase {
        return CreateQnaUseCaseImp(qnaRepository)
    }
}