package com.purwadhika.purwadhikaapp.data.models.response

data class ProfileDetailResponse(
    var status: Status,
    var message: String?,
    var data: ProfileData?
)

data class ProfileData(
    var id: Int,
    var firstName: String,
    var lastName: String,
    var email: String,
    var phoneNumber: String,
    var gender: String,
    var birthPlace: String,
    var birthDate: String,
    var address: String,
    var addressLat: Double,
    var addressLon: Double,
    var profilePicture: String
)
