package com.purwadhika.purwadhikaapp.data.models.request

data class ForgotPasswordRequest(
    var email: String
)
