package com.purwadhika.purwadhikaapp.data.db

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.purwadhika.purwadhikaapp.data.models.response.Program
import com.purwadhika.purwadhikaapp.data.models.response.ProgramCategoryData
import com.purwadhika.purwadhikaapp.data.models.response.ProgramTopicData

@Database(entities = [Program::class, ProgramTopicData::class, ProgramCategoryData::class], version = 1)
@TypeConverters
abstract class AppDatabase: RoomDatabase() {
    abstract fun getPrograms(): ProgramDao
}