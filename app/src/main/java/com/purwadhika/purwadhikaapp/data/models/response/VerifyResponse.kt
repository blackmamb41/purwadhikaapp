package com.purwadhika.purwadhikaapp.data.models.response

data class VerifyResponse(
    var status: Status,
    var message: String?
)