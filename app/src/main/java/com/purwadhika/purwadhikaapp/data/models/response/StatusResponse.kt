package com.purwadhika.purwadhikaapp.data.models.response

import com.purwadhika.purwadhikaapp.utils.Config

sealed class StatusResponse {
    data class SuccessRegister(
        var message: String,
        var data: DataRegister
    )

    data class ErrorRegister(
        var message: String,
        var error: Error
    )
}

data class Error(
    var message: String
)