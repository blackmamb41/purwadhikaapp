package com.purwadhika.purwadhikaapp.data.models.request

data class VerifyForgotPasswordRequest(
    var email: String,
    var verificationCode: String
)