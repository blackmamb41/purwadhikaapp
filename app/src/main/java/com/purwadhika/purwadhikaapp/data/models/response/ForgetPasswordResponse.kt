package com.purwadhika.purwadhikaapp.data.models.response

data class ForgetPasswordResponse(
    var status: Status,
    var message: String?,
    var error: ErrorMessage?
)

data class ErrorMessage(
    var message: String
)