package com.purwadhika.purwadhikaapp.data.models.response

enum class Status {
    SUCCESS,
    ERROR,
    LOADING
}