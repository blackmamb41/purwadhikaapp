package com.purwadhika.purwadhikaapp.data.models.request

data class LoginRequest(
    var email: String,
    var password: String
)
