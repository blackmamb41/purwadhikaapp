package com.purwadhika.purwadhikaapp.data.models.request

data class QnaRequest(
    var userId: Int,
    var question: String
)
