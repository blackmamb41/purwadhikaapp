package com.purwadhika.purwadhikaapp.data.models.request

data class RegisterRequest(
    var firstName: String,
    var lastName: String,
    var email: String,
    var password: String,
    var phoneNumber: String,
)