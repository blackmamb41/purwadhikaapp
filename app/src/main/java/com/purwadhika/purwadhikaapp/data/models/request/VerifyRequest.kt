package com.purwadhika.purwadhikaapp.data.models.request

data class VerifyRequest(
    var email: String,
    var verificationCode: String
)