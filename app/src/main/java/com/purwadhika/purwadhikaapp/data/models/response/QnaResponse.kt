package com.purwadhika.purwadhikaapp.data.models.response

data class QnaResponse(
    var status: Status,
    var message: String?,
    var data: DataQuestion?
)

data class DataQuestion(
    var id: Int,
    var question: String,
    var userId: Int
)
