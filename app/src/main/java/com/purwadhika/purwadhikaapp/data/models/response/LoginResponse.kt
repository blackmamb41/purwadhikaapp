package com.purwadhika.purwadhikaapp.data.models.response

data class LoginResponse(
    var status: Status,
    var message: String?,
    var data: Token?
)

data class Token(
    var token: String,
    var userId: Int
)
