package com.purwadhika.purwadhikaapp.data.db

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.purwadhika.purwadhikaapp.data.models.response.Program
import com.purwadhika.purwadhikaapp.data.models.response.ProgramCategoryData
import com.purwadhika.purwadhikaapp.data.models.response.ProgramTopicData

@Dao
interface ProgramDao {
    @Query("SELECT * FROM Program")
    fun getAllProgramsFromDB(): LiveData<List<Program>>

    @Query("SELECT * FROM Category")
    fun getAllProgramCategoryFromDB(): LiveData<List<ProgramCategoryData>>

    @Query("SELECT * FROM Topic")
    fun getAllProgramTopicFromDB(): LiveData<List<ProgramTopicData>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun saveProgramToDB(program: Program)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun saveTopicToDB(programTopicData: ProgramTopicData)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun saveCategoryToDB(programCategoryData: ProgramCategoryData)
}