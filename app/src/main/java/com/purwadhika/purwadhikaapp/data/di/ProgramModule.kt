package com.purwadhika.purwadhikaapp.data.di

import android.content.Context
import androidx.room.Room
import com.purwadhika.purwadhikaapp.data.db.AppDatabase
import com.purwadhika.purwadhikaapp.data.db.ProgramDao
import com.purwadhika.purwadhikaapp.data.models.response.ProgramCategoryData
import com.purwadhika.purwadhikaapp.domain.repositories.ProgramRepository
import com.purwadhika.purwadhikaapp.domain.repositories.ProgramRepositoryImp
import com.purwadhika.purwadhikaapp.domain.services.ProgramService
import com.purwadhika.purwadhikaapp.domain.usecases.*
import com.purwadhika.purwadhikaapp.utils.Config
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Request
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

@Module
@InstallIn(SingletonComponent::class)
object ProgramModule {
    private val okHttpClient: OkHttpClient.Builder = OkHttpClient.Builder()

    private fun interceptHttp(context: Context) {
        okHttpClient.addInterceptor(Interceptor {
            val origin: Request = it.request()
            val request: Request = origin.newBuilder()
                .header("client-key", Config.CLIENT_KEY)
                .header("Authorization", "Bearer ${Config.getToken(context)}")
                .build()
            return@Interceptor it.proceed(request)
        })
    }

    @Provides
    fun provideBindAPI(@ApplicationContext context: Context): ProgramService {
        interceptHttp(context)
        val client: OkHttpClient = okHttpClient.build()
        val retrofit = Retrofit.Builder()
            .baseUrl(Config.BASE_URL)
            .client(client)
            .addConverterFactory(GsonConverterFactory.create())
            .build()

        return retrofit.create(ProgramService::class.java)
    }

    @Provides
    fun provideProgramDao(@ApplicationContext context: Context): ProgramDao {
        return Room.databaseBuilder(context, AppDatabase::class.java, "Program")
            .build().getPrograms()
    }

    @Provides
    fun provideProgramRepository(programService: ProgramService, programDao: ProgramDao): ProgramRepository {
        return ProgramRepositoryImp(programService, programDao)
    }

    @Provides
    fun provideProgramUseCase(programRepository: ProgramRepository): GetProgramsUseCase {
        return GetProgramUseCaseImp(programRepository)
    }

    @Provides
    fun provideProgramCategoryListUseCase(programRepository: ProgramRepository): GetProgramCategoryUseCase {
        return GetProgramCategoryUseCaseImp(programRepository)
    }

    @Provides
    fun provideProgramTopicListUseCase(programRepository: ProgramRepository): GetProgramTopicUseCase {
        return GetProgramTopicUseCaseImp(programRepository)
    }

    @Provides
    fun provideGetProgramByFilterUseCase(programRepository: ProgramRepository): GetProgramByFilterUseCase {
        return GetProgramByFilterUseCaseImp(programRepository)
    }

    @Provides
    fun provideGetProgramsOfflineUseCase(programRepository: ProgramRepository): GetProgramOfflineUseCase {
        return GetProgramOfflineUseCaseImp(programRepository)
    }

    @Provides
    fun provideSaveProgramUseCase(programRepository: ProgramRepository): SaveProgramUseCase {
        return SaveProgramUseCaseImp(programRepository)
    }

    @Provides
    fun provideSaveCategoryProgramUseCase(programRepository: ProgramRepository): SaveCategoryProgramUseCase {
        return SaveCategoryProgramUseCaseImp(programRepository)
    }

    @Provides
    fun provideSaveTopicProgramUseCase(programRepository: ProgramRepository): SaveTopicProgramUseCase {
        return SaveTopicProgramUseCaseImp(programRepository)
    }

    @Provides
    fun provideGetProgramCategoryOfflineUseCase(programRepository: ProgramRepository): GetProgramCategoryOfflineUseCase {
        return GetProgramCategoryOfflineUseCaseImp(programRepository)
    }

    @Provides
    fun provideGetProgramTopicOfflineUseCase(programRepository: ProgramRepository): GetProgramTopicOfflineUseCase {
        return GetProgramTopicOfflineUseCaseImp(programRepository)
    }
}