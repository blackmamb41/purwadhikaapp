package com.purwadhika.purwadhikaapp.data.models.response

data class Notification(
    var title: String
)
