package com.purwadhika.purwadhikaapp.data.models.response

import android.os.Parcelable
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey
import kotlinx.parcelize.Parcelize
import java.io.Serializable

data class ProgramTopicResponse(
    var status: Status,
    var message: String?,
    var data: List<ProgramTopicData>?
)

@Entity(indices = [Index(value = ["id"], unique = true)], tableName = "Topic")
data class ProgramTopicData(
    @PrimaryKey var id: Int = 0,
    var icon: String,
    var image: String,
    var title: String,
    var description: String
): Serializable {
    constructor(): this(0,"","","","")
}
