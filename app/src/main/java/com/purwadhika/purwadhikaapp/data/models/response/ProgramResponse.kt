package com.purwadhika.purwadhikaapp.data.models.response

import androidx.room.*
import java.io.Serializable

data class ProgramResponse(
    var status: Status,
    var message: String?,
    var data: List<Program>?
)

@Entity(indices = [Index(value = ["id"], unique = true)])
data class Program(
    @PrimaryKey(autoGenerate = true) var id: Int = 0,
    var image: String,
    var title: String,
    var programCategoryId: Int?,
    var programTopicId: Int?,
    var startDate: String,
    var endDate: String,
    var schedule: String,
    var location: String,
    @Ignore var programCategory: ProgramCategoryData?,
    @Ignore var programTopic: ProgramTopicData?
): Serializable {
    constructor(): this(0, "", "", null, null, "", "", "", "",null, null)
}