package com.purwadhika.purwadhikaapp.presentation.external

import com.purwadhika.purwadhikaapp.data.models.response.Program

interface ProgramClickListener {
    fun onClickProgram(program: Program)
}