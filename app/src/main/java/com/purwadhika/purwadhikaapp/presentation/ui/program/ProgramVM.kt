package com.purwadhika.purwadhikaapp.presentation.ui.program

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.purwadhika.purwadhikaapp.data.models.response.ProfileDetailResponse
import com.purwadhika.purwadhikaapp.data.models.response.ProgramTopicResponse
import com.purwadhika.purwadhikaapp.data.models.response.Status
import com.purwadhika.purwadhikaapp.domain.usecases.GetProfileDetailUseCase
import com.purwadhika.purwadhikaapp.domain.usecases.GetProgramTopicUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import javax.inject.Inject

@HiltViewModel
class ProgramVM @Inject constructor(
    var getProgramTopicUseCase: GetProgramTopicUseCase,
    var getProfileDetailUseCase: GetProfileDetailUseCase
): ViewModel() {
    var programTopicData = MutableLiveData<ProgramTopicResponse>()
    var scoreProfile = MutableLiveData<Int>()
    fun getProgramTopic() {
        programTopicData.postValue(
            ProgramTopicResponse(
                Status.LOADING,
                null,
                null
            )
        )
        viewModelScope.launch(Dispatchers.IO) {
            getProgramTopicUseCase.getListProgramTopic().enqueue(object: Callback<ProgramTopicResponse> {
                override fun onResponse(
                    call: Call<ProgramTopicResponse>,
                    response: Response<ProgramTopicResponse>
                ) {
                    if (response.isSuccessful)
                        programTopicData.postValue(
                            ProgramTopicResponse(
                                Status.SUCCESS,
                                response.body()?.message,
                                response.body()?.data
                            )
                        )
                    else
                        programTopicData.postValue(
                            ProgramTopicResponse(
                                Status.ERROR,
                                response.body()?.message,
                                null
                            )
                        )
                }

                override fun onFailure(call: Call<ProgramTopicResponse>, t: Throwable) {
                    programTopicData.postValue(
                        ProgramTopicResponse(
                            Status.ERROR,
                            null,
                            null
                        )
                    )
                }
            })
        }
    }
    fun checkProfile(id: Int) {
        viewModelScope.launch(Dispatchers.IO) {
            getProfileDetailUseCase.invokeGetProfileDetail(id).enqueue(object: Callback<ProfileDetailResponse> {
                override fun onResponse(
                    call: Call<ProfileDetailResponse>,
                    response: Response<ProfileDetailResponse>
                ) {
                    val responseBody = response.body()
                    var score = 0
                    if (response.isSuccessful) {
                        score += if (responseBody?.data?.gender == null || responseBody.data?.gender == "")
                            0
                        else
                            1
                        score += if (responseBody?.data?.address == null || responseBody.data?.address == "")
                            0
                        else
                            1
                        score += if (responseBody?.data?.birthDate == null || responseBody.data?.birthDate == "")
                            0
                        else
                            1
                        scoreProfile.postValue(score)
                    }
                }
                override fun onFailure(call: Call<ProfileDetailResponse>, t: Throwable) {
                    t.printStackTrace()
                }
            })
        }
    }
}