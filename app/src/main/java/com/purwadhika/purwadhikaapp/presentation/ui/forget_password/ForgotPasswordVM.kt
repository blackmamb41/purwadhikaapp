package com.purwadhika.purwadhikaapp.presentation.ui.forget_password

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.purwadhika.purwadhikaapp.data.models.request.ForgotPasswordRequest
import com.purwadhika.purwadhikaapp.data.models.response.ForgetPasswordResponse
import com.purwadhika.purwadhikaapp.data.models.response.Status
import com.purwadhika.purwadhikaapp.domain.usecases.ForgotPasswordUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import javax.inject.Inject

@HiltViewModel
class ForgotPasswordVM @Inject constructor(var forgotPasswordUseCase: ForgotPasswordUseCase): ViewModel() {
    val forgotPasswordData = MutableLiveData<ForgetPasswordResponse>()
    fun doForgotPassword(forgotPasswordRequest: ForgotPasswordRequest) {
        forgotPasswordData.postValue(
            ForgetPasswordResponse(
                Status.LOADING,
                null,
                null
            )
        )
        viewModelScope.launch(Dispatchers.IO) {
            forgotPasswordUseCase.invokeForgotPassword(forgotPasswordRequest).enqueue(object: Callback<ForgetPasswordResponse> {
                override fun onResponse(
                    call: Call<ForgetPasswordResponse>,
                    response: Response<ForgetPasswordResponse>
                ) {
                    if (response.isSuccessful)
                        forgotPasswordData.postValue(
                            ForgetPasswordResponse(
                                Status.SUCCESS,
                                response.body()?.message,
                                null
                            )
                        )
                    else
                        forgotPasswordData.postValue(
                            ForgetPasswordResponse(
                                Status.ERROR,
                                response.body()?.message,
                                response.body()?.error
                            )
                        )
                }

                override fun onFailure(call: Call<ForgetPasswordResponse>, t: Throwable) {
                    forgotPasswordData.postValue(
                        ForgetPasswordResponse(
                            Status.ERROR,
                            null,
                            null
                        )
                    )
                    t.printStackTrace()
                }
            })
        }
    }
}