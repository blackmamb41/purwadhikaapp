package com.purwadhika.purwadhikaapp.presentation.external

import com.purwadhika.purwadhikaapp.data.models.response.ProgramCategoryData

interface ProgramCategoryOnClickListener {
    fun onClickProgramCategory(programCategoryData: ProgramCategoryData)
}