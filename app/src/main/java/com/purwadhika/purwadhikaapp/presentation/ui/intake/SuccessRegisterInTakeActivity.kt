package com.purwadhika.purwadhikaapp.presentation.ui.intake

import android.graphics.PorterDuff
import android.view.MenuItem
import com.purwadhika.purwadhikaapp.R
import com.purwadhika.purwadhikaapp.databinding.ActivitySuccessRegisterInTakeBinding
import com.purwadhika.purwadhikaapp.presentation.routers.BaseRouter
import com.purwadhika.purwadhikaapp.presentation.ui.base.BaseActivity

class SuccessRegisterInTakeActivity : BaseActivity() {
    lateinit var binding: ActivitySuccessRegisterInTakeBinding
    override fun setupDepedencies() {
        binding = ActivitySuccessRegisterInTakeBinding.inflate(layoutInflater)
        setContentView(binding.root)
    }

    override fun observe() {
        btnAction()
    }

    override fun setupToolbar() {
        setSupportActionBar(binding.toolbarSuccessIntake)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        binding.toolbarSuccessIntake.navigationIcon?.setColorFilter(resources.getColor(R.color.white), PorterDuff.Mode.SRC_ATOP)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item.itemId) {
            android.R.id.home -> startActivity(BaseRouter.gotToMainPage(this))
        }
        return super.onOptionsItemSelected(item)
    }

    private fun btnAction() {
        binding.btnBackToHome.setOnClickListener {
            startActivity(BaseRouter.gotToMainPage(this))
        }
    }

}