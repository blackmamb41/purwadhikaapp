package com.purwadhika.purwadhikaapp.presentation.ui.program

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.purwadhika.purwadhikaapp.data.models.response.ProgramResponse
import com.purwadhika.purwadhikaapp.data.models.response.Status
import com.purwadhika.purwadhikaapp.domain.usecases.GetProgramsUseCase
import dagger.hilt.android.AndroidEntryPoint
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import javax.inject.Inject

@HiltViewModel
class MyProgramVM @Inject constructor(var getProgramsUseCase: GetProgramsUseCase): ViewModel() {
    val dataProgram = MutableLiveData<ProgramResponse>()

    fun getListProgram() {
        dataProgram.postValue(
            ProgramResponse(
                Status.LOADING,
                null,
                null
            )
        )
        viewModelScope.launch(Dispatchers.IO) {
            getProgramsUseCase.getProgramList().enqueue(object: Callback<ProgramResponse> {
                override fun onResponse(
                    call: Call<ProgramResponse>,
                    response: Response<ProgramResponse>
                ) {
                    if (response.isSuccessful)
                        dataProgram.postValue(
                            ProgramResponse(
                                Status.SUCCESS,
                                response.body()?.message,
                                response.body()?.data
                            )
                        )
                    else
                        dataProgram.postValue(
                            ProgramResponse(
                                Status.ERROR,
                                response.body()?.message,
                                null
                            )
                        )
                }

                override fun onFailure(call: Call<ProgramResponse>, t: Throwable) {
                    dataProgram.postValue(
                        ProgramResponse(
                            Status.ERROR,
                            null,
                            null
                        )
                    )
                }

            })
        }
    }
}