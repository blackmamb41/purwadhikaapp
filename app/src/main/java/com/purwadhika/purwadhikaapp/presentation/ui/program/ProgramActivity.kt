package com.purwadhika.purwadhikaapp.presentation.ui.program

import android.app.AlertDialog
import android.content.Intent
import android.graphics.Color
import android.graphics.PorterDuff
import android.graphics.drawable.ColorDrawable
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.widget.Button
import android.widget.Toast
import androidx.activity.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.purwadhika.purwadhikaapp.R
import com.purwadhika.purwadhikaapp.data.models.response.Program
import com.purwadhika.purwadhikaapp.data.models.response.ProgramCategoryData
import com.purwadhika.purwadhikaapp.data.models.response.ProgramTopicData
import com.purwadhika.purwadhikaapp.databinding.ActivityProgramBinding
import com.purwadhika.purwadhikaapp.presentation.external.ProgramTopicAdapter
import com.purwadhika.purwadhikaapp.presentation.external.TopicClickListener
import com.purwadhika.purwadhikaapp.presentation.routers.BaseRouter
import com.purwadhika.purwadhikaapp.presentation.ui.base.BaseActivity
import com.purwadhika.purwadhikaapp.presentation.ui.topic.TopicDetailActivity
import com.purwadhika.purwadhikaapp.utils.Config
import com.purwadhika.purwadhikaapp.utils.Variables
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class ProgramActivity : BaseActivity(), TopicClickListener {
    lateinit var binding: ActivityProgramBinding
    lateinit var programTopicAdapter: ProgramTopicAdapter
    private val programVM by viewModels<ProgramVM>()
    private var statusProfile: Boolean = false
    private var program: Program? = null
    private var programCategoryData: ProgramCategoryData? = null
    lateinit var dialogPrevent: View
    override fun setupDepedencies() {
        binding = ActivityProgramBinding.inflate(layoutInflater)
        setContentView(binding.root)
        val dataProgram = intent.getSerializableExtra(Variables.PROGRAM_DATA) as Program?
        val dataCategory = intent.getSerializableExtra(Variables.PROGRAM_CATEGORY_DATA) as ProgramCategoryData?
        dataProgram?.let {
            program = it
            programCategoryData = it.programCategory
        }
        dataCategory.let {
            programCategoryData = it
        }
    }

    override fun observe() {
        checkProfile()
        setupRvTopic()
        btnAction()
        dataListener()
        if (program != null) bindingData() else bindingCategoryData()
    }

    override fun setupToolbar() {
        setSupportActionBar(binding.toolbarProgram)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowTitleEnabled(false)
        binding.toolbarProgram.navigationIcon?.setColorFilter(resources.getColor(R.color.white), PorterDuff.Mode.SRC_ATOP)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item.itemId) {
            android.R.id.home -> finish()
        }
        return super.onOptionsItemSelected(item)
    }

    private fun checkProfile() {
        Config.getIdUser(this).let {
            programVM.checkProfile(it)
        }
        programVM.scoreProfile.observe(this){
            statusProfile = it >= 3
        }
    }

    private fun dataListener() {
        programVM.getProgramTopic()
        programVM.programTopicData.observe(this){
            it.data?.let { it1 -> programTopicAdapter.updateDataTopic(it1) }
        }
    }

    private fun btnAction() {
        dialogPrevent = layoutInflater.inflate(R.layout.prevent_profile_dialog, null)
        val dialogNotAllowed = AlertDialog.Builder(this)
            .setView(dialogPrevent)
        val dialogWindow = dialogNotAllowed.create()
        dialogWindow.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        binding.btnRegister.setOnClickListener {
            Log.i("TAG", "btnAction: $statusProfile")
            if (statusProfile)
                startActivity(BaseRouter.goToInTakePage(this))
            else {
                dialogWindow.show()
            }
        }
        dialogPrevent.findViewById<Button>(R.id.btn_profile).setOnClickListener {
            dialogWindow.dismiss()
            startActivity(BaseRouter.goToProfilePage(this))
        }
        binding.qna.setOnClickListener {
            startActivity(BaseRouter.goToQnaPage(this))
        }
    }

    private fun setupRvTopic() {
        programTopicAdapter = ProgramTopicAdapter(this)
        binding.rvTopics.adapter = programTopicAdapter
        binding.rvTopics.layoutManager = LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
    }

    override fun onTopicClick(programTopicData: ProgramTopicData) {
        val topicDetailIntent = Intent(this, TopicDetailActivity::class.java)
        topicDetailIntent.putExtra(Variables.PROGRAM_TOPIC, programTopicData)
        startActivity(topicDetailIntent)
    }

    private fun bindingData() {
        Glide.with(this)
            .load(program?.programCategory?.image)
            .diskCacheStrategy(DiskCacheStrategy.DATA)
            .into(binding.heroProgram)
        binding.titleProgram.text = program?.title
        binding.programDescription.text= program?.programCategory?.description
    }

    private fun bindingCategoryData() {
        Glide.with(this).load(programCategoryData?.image).into(binding.heroProgram)
        binding.titleProgram.text = programCategoryData?.title
        binding.programDescription.text = programCategoryData?.description
    }
}