package com.purwadhika.purwadhikaapp.presentation.ui.home

import android.annotation.SuppressLint
import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.*
import android.widget.ImageButton
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.NavHost
import androidx.navigation.Navigation
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.fragment.findNavController
import androidx.navigation.ui.onNavDestinationSelected
import androidx.recyclerview.widget.LinearLayoutManager
import com.purwadhika.purwadhikaapp.R
import com.purwadhika.purwadhikaapp.data.models.response.Program
import com.purwadhika.purwadhikaapp.data.models.response.ProgramCategoryData
import com.purwadhika.purwadhikaapp.data.models.response.ProgramTopicData
import com.purwadhika.purwadhikaapp.data.models.response.Status
import com.purwadhika.purwadhikaapp.databinding.FragmentHomeBinding
import com.purwadhika.purwadhikaapp.presentation.external.ProgramClickListener
import com.purwadhika.purwadhikaapp.presentation.external.ProgramListAdapter
import com.purwadhika.purwadhikaapp.presentation.external.ProgramTopicAdapter
import com.purwadhika.purwadhikaapp.presentation.external.TopicClickListener
import com.purwadhika.purwadhikaapp.presentation.routers.BaseRouter
import dagger.hilt.android.AndroidEntryPoint
import kotlin.properties.Delegates


@AndroidEntryPoint
class HomeFragment : Fragment(), ProgramClickListener, TopicClickListener {
    lateinit var binding: FragmentHomeBinding
    private val homeVM by viewModels<HomeVM>()
    lateinit var adapterProgram: ProgramListAdapter
    lateinit var adapterTopicProgram: ProgramTopicAdapter
    private var arrayProgramCategory: ArrayList<ProgramCategoryData> = arrayListOf()
    private var arrayProgramTopic: ArrayList<ProgramTopicData> = arrayListOf()
    private var isLoaded: Boolean = false
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentHomeBinding.inflate(inflater, container, false)
        return binding.root
    }

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        btnQna()
        setupRVProgram()
        setupRVTopicProgram()
        homeVM.getProgramCategoryList()
        homeVM.getListProgramTopic()
        homeVM.getProgramList()
        dataListener()
        navigationComponent()
        if (!isLoaded) loadData()
        setupToolbar()
        setHasOptionsMenu(true)
        super.onViewCreated(view, savedInstanceState)
    }

    private fun setupToolbar() {
        (activity as AppCompatActivity).setSupportActionBar(binding.mainToolbar)
        (activity as AppCompatActivity).supportActionBar?.setDisplayShowTitleEnabled(false)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.main_header_menu, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    private fun setupRVProgram() {
        adapterProgram = ProgramListAdapter(this)
        binding.rvProgram.adapter = adapterProgram
        binding.rvProgram.layoutManager = LinearLayoutManager(requireContext(), LinearLayoutManager.HORIZONTAL, false)
    }

    private fun setupRVTopicProgram() {
        adapterTopicProgram = ProgramTopicAdapter(this)
        binding.rvTopics.adapter = adapterTopicProgram
        binding.rvTopics.layoutManager = LinearLayoutManager(requireContext(), LinearLayoutManager.HORIZONTAL, false)
    }

    private fun loadData() {
        homeVM.programList.observe(viewLifecycleOwner) {
            when(it.status) {
                Status.LOADING -> {
                    binding.loadProgram.visibility = View.VISIBLE
                }
                Status.SUCCESS -> {
                    binding.loadProgram.visibility = View.GONE
                    it.data.let { program ->
                        if (program != null) adapterProgram.updateProgram(program)
                    }
                }
                Status.ERROR -> {
                    binding.loadProgram.visibility = View.GONE
                    homeVM.programListDB.observe(viewLifecycleOwner) { programs ->
                        Log.i("TAG", "loadData: $programs")
                        if (programs != null) adapterProgram.updateProgram(programs)
                    }
                }
            }
        }
        homeVM.getProgramList()
    }

    override fun onClickProgram(program: Program) {
        activity?.applicationContext?.let {
            startActivity(BaseRouter.goToProgramPage(it, program, false))
        }
    }

    private fun btnQna() {
        binding.qna.setOnClickListener {
            activity?.applicationContext?.let {
                startActivity(BaseRouter.goToQnaPage(it))
            }
        }
    }

    private fun dataListener() {
        val programListView = binding.programFilter
        val programTopicListView = binding.topicFilter
        homeVM.programCategroyList.observe(viewLifecycleOwner) {
            val arrayListProgramCategory = arrayListOf<String>()
            it.data?.forEach { category ->
                category.title?.let { it1 -> arrayListProgramCategory.add(it1) }
                arrayProgramCategory.add(category)
            }
            programListView.setItems(arrayListProgramCategory)
        }
        homeVM.programTopicList.observe(viewLifecycleOwner) {
            val arrayListTopicProgram = arrayListOf<String>()
            it.data?.forEach { topic ->
                arrayListTopicProgram.add(topic.title)
                arrayProgramTopic.add(topic)
            }
            programTopicListView.setItems(arrayListTopicProgram)
            when(it.status) {
                Status.LOADING -> binding.loadTopic.visibility = View.VISIBLE
                Status.SUCCESS -> {
                    binding.loadTopic.visibility = View.GONE
                    it.data?.let { topics -> adapterTopicProgram.updateDataTopic(topics) }
                }
                Status.ERROR -> {
                    binding.loadTopic.visibility = View.GONE
                    homeVM.programTopicDB.observe(viewLifecycleOwner){ topics ->
                        adapterTopicProgram.updateDataTopic(topics)
                    }
                }
            }
        }
    }

    private fun navigationComponent() {
        binding.seeAllProgram.setOnClickListener {
            Navigation.findNavController(it).navigate(R.id.action_homeFragment_to_allProgramFragment)
        }
    }

    override fun onTopicClick(programTopicData: ProgramTopicData) {
        activity?.applicationContext?.let {
            startActivity(BaseRouter.goToProgramTopicPage(it, programTopicData))
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item.itemId) {
            R.id.profileFragment -> NavHostFragment.findNavController(this).navigate(R.id.action_homeFragment_to_profileFragment)
            R.id.notification -> NavHostFragment.findNavController(this).navigate(R.id.action_homeFragment_to_notificationFragment)
        }
        return super.onOptionsItemSelected(item)
    }
}