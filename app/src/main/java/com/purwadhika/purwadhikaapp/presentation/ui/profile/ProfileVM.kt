package com.purwadhika.purwadhikaapp.presentation.ui.profile

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.purwadhika.purwadhikaapp.data.models.response.ProfileDetailResponse
import com.purwadhika.purwadhikaapp.data.models.response.ProfileResponse
import com.purwadhika.purwadhikaapp.data.models.response.Status
import com.purwadhika.purwadhikaapp.domain.usecases.GetProfileDetailUseCase
import com.purwadhika.purwadhikaapp.domain.usecases.UpdateBirthDateUseCase
import com.purwadhika.purwadhikaapp.utils.EspressoIdlingResource
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import javax.inject.Inject

@HiltViewModel
class ProfileVM @Inject constructor(
    var getProfileDetailUseCase: GetProfileDetailUseCase,
    var updateBirthDateUseCase: UpdateBirthDateUseCase
): ViewModel() {
    var profileData = MutableLiveData<ProfileDetailResponse>()
    var updateProfileResponse = MutableLiveData<ProfileResponse>()
    fun getDataProfile(id: Int) {
        EspressoIdlingResource.idling()
        profileData.postValue(
            ProfileDetailResponse(
                Status.LOADING,
                null,
                null
            )
        )
        viewModelScope.launch(Dispatchers.IO) {
            getProfileDetailUseCase.invokeGetProfileDetail(id).enqueue(object: Callback<ProfileDetailResponse> {
                override fun onResponse(
                    call: Call<ProfileDetailResponse>,
                    response: Response<ProfileDetailResponse>
                ) {
                    EspressoIdlingResource.execute()
                    if (response.isSuccessful)
                        profileData.postValue(
                            ProfileDetailResponse(
                                Status.SUCCESS,
                                response.body()?.message,
                                response.body()?.data
                            )
                        )
                    else
                        profileData.postValue(
                            ProfileDetailResponse(
                                Status.ERROR,
                                response.body()?.message,
                                null
                            )
                        )
                }

                override fun onFailure(call: Call<ProfileDetailResponse>, t: Throwable) {
                    EspressoIdlingResource.execute()
                    profileData.postValue(
                        ProfileDetailResponse(
                            Status.ERROR,
                            null,
                            null
                        )
                    )
                    t.printStackTrace()
                }
            })
        }
    }
    fun updateDateBirthProfile(id: Int, dateBirth: String) {
        EspressoIdlingResource.idling()
        updateProfileResponse.postValue(
            ProfileResponse(
                Status.LOADING,
                null,
                null
            )
        )
        viewModelScope.launch(Dispatchers.IO) {
            updateBirthDateUseCase.updateBirthDate(id, dateBirth).enqueue(object: Callback<ProfileResponse> {
                override fun onResponse(
                    call: Call<ProfileResponse>,
                    response: Response<ProfileResponse>
                ) {
                    EspressoIdlingResource.execute()
                    if (response.isSuccessful)
                        updateProfileResponse.postValue(
                            ProfileResponse(
                                Status.SUCCESS,
                                response.body()?.message,
                                null
                            )
                        )
                    else
                        updateProfileResponse.postValue(
                            ProfileResponse(
                                Status.ERROR,
                                response.body()?.message,
                                response.body()?.error
                            )
                        )
                }

                override fun onFailure(call: Call<ProfileResponse>, t: Throwable) {
                    EspressoIdlingResource.execute()
                    updateProfileResponse.postValue(
                        ProfileResponse(
                            Status.ERROR,
                            null,
                            null
                        )
                    )
                }

            })
        }
    }
}