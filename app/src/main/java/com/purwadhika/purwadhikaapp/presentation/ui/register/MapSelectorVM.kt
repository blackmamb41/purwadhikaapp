package com.purwadhika.purwadhikaapp.presentation.ui.register

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.purwadhika.purwadhikaapp.data.models.response.ProfileResponse
import com.purwadhika.purwadhikaapp.data.models.response.Status
import com.purwadhika.purwadhikaapp.domain.usecases.UpdateMapLocationUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import javax.inject.Inject

@HiltViewModel
class MapSelectorVM @Inject constructor(var updateMapLocationUseCase: UpdateMapLocationUseCase): ViewModel() {
    var updateLocationResponse = MutableLiveData<ProfileResponse>()
    fun updateAddressLocation(id: Int, address: String, lat: Double, lon: Double) {
        updateLocationResponse.postValue(
            ProfileResponse(
                Status.LOADING,
                null,
                null
            )
        )
        viewModelScope.launch(Dispatchers.IO) {
            updateMapLocationUseCase.updateLocationProfile(id, address, lat.toString(), lon.toString()).enqueue(object: Callback<ProfileResponse> {
                override fun onResponse(
                    call: Call<ProfileResponse>,
                    response: Response<ProfileResponse>
                ) {
                    if(response.isSuccessful)
                        updateLocationResponse.postValue(
                            ProfileResponse(
                                Status.SUCCESS,
                                response.body()?.message,
                                null
                            )
                        )
                    else
                        updateLocationResponse.postValue(
                            ProfileResponse(
                                Status.ERROR,
                                response.body()?.message,
                                null
                            )
                        )
                }

                override fun onFailure(call: Call<ProfileResponse>, t: Throwable) {
                    t.printStackTrace()
                }

            })
        }
    }
}