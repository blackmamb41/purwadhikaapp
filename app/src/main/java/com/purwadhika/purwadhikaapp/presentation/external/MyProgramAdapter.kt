package com.purwadhika.purwadhikaapp.presentation.external

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.purwadhika.purwadhikaapp.data.models.response.Program
import com.purwadhika.purwadhikaapp.databinding.ListMyProgramBinding
import java.text.SimpleDateFormat
import java.util.*

class MyProgramAdapter (var programClickListener: ProgramClickListener): RecyclerView.Adapter<MyProgramAdapter.MyProgramViewHolder>() {

    var listProgram = listOf<Program>()

    inner class MyProgramViewHolder(var binding: ListMyProgramBinding): RecyclerView.ViewHolder(binding.root) {
        @SuppressLint("SetTextI18n")
        fun bind(program: Program) {
            binding.titleProgram.text = program.title
            binding.location.text = program.location
            binding.schedule.text = "${convertDate(program.startDate)} - ${convertDate(program.endDate)}"
            binding.category.text = program.programCategory?.title
            itemView.setOnClickListener {
                programClickListener.onClickProgram(program)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyProgramViewHolder {
        val binding = ListMyProgramBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return MyProgramViewHolder(binding)
    }

    override fun onBindViewHolder(holder: MyProgramViewHolder, position: Int) {
        return holder.bind(listProgram[position])
    }

    override fun getItemCount(): Int = listProgram.size

    fun updateProgramData(programs: List<Program>) {
        listProgram = programs
        notifyDataSetChanged()
    }

    @SuppressLint("SimpleDateFormat")
    private fun convertDate(dateString: String): String {
        val inputFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
        val outputFormat = SimpleDateFormat("dd MMM yyyy")
        val date: Date = inputFormat.parse(dateString)
        return outputFormat.format(date)
    }
}