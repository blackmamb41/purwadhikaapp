package com.purwadhika.purwadhikaapp.presentation.ui.home

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.purwadhika.purwadhikaapp.data.models.response.*
import com.purwadhika.purwadhikaapp.domain.usecases.*
import com.purwadhika.purwadhikaapp.utils.EspressoIdlingResource
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import javax.inject.Inject
import javax.net.ssl.SSLEngineResult

@HiltViewModel
class HomeVM @Inject constructor(
    var getProgramsUseCase: GetProgramsUseCase,
    var getProgramCategoryUseCase: GetProgramCategoryUseCase,
    var getProgramTopicUseCase: GetProgramTopicUseCase,
    var getProgramOfflineUseCase: GetProgramOfflineUseCase,
    var getProgramCategoryOfflineUseCase: GetProgramCategoryOfflineUseCase,
    var getProgramTopicOfflineUseCase: GetProgramTopicOfflineUseCase,
    var saveProgramUseCase: SaveProgramUseCase,
    var saveTopicProgramUseCase: SaveTopicProgramUseCase,
    var saveCategoryProgramUseCase: SaveCategoryProgramUseCase
): ViewModel() {
    var programList = MutableLiveData<ProgramResponse>()
    var programCategroyList = MutableLiveData<ProgramCategoryResponse>()
    var programTopicList = MutableLiveData<ProgramTopicResponse>()
    var programListDB = getProgramOfflineUseCase.getPrograms()
    var programTopicDB = getProgramTopicOfflineUseCase.invokeGetProgramTopicOffline()
    var programCategoryDB = getProgramCategoryOfflineUseCase.invokeGetProgramCategoryOffline()
    fun getProgramList() {
        EspressoIdlingResource.idling()
        programList.postValue(
            ProgramResponse(
                Status.LOADING,
                null,
                null
            )
        )
        viewModelScope.launch(Dispatchers.IO) {
            getProgramsUseCase.getProgramList().enqueue(object: Callback<ProgramResponse> {
                override fun onResponse(
                    call: Call<ProgramResponse>,
                    response: Response<ProgramResponse>
                ) {
                    EspressoIdlingResource.execute()
                    if (response.isSuccessful) {
                        programList.postValue(
                            ProgramResponse(
                                Status.SUCCESS,
                                response.body()?.message,
                                response.body()?.data
                            )
                        )
                        response.body()?.data?.forEach { programData ->
                            viewModelScope.launch(Dispatchers.IO) {
                                saveProgramUseCase.invokeInsertProgram(programData)
                            }
                        }
                    } else
                        programList.postValue(
                            ProgramResponse(
                                Status.ERROR,
                                response.body()?.message,
                                null
                            )
                        )
                }

                override fun onFailure(call: Call<ProgramResponse>, t: Throwable) {
                    EspressoIdlingResource.execute()
                    programList.postValue(
                        ProgramResponse(
                            Status.ERROR,
                            null,
                            null
                        )
                    )
                    t.printStackTrace()
                }

            })
        }
    }

    fun getProgramCategoryList() {
        EspressoIdlingResource.idling()
        programCategroyList.postValue(
            ProgramCategoryResponse(
                Status.LOADING,
                null,
                null
            )
        )
        viewModelScope.launch(Dispatchers.IO) {
            getProgramCategoryUseCase.getListProgramCategory().enqueue(object: Callback<ProgramCategoryResponse> {
                override fun onResponse(
                    call: Call<ProgramCategoryResponse>,
                    response: Response<ProgramCategoryResponse>
                ) {
                    EspressoIdlingResource.execute()
                    if (response.isSuccessful) {
                        programCategroyList.postValue(
                            ProgramCategoryResponse(
                                Status.SUCCESS,
                                response.body()?.message,
                                response.body()?.data
                            )
                        )
                        viewModelScope.launch(Dispatchers.IO) {
                            response.body()?.data?.forEach { categoryData ->
                                saveCategoryProgramUseCase.invokeSaveCategoryProgram(categoryData)
                            }
                        }
                    }
                    else
                        programCategroyList.postValue(
                            ProgramCategoryResponse(
                                Status.ERROR,
                                response.body()?.message,
                                null
                            )
                        )
                }

                override fun onFailure(call: Call<ProgramCategoryResponse>, t: Throwable) {
                    EspressoIdlingResource.execute()
                    programCategroyList.postValue(
                        ProgramCategoryResponse(
                            Status.ERROR,
                            null,
                            null
                        )
                    )
                }

            })
        }
    }

    fun getListProgramTopic() {
        EspressoIdlingResource.idling()
        programTopicList.postValue(
            ProgramTopicResponse(
                Status.LOADING,
                null,
                null
            )
        )
        viewModelScope.launch(Dispatchers.IO) {
            getProgramTopicUseCase.getListProgramTopic().enqueue(object: Callback<ProgramTopicResponse> {
                override fun onResponse(
                    call: Call<ProgramTopicResponse>,
                    response: Response<ProgramTopicResponse>
                ) {
                    EspressoIdlingResource.execute()
                    if (response.isSuccessful){
                        programTopicList.postValue(
                            ProgramTopicResponse(
                                Status.SUCCESS,
                                response.body()?.message,
                                response.body()?.data
                            )
                        )
                        viewModelScope.launch(Dispatchers.IO) {
                            response.body()?.data?.forEach { topicData ->
                                saveTopicProgramUseCase.invokeSaveTopicProgram(topicData)
                            }
                        }
                    }
                    else
                        programCategroyList.postValue(
                            ProgramCategoryResponse(
                                Status.ERROR,
                                response.body()?.message,
                                null
                            )
                        )
                }

                override fun onFailure(call: Call<ProgramTopicResponse>, t: Throwable) {
                    EspressoIdlingResource.execute()
                    programTopicList.postValue(
                        ProgramTopicResponse(
                            Status.ERROR,
                            null,
                            null
                        )
                    )
                }

            })
        }
    }
}