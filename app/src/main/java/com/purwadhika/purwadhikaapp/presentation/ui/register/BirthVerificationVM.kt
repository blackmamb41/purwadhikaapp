package com.purwadhika.purwadhikaapp.presentation.ui.register

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.purwadhika.purwadhikaapp.data.models.response.ProfileResponse
import com.purwadhika.purwadhikaapp.data.models.response.Status
import com.purwadhika.purwadhikaapp.domain.usecases.UpdateBirthDateUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import javax.inject.Inject

@HiltViewModel
class BirthVerificationVM @Inject constructor(var updateBirthDateUseCase: UpdateBirthDateUseCase): ViewModel() {
    var responseUpdate = MutableLiveData<ProfileResponse>()
    fun doUpdateDateBirth(id: Int, dateBirth: String) {
        responseUpdate.postValue(
            ProfileResponse(
                Status.LOADING,
                null,
                null
            )
        )
        viewModelScope.launch(Dispatchers.IO) {
            updateBirthDateUseCase.updateBirthDate(id, dateBirth).enqueue(object: Callback<ProfileResponse> {
                override fun onResponse(
                    call: Call<ProfileResponse>,
                    response: Response<ProfileResponse>
                ) {
                    if (response.isSuccessful)
                        responseUpdate.postValue(
                            ProfileResponse(
                                Status.SUCCESS,
                                response.body()?.message,
                                null
                            )
                        )
                    else
                        responseUpdate.postValue(
                            ProfileResponse(
                                Status.ERROR,
                                response.body()?.message,
                                null
                            )
                        )
                }

                override fun onFailure(call: Call<ProfileResponse>, t: Throwable) {
                    responseUpdate.postValue(
                        ProfileResponse(
                            Status.ERROR,
                            null,
                            null
                        )
                    )
                }

            })
        }
    }
}