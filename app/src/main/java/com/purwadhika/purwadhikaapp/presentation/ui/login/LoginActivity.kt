package com.purwadhika.purwadhikaapp.presentation.ui.login

import android.graphics.Color
import android.text.method.HideReturnsTransformationMethod
import android.text.method.PasswordTransformationMethod
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.widget.Button
import android.widget.TextView
import androidx.activity.viewModels
import com.google.android.material.snackbar.Snackbar
import com.kusu.loadingbutton.LoadingButton
import com.purwadhika.purwadhikaapp.R
import com.purwadhika.purwadhikaapp.databinding.ActivityLoginBinding
import com.purwadhika.purwadhikaapp.data.models.request.LoginRequest
import com.purwadhika.purwadhikaapp.data.models.response.Status
import com.purwadhika.purwadhikaapp.presentation.routers.BaseRouter
import com.purwadhika.purwadhikaapp.presentation.ui.base.BaseActivity
import com.purwadhika.purwadhikaapp.utils.Config
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class LoginActivity : BaseActivity() {
    lateinit var binding: ActivityLoginBinding
    lateinit var tvEmail: TextView
    lateinit var tvPassword: TextView
    lateinit var btnSignIn: Button
    private val loginVM by viewModels<LoginVM>()
    private var seePassword: Boolean = false

    override fun setupDepedencies() {
        binding = ActivityLoginBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setupToolbar()
        tvEmail = binding.emailForm
        tvPassword = binding.passForm
        btnSignIn = binding.btnSignIn
        btnAction()
    }

    override fun observe() {
        loginVM.getLoginLiveData().observe(this) {
            when(it.status) {
                Status.LOADING -> {
                    btnSignIn.text = ""
                    binding.loadLogin.visibility = View.VISIBLE
                }
                Status.ERROR -> {
                    Snackbar.make(binding.root, R.string.wrong_login, Snackbar.LENGTH_SHORT)
                        .setBackgroundTint(Color.RED)
                        .show()
                    binding.loadLogin.visibility = View.GONE
                    btnSignIn.setText(R.string.sign_in)
                }
                Status.SUCCESS -> {
                    btnSignIn.setText(R.string.sign_in)
                    binding.loadLogin.visibility = View.GONE
                    it.data?.let { data -> Config.setToken(this, data.token, data.userId) }
                    startActivity(BaseRouter.gotToMainPage(this))
                    finish()
                }
            }
        }
    }

    private fun btnAction() {
        btnSignIn.setOnClickListener {
            val dataLogin = LoginRequest(tvEmail.text.toString(), tvPassword.text.toString())
            loginVM.doLogin(dataLogin)
        }
        binding.forgotPass.setOnClickListener {
            startActivity(BaseRouter.goToForgotPasswordPage(this))
        }
        binding.passInput.setEndIconOnClickListener {
            if(seePassword) {
                binding.passForm.transformationMethod = PasswordTransformationMethod.getInstance()
                seePassword = false
            } else {
                binding.passForm.transformationMethod =
                    HideReturnsTransformationMethod.getInstance()
                seePassword = true
            }
        }
    }

    override fun setupToolbar() {
        setSupportActionBar(binding.loginToolbar)
        supportActionBar?.title = "Sign In"
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item.itemId) {
            android.R.id.home -> finish()
        }
        return super.onOptionsItemSelected(item)
    }
}