package com.purwadhika.purwadhikaapp.presentation.external

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.purwadhika.purwadhikaapp.data.models.response.ProgramCategoryData
import com.purwadhika.purwadhikaapp.databinding.ListAllProgramBinding

class ProgramCategoryAdapter (var programCategoryOnClickListener: ProgramCategoryOnClickListener): RecyclerView.Adapter<ProgramCategoryAdapter.ProgramCategoryViewHolder>() {
    var listProgramCategory = listOf<ProgramCategoryData>()

    inner class ProgramCategoryViewHolder(var binding: ListAllProgramBinding): RecyclerView.ViewHolder(binding.root) {
        fun bind(programCategoryData: ProgramCategoryData) {
            binding.programCategoryTitle.text = programCategoryData.title
            itemView.setOnClickListener {
                programCategoryOnClickListener.onClickProgramCategory(programCategoryData)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProgramCategoryViewHolder {
        val binding = ListAllProgramBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ProgramCategoryViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ProgramCategoryViewHolder, position: Int) {
        holder.bind(listProgramCategory[position])
    }

    override fun getItemCount(): Int = listProgramCategory.size

    fun updateDataProgramCategory(listCategory: List<ProgramCategoryData>) {
        listProgramCategory = listCategory
        notifyDataSetChanged()
    }
}