package com.purwadhika.purwadhikaapp.presentation.ui.profile

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import com.bumptech.glide.Glide
import com.github.florent37.singledateandtimepicker.dialog.SingleDateAndTimePickerDialog
import com.github.ybq.android.spinkit.SpinKitView
import com.kizitonwose.calendarview.model.CalendarDay
import com.kizitonwose.calendarview.model.CalendarMonth
import com.kizitonwose.calendarview.model.DayOwner
import com.kizitonwose.calendarview.ui.DayBinder
import com.kizitonwose.calendarview.ui.MonthHeaderFooterBinder
import com.kizitonwose.calendarview.ui.ViewContainer
import com.kusu.loadingbutton.LoadingButton
import com.purwadhika.purwadhikaapp.R
import com.purwadhika.purwadhikaapp.data.models.response.Status
import com.purwadhika.purwadhikaapp.databinding.CalendarHeaderLayoutBinding
import com.purwadhika.purwadhikaapp.databinding.FragmentProfileBinding
import com.purwadhika.purwadhikaapp.presentation.routers.BaseRouter
import com.purwadhika.purwadhikaapp.presentation.ui.login.LoginActivity
import com.purwadhika.purwadhikaapp.utils.Config
import com.purwadhika.purwadhikaapp.utils.Variables
import dagger.hilt.android.AndroidEntryPoint
import java.text.SimpleDateFormat
import java.time.LocalDate
import java.time.YearMonth
import java.time.temporal.WeekFields
import java.util.*

@AndroidEntryPoint
class ProfileFragment : Fragment() {
    lateinit var binding: FragmentProfileBinding
    lateinit var calendarView: View
    private val profileVM by viewModels<ProfileVM>()
    lateinit var calendar: Calendar
    lateinit var monthFormat: SimpleDateFormat
    lateinit var yearFormat: SimpleDateFormat
    private var month: String? = null
    private var monthNumber: Int = 0
    private var year: String? = null
    private var selectedDate: LocalDate? = null
    lateinit var calendarContainer: com.kizitonwose.calendarview.CalendarView
    lateinit var dialogBuilder: AlertDialog
    lateinit var btnSaveDate: Button

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentProfileBinding.inflate(layoutInflater, container, false)
        return binding.root
    }

    @SuppressLint("InflateParams")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        calendarView = layoutInflater.inflate(R.layout.calendar_dialog_layout, null)
        initCalendar()
        calendar = Calendar.getInstance()
        monthFormat = SimpleDateFormat("MMMM", Locale.getDefault())
        yearFormat = SimpleDateFormat("yyyy", Locale.getDefault())
        month = monthFormat.format(calendar.time)
        year = yearFormat.format(calendar.time)
        setupCalendar(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH) + 1)
        activity?.applicationContext?.let { Config.getIdUser(it) }?.let {
            profileVM.getDataProfile(
                it
            )
            loadData()
        }
        binding.buttonLogout.setOnClickListener {
            activity?.application?.let {
                Config.deleteToken(it)
                val loginIntent = Intent(activity, LoginActivity::class.java)
                loginIntent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                startActivity(loginIntent)
            }
        }
        binding.formGender.setOnClickListener {
            activity?.application?.let {
                startActivity(BaseRouter.goToGenrePickPage(it, false))
            }
        }
        binding.formLocation.setOnClickListener {
            startActivity(activity?.applicationContext?.let { context ->
                BaseRouter.goToMapLocation(
                    context,
                    false
                )
            })
        }
        binding.selectToMap.setOnClickListener {
            startActivity(activity?.applicationContext?.let { context ->
                BaseRouter.goToMapLocation(
                    context,
                    false
                )
            })
        }
        calendarView.findViewById<Button>(R.id.btn_save).setOnClickListener {
            if (selectedDate == null)
                Toast.makeText(activity, "Date Not Selected", Toast.LENGTH_LONG).show()
            else
                activity?.applicationContext?.let {
                    Config.getIdUser(it).let { userId ->
                        val monthChoice: String = if (monthNumber < 10) {
                            "0$monthNumber"
                        }else{
                            monthNumber.toString()
                        }
                        val dayChoice: String = if (selectedDate?.dayOfMonth!! < 10) {
                            "0${selectedDate?.dayOfMonth}"
                        }else{
                            selectedDate?.dayOfMonth.toString()
                        }
                        val dateChoice = "$year-$monthChoice-$dayChoice"
                        profileVM.updateDateBirthProfile(userId, dateChoice)
                    }
                }
        }
        binding.changePhotoBtn.setOnClickListener {
            startActivity(activity?.applicationContext?.let { context ->
                BaseRouter.goToAvatarUpload(
                    context,false)
            })
        }
        binding.textSemuaProgram.setOnClickListener {
            startActivity(activity?.applicationContext?.let { it1 ->
                BaseRouter.goToMyProgramPage(
                    it1
                )
            })
        }
        binding.arrowSemuaProgram.setOnClickListener {
            startActivity(activity?.applicationContext?.let { it1 ->
                BaseRouter.goToMyProgramPage(it1)
            })
        }
        binding.frameLayout3.setOnClickListener {
            startActivity(activity?.applicationContext?.let { it1 ->
                BaseRouter.goToMyProgramPage(it1)
            })
        }
        btnBirthCalendarListener()
        dateBirthUpdateListener()
        super.onViewCreated(view, savedInstanceState)
    }

    @SuppressLint("SetTextI18n", "SimpleDateFormat")
    private fun loadData() {
        profileVM.profileData.observe(viewLifecycleOwner) {
            when(it.status) {
                Status.LOADING -> binding.containerLoading.visibility = View.VISIBLE
                Status.ERROR -> {
                    binding.containerLoading.visibility = View.GONE
                    Toast.makeText(activity, "Error Loading Information", Toast.LENGTH_LONG).show()
                }
                Status.SUCCESS -> {
                    if(it.data?.birthDate != null || it.data?.birthDate == "") {
                        it.data?.birthDate?.let { dateBirth ->
                            binding.formBirthday.text = convertDate(dateBirth)
                        }
                    }
                    it.data?.profilePicture.let { picture ->
                        if (!picture.isNullOrEmpty()) Glide.with(this).load(picture).into(binding.imageProfile)
                    }
                    binding.containerLoading.visibility = View.GONE
                    binding.formName.text = "${it.data?.firstName} ${it.data?.lastName}"
                    binding.formGender.text = when(it.data?.gender) {
                        "male" -> "Male"
                        "female" -> "Female"
                        else -> "-----"
                    }
                    binding.formLocation.text = it.data?.address
                }
            }
        }
    }

    override fun onResume() {
        activity?.applicationContext?.let { context ->
            Config.getIdUser(context).let {
                profileVM.getDataProfile(it)
            }
        }
        super.onResume()
    }

    private fun btnBirthCalendarListener() {
        dialogBuilder = AlertDialog.Builder(activity)
            .setView(calendarView).create()
        binding.formBirthday.setOnClickListener {
            dialogBuilder.show()
        }
        btnSaveDate = calendarView.findViewById(R.id.btn_save)
        val btnMonth = calendarView.findViewById<Button>(R.id.btn_pick_month)
        val btnYear = calendarView.findViewById<Button>(R.id.btn_pick_year)
        btnMonth.text = month
        btnYear.text = year
        btnMonth.setOnClickListener {
            dialogBuilder.dismiss()
            SingleDateAndTimePickerDialog.Builder(activity)
                .curved()
                .displayYears(false)
                .displayMonth(true)
                .displayDays(false)
                .displayHours(false)
                .displayMinutes(false)
                .displayAmPm(false)
                .listener {
                    month = Variables.MONTH[it.month + 1]
                    monthNumber = it.month+1
                    btnMonth.text = Variables.MONTH[it.month]
                    dialogBuilder.show()
                    setupCalendar(convertLongToTime(it.time).toInt(), it.month + 1)
                }
                .dimBackground()
                .mainColor(resources.getColor(R.color.primary))
                .title("Month")
                .dimAmount(.25f)
                .display()
        }
        btnYear.setOnClickListener {
            dialogBuilder.dismiss()
            SingleDateAndTimePickerDialog.Builder(activity)
                .curved()
                .displayYears(true)
                .displayMonth(false)
                .displayDays(false)
                .displayHours(false)
                .displayMinutes(false)
                .displayAmPm(false)
                .listener {
                    year = convertLongToTime(it.time)
                    btnYear.text = convertLongToTime(it.time)
                    dialogBuilder.show()
                    setupCalendar(convertLongToTime(it.time).toInt(), it.month + 1)
                }
                .dimBackground()
                .mainColor(resources.getColor(R.color.primary))
                .dimAmount(.25f)
                .title(getString(R.string.year))
                .display()
        }
    }

    private fun convertLongToTime(time: Long): String {
        val date = Date(time)
        val format = SimpleDateFormat("yyyy")
        return format.format(date)
    }

    private fun setupCalendar(year: Int, month: Int) {
        val currentMonth = YearMonth.of(year,month)
        val firstMonth = currentMonth.minusMonths(0)
        val lastMonth = currentMonth.plusMonths(0)
        val firstDayOfWeek = WeekFields.of(Locale.getDefault()).firstDayOfWeek
        calendarContainer.setup(firstMonth, lastMonth, firstDayOfWeek)
    }

    private fun initCalendar() {
        calendarContainer = calendarView.findViewById<com.kizitonwose.calendarview.CalendarView>(R.id.calendar_body)
        calendarContainer.dayBinder = object: DayBinder<DayViewContainer> {
            override fun bind(container: DayViewContainer, day: CalendarDay) {
                container.day = day
                container.textView.text = day.date.dayOfMonth.toString()
                val textView = container.textView
                if(day.owner == DayOwner.THIS_MONTH) {
                    textView.visibility = View.VISIBLE
                    when(day.date) {
                        selectedDate -> {
                            textView.setTextColor(Color.WHITE)
                            textView.setBackgroundResource(R.drawable.green_circle)
                        }
                        else -> {
                            textView.setTextColor(Color.BLACK)
                            textView.setBackgroundColor(Color.WHITE)
                        }
                    }
                }
            }
            override fun create(view: View): DayViewContainer = DayViewContainer(view)
        }
        calendarContainer.monthHeaderBinder = object: MonthHeaderFooterBinder<MonthViewContainer> {
            override fun bind(container: MonthViewContainer, month: CalendarMonth) {
                if(container.legendLayout.tag == null) container.legendLayout.tag = month.yearMonth
            }

            override fun create(view: View): MonthViewContainer = MonthViewContainer(view)
        }
    }

    inner class DayViewContainer(view: View): ViewContainer(view) {
        lateinit var day: CalendarDay
        val textView: TextView = view.findViewById(R.id.calendarDayText)
        init {
            view.setOnClickListener {
                if(day.owner == DayOwner.THIS_MONTH) {
                    selectDate(day.date)
                }
            }
        }
    }

    private fun selectDate(date: LocalDate) {
        if(selectedDate != date) {
            val oldDate = selectedDate
            selectedDate = date
            oldDate?.let {
                calendarContainer.notifyDateChanged(it)
            }
            calendarContainer.notifyDateChanged(date)
        }
    }

    inner class MonthViewContainer(view: View): ViewContainer(view) {
        val legendLayout = CalendarHeaderLayoutBinding.bind(view).root
    }

    @SuppressLint("SetTextI18n")
    private fun dateBirthUpdateListener() {
        profileVM.updateProfileResponse.observe(viewLifecycleOwner) {
            when(it.status) {
                Status.LOADING -> {
                    btnSaveDate.text = ""
                    calendarView.findViewById<SpinKitView>(R.id.load_save).visibility = View.VISIBLE
                }
                Status.ERROR -> {
                    calendarView.findViewById<SpinKitView>(R.id.load_save).visibility = View.GONE
                    dialogBuilder.dismiss()
                    Toast.makeText(activity, "Failed Update Birth Date", Toast.LENGTH_SHORT).show()
                }
                Status.SUCCESS -> {
                    val dayChoice = if (selectedDate?.dayOfMonth!! < 10) {
                        "0${selectedDate?.dayOfMonth}"
                    }else{
                        selectedDate?.dayOfMonth.toString()
                    }
                    calendarView.findViewById<SpinKitView>(R.id.load_save).visibility = View.GONE
                    dialogBuilder.dismiss()
                    binding.formBirthday.text = "$dayChoice ${Variables.MONTH[monthNumber-1]} $year"
                    Toast.makeText(activity, "Success Update Birth Date", Toast.LENGTH_LONG).show()
                }
            }
        }
    }

    @SuppressLint("SimpleDateFormat")
    private fun convertDate(dateData: String): String {
        val inputFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
        val outputFormat = SimpleDateFormat("dd MMMM yyyy")
        val date: Date = inputFormat.parse(dateData)
        return outputFormat.format(date)
    }
}