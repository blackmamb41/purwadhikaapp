package com.purwadhika.purwadhikaapp.presentation.external

interface NotificationOnClickListener {
    fun onClickNotification()
}