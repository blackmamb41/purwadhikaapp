package com.purwadhika.purwadhikaapp.presentation.ui.topic

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.purwadhika.purwadhikaapp.data.models.response.ProfileDetailResponse
import com.purwadhika.purwadhikaapp.domain.usecases.GetProfileDetailUseCase
import com.purwadhika.purwadhikaapp.utils.EspressoIdlingResource
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import javax.inject.Inject

@HiltViewModel
class TopicVM @Inject constructor(var getProfileDetailUseCase: GetProfileDetailUseCase): ViewModel() {
    var scoreProfile = MutableLiveData<Int>()
    fun checkProfile(id: Int) {
        EspressoIdlingResource.idling()
        viewModelScope.launch(Dispatchers.IO) {
            getProfileDetailUseCase.invokeGetProfileDetail(id).enqueue(object:
                Callback<ProfileDetailResponse> {
                override fun onResponse(
                    call: Call<ProfileDetailResponse>,
                    response: Response<ProfileDetailResponse>
                ) {
                    EspressoIdlingResource.execute()
                    val responseBody = response.body()
                    var score = 0
                    if (response.isSuccessful) {
                        score += if (responseBody?.data?.gender == null || responseBody.data?.gender == "")
                            0
                        else
                            1
                        score += if (responseBody?.data?.address == null || responseBody.data?.address == "")
                            0
                        else
                            1
                        score += if (responseBody?.data?.birthDate == null || responseBody.data?.birthDate == "")
                            0
                        else
                            1
                        scoreProfile.postValue(score)
                    }
                }
                override fun onFailure(call: Call<ProfileDetailResponse>, t: Throwable) {
                    EspressoIdlingResource.execute()
                    t.printStackTrace()
                }
            })
        }
    }
}