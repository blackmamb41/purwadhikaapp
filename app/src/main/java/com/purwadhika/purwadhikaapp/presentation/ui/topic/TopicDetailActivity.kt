package com.purwadhika.purwadhikaapp.presentation.ui.topic

import android.app.AlertDialog
import android.graphics.Color
import android.graphics.PorterDuff
import android.graphics.drawable.ColorDrawable
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.widget.Button
import androidx.activity.viewModels
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.purwadhika.purwadhikaapp.R
import com.purwadhika.purwadhikaapp.data.models.response.ProgramTopicData
import com.purwadhika.purwadhikaapp.data.models.response.ProgramTopicResponse
import com.purwadhika.purwadhikaapp.databinding.ActivityTopicDetailBinding
import com.purwadhika.purwadhikaapp.presentation.routers.BaseRouter
import com.purwadhika.purwadhikaapp.presentation.ui.base.BaseActivity
import com.purwadhika.purwadhikaapp.utils.Config
import com.purwadhika.purwadhikaapp.utils.Variables
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class TopicDetailActivity : BaseActivity() {
    lateinit var binding: ActivityTopicDetailBinding
    private val topicVM by viewModels<TopicVM>()
    private var statusProfile: Boolean = false
    lateinit var dialogPrevent: View
    override fun setupDepedencies() {
        binding = ActivityTopicDetailBinding.inflate(layoutInflater)
        setContentView(binding.root)
    }

    override fun observe() {
        checkProfile()
        btnAction()
        val dataTopic = intent.getSerializableExtra(Variables.PROGRAM_TOPIC) as ProgramTopicData?
        Glide.with(this)
            .load(dataTopic?.image)
            .diskCacheStrategy(DiskCacheStrategy.DATA)
            .into(binding.imageView4)
        Glide.with(this).load(dataTopic?.icon).into(binding.iconTopic)
        binding.titleTopic.text = dataTopic?.title
        binding.description.text = dataTopic?.description
    }

    override fun setupToolbar() {
        setSupportActionBar(binding.toolbarDetailTopic)
        supportActionBar?.setDisplayShowTitleEnabled(false)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        binding.toolbarDetailTopic.navigationIcon?.setColorFilter(resources.getColor(R.color.white), PorterDuff.Mode.SRC_ATOP)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item.itemId) {
            android.R.id.home -> finish()
        }
        return super.onOptionsItemSelected(item)
    }

    private fun checkProfile() {
        Config.getIdUser(this).let {
            topicVM.checkProfile(it)
        }
        topicVM.scoreProfile.observe(this){
            statusProfile = it >= 3
        }
    }

    private fun btnAction() {
        dialogPrevent = layoutInflater.inflate(R.layout.prevent_profile_dialog, null)
        val dialogNotAllowed = AlertDialog.Builder(this)
            .setView(dialogPrevent)
        val dialogWindow = dialogNotAllowed.create()
        dialogWindow.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        binding.btnProgramRegister.setOnClickListener {
            Log.i("TAG", "btnAction: $statusProfile")
            if (statusProfile)
                startActivity(BaseRouter.goToInTakePage(this))
            else {
                dialogWindow.show()
            }
        }
        dialogPrevent.findViewById<Button>(R.id.btn_profile).setOnClickListener {
            dialogWindow.dismiss()
            startActivity(BaseRouter.goToProfilePage(this))
        }
        binding.qna.setOnClickListener {
            startActivity(BaseRouter.goToQnaPage(this))
        }
    }

}