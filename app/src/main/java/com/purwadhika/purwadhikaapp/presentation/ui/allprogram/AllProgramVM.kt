package com.purwadhika.purwadhikaapp.presentation.ui.allprogram

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.purwadhika.purwadhikaapp.data.models.response.ProgramCategoryData
import com.purwadhika.purwadhikaapp.data.models.response.ProgramCategoryResponse
import com.purwadhika.purwadhikaapp.data.models.response.ProgramResponse
import com.purwadhika.purwadhikaapp.data.models.response.Status
import com.purwadhika.purwadhikaapp.domain.usecases.GetProgramCategoryUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import javax.inject.Inject

@HiltViewModel
class AllProgramVM @Inject constructor(var getProgramCategoryUseCase: GetProgramCategoryUseCase): ViewModel() {
    var programCategoryList = MutableLiveData<ProgramCategoryResponse>()
    fun getCategoryProgram() {
        programCategoryList.postValue(
            ProgramCategoryResponse(
                Status.LOADING,
                null,
                null
            )
        )
        viewModelScope.launch(Dispatchers.IO) {
            getProgramCategoryUseCase.getListProgramCategory().enqueue(object: Callback<ProgramCategoryResponse> {
                override fun onResponse(
                    call: Call<ProgramCategoryResponse>,
                    response: Response<ProgramCategoryResponse>
                ) {
                    if (response.isSuccessful)
                        programCategoryList.postValue(
                            ProgramCategoryResponse(
                                Status.SUCCESS,
                                response.body()?.message,
                                response.body()?.data
                            )
                        )
                    else
                        programCategoryList.postValue(
                            ProgramCategoryResponse(
                                Status.ERROR,
                                response.body()?.message,
                                null
                            )
                        )
                }

                override fun onFailure(call: Call<ProgramCategoryResponse>, t: Throwable) {
                    programCategoryList.postValue(
                        ProgramCategoryResponse(
                            Status.ERROR,
                            null,
                            null
                        )
                    )
                }

            })
        }
    }
}