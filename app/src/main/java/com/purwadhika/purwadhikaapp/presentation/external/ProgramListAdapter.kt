package com.purwadhika.purwadhikaapp.presentation.external

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.purwadhika.purwadhikaapp.data.models.response.Program
import com.purwadhika.purwadhikaapp.databinding.CardProgramItemBinding

class ProgramListAdapter(var programClickListener: ProgramClickListener): RecyclerView.Adapter<ProgramListAdapter.ProgramViewHolder>() {
    var listProgram = listOf<Program>()
    inner class ProgramViewHolder(var binding: CardProgramItemBinding): RecyclerView.ViewHolder(binding.root) {
        fun bind(program: Program) {
            binding.title.text = program.title
            binding.schedule.text = program.schedule
//            Glide.with(binding.root.context).load(program.image).into(binding.thumbnailProgram)
            itemView.setOnClickListener {
                programClickListener.onClickProgram(program)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProgramViewHolder {
        val binding = CardProgramItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ProgramViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ProgramViewHolder, position: Int) {
        holder.bind(listProgram[position])
    }

    override fun getItemCount(): Int = listProgram.size

    fun updateProgram(listPrograms: List<Program>) {
        this.listProgram = listPrograms
        notifyDataSetChanged()
    }
}