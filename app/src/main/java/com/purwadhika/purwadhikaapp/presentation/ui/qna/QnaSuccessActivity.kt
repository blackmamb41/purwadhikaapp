package com.purwadhika.purwadhikaapp.presentation.ui.qna

import android.view.MenuItem
import com.purwadhika.purwadhikaapp.databinding.ActivityQnaSuccessBinding
import com.purwadhika.purwadhikaapp.presentation.routers.BaseRouter
import com.purwadhika.purwadhikaapp.presentation.ui.base.BaseActivity
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class QnaSuccessActivity : BaseActivity() {
    lateinit var binding: ActivityQnaSuccessBinding
    override fun setupDepedencies() {
        binding = ActivityQnaSuccessBinding.inflate(layoutInflater)
        setContentView(binding.root)
    }

    override fun observe() {
        btnActionListener()
    }

    override fun setupToolbar() {
        setSupportActionBar(binding.toolbarQnaSuccess)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }

    private fun btnActionListener() {
        application?.applicationContext?.let { context ->
            binding.btnBackToHome.setOnClickListener {
                startActivity(BaseRouter.gotToMainPage(context))
            }
            binding.btnMoreQuestion.setOnClickListener {
                startActivity(BaseRouter.goToQnaPageWithoutBack(context))
            }
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item.itemId) {
            android.R.id.home -> finish()
        }
        return super.onOptionsItemSelected(item)
    }
}