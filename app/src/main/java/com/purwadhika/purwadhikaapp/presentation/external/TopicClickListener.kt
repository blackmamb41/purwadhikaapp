package com.purwadhika.purwadhikaapp.presentation.external

import com.purwadhika.purwadhikaapp.data.models.response.ProgramTopicData

interface TopicClickListener {
    fun onTopicClick(programTopicData: ProgramTopicData)
}