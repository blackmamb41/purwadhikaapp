package com.purwadhika.purwadhikaapp.presentation.ui.register

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.purwadhika.purwadhikaapp.data.models.response.ProfileResponse
import com.purwadhika.purwadhikaapp.data.models.response.Status
import com.purwadhika.purwadhikaapp.domain.usecases.UpdateAvatarProfileUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import okhttp3.MediaType
import okhttp3.MediaType.Companion.toMediaType
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.File
import javax.inject.Inject

@HiltViewModel
class AvatarUploadVM @Inject constructor(var updateAvatarProfileUseCase: UpdateAvatarProfileUseCase): ViewModel() {
    var updateResponse = MutableLiveData<ProfileResponse>()
    fun uploadAvatar(id: Int, pictureProfile: RequestBody) {
        updateResponse.postValue(
            ProfileResponse(
                Status.LOADING,
                null,
                null
            )
        )
        viewModelScope.launch(Dispatchers.IO) {
            updateAvatarProfileUseCase.uploadAvatarImg(id, pictureProfile).enqueue(object: Callback<ProfileResponse> {
                override fun onResponse(
                    call: Call<ProfileResponse>,
                    response: Response<ProfileResponse>
                ) {
                    if (response.isSuccessful)
                        updateResponse.postValue(
                            ProfileResponse(
                                Status.SUCCESS,
                                response.body()?.message,
                                null
                            )
                        )
                    else
                        updateResponse.postValue(
                            ProfileResponse(
                                Status.ERROR,
                                response.body()?.message,
                                response.body()?.error
                            )
                        )
                }

                override fun onFailure(call: Call<ProfileResponse>, t: Throwable) {
                    updateResponse.postValue(
                        ProfileResponse(
                            Status.ERROR,
                            null,
                            null
                        )
                    )
                }

            })
        }
    }
}