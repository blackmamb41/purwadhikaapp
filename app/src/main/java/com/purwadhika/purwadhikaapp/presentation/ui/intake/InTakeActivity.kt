package com.purwadhika.purwadhikaapp.presentation.ui.intake

import android.graphics.Color
import android.graphics.PorterDuff
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import androidx.activity.viewModels
import com.google.android.material.snackbar.Snackbar
import com.purwadhika.purwadhikaapp.R
import com.purwadhika.purwadhikaapp.data.models.response.ProgramCategoryData
import com.purwadhika.purwadhikaapp.data.models.response.ProgramTopicData
import com.purwadhika.purwadhikaapp.databinding.ActivityInTakeBinding
import com.purwadhika.purwadhikaapp.presentation.routers.BaseRouter
import com.purwadhika.purwadhikaapp.presentation.ui.base.BaseActivity
import com.skydoves.powerspinner.PowerSpinnerView
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class InTakeActivity : BaseActivity() {
    lateinit var binding: ActivityInTakeBinding
    lateinit var programSpinner: PowerSpinnerView
    lateinit var topicSpinner: PowerSpinnerView
    private val inTakeVM by viewModels<InTakeVM>()
    private var programList = listOf<ProgramCategoryData>()
    private var topicList = listOf<ProgramTopicData>()
    private var choiceProgramId: Int? = null
    private var choiceTopicId: Int? = null
    override fun setupDepedencies() {
        binding = ActivityInTakeBinding.inflate(layoutInflater)
        setContentView(binding.root)
        programSpinner = binding.programFilter
        topicSpinner = binding.topicFilter
    }

    override fun observe() {
        listenData()
        selectListener()
        inTakeVM.getProgramTopic()
        inTakeVM.getProgramCategory()
        btnAction()
    }

    override fun setupToolbar() {
        setSupportActionBar(binding.toolbarIntake)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        binding.toolbarIntake.navigationIcon?.setColorFilter(resources.getColor(R.color.white), PorterDuff.Mode.SRC_ATOP)
    }

    private fun listenData() {
        inTakeVM.programCategoryData.observe(this) {
            val arrayProgramList = arrayListOf<String>()
            it.data?.forEach { program ->
                program.title?.let { it1 -> arrayProgramList.add(it1) }
            }
            it.data?.let { programs ->
                programList = programs
            }
            programSpinner.setItems(arrayProgramList)
        }
        inTakeVM.programTopicData.observe(this){
            val arrayTopicList = arrayListOf<String>()
            it.data?.forEach { topic ->
                arrayTopicList.add(topic.title)
            }
            it.data?.let { topics ->
                topicList = topics
            }
            topicSpinner.setItems(arrayTopicList)
        }
    }

    private fun selectListener() {
        programSpinner.setOnSpinnerItemSelectedListener<String> { oldIndex, oldItem, newIndex, newItem ->
            choiceProgramId = programList[newIndex].id
        }
        topicSpinner.setOnSpinnerItemSelectedListener<String> { oldIndex, oldItem, newIndex, newItem ->
            choiceTopicId = topicList[newIndex].id
        }
    }

    private fun btnAction() {
        binding.btnContinue.setOnClickListener {
            when {
                choiceProgramId == null -> Snackbar.make(binding.root, "Please Select Program", Snackbar.LENGTH_SHORT)
                    .setBackgroundTint(Color.RED)
                    .setTextColor(Color.WHITE)
                    .show()
                choiceTopicId == null -> Snackbar.make(binding.root, "Please Select Topic", Snackbar.LENGTH_SHORT)
                    .setBackgroundTint(Color.RED)
                    .setTextColor(Color.WHITE)
                    .show()
                else -> startActivity(BaseRouter.goToProgramSchedule(this,
                    choiceProgramId!!, choiceTopicId!!
                ))
            }
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item.itemId) {
            android.R.id.home -> finish()
        }
        return super.onOptionsItemSelected(item)
    }
}