package com.purwadhika.purwadhikaapp.presentation.ui.register

import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.ImageDecoder
import android.net.Uri
import android.os.Build
import android.provider.MediaStore
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.activity.viewModels
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.purwadhika.purwadhikaapp.data.models.response.Status
import com.purwadhika.purwadhikaapp.databinding.ActivityAvatarUploadBinding
import com.purwadhika.purwadhikaapp.presentation.routers.BaseRouter
import com.purwadhika.purwadhikaapp.presentation.ui.base.BaseActivity
import com.purwadhika.purwadhikaapp.utils.Config
import dagger.hilt.android.AndroidEntryPoint
import okhttp3.MediaType
import okhttp3.MediaType.Companion.toMediaType
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.IOException


@AndroidEntryPoint
class AvatarUploadActivity : BaseActivity() {
    lateinit var binding: ActivityAvatarUploadBinding
    private var profilePicture: File? = null
    private val avatarUploadVM by viewModels<AvatarUploadVM>()
    private var picture: MultipartBody.Part? = null
    companion object {
        const val REQUEST_IMAGE_CAPTURE = 1
        const val CODE_REQUEST_CAMERA_PERMISSION = 1
        const val REQUEST_PICK_IMAGE = 2
        const val CODE_REQUEST_EXTERNAL_STORAGE = 2
    }
    override fun setupDepedencies() {
        binding = ActivityAvatarUploadBinding.inflate(layoutInflater)
        setContentView(binding.root)
    }

    override fun observe() {
        binding.btnTakeFromCamera.setOnClickListener {
            checkPermissionCamera()
        }
        binding.btnPickGallery.setOnClickListener {
            checkPermissionExternalStorage()
        }
        binding.btnContinue.setOnClickListener {
            Config.getIdUser(this).let { userId ->
                this.profilePicture?.let { pp ->
                    val body = MultipartBody.Builder().setType(MultipartBody.FORM)
                        .addFormDataPart("profilePicture",
                            pp.name,
                            RequestBody.create("application/octet-stream".toMediaTypeOrNull(), pp)
                        ).build()
                    avatarUploadVM.uploadAvatar(userId, body)
                }
            }
        }
        binding.btnBackToHome.setOnClickListener {
            startActivity(BaseRouter.gotToMainPage(this))
        }
        dataListener()
    }

    override fun setupToolbar() {
        setSupportActionBar(binding.toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowTitleEnabled(false)
    }

    private fun dispatchTakePicture() {
        val cameraIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        startActivityForResult(cameraIntent, REQUEST_IMAGE_CAPTURE)
    }

    private fun dispatchPickPicture() {
        val galleryIntent = Intent(Intent.ACTION_PICK)
        galleryIntent.type = "image/*"
        startActivityForResult(galleryIntent, REQUEST_PICK_IMAGE)
    }

    private fun checkPermissionCamera() {
        if(ContextCompat.checkSelfPermission(
                this,
                android.Manifest.permission.CAMERA,
            ) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(
                this,
                arrayOf(android.Manifest.permission.CAMERA),
                CODE_REQUEST_CAMERA_PERMISSION
            )
        }else{
            dispatchTakePicture()
        }
    }

    private fun checkPermissionExternalStorage() {
        if(ContextCompat.checkSelfPermission(
                this,
                android.Manifest.permission.READ_EXTERNAL_STORAGE,
        ) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(
                this,
                arrayOf(android.Manifest.permission.READ_EXTERNAL_STORAGE),
                CODE_REQUEST_EXTERNAL_STORAGE
            )
        }else{
            dispatchPickPicture()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(resultCode == RESULT_OK) {
            when(requestCode) {
                REQUEST_IMAGE_CAPTURE -> {
                    val imageBitmap = data?.extras?.get("data") as Bitmap
                    val uriBitmap = getImageUri(this, imageBitmap)
                    profilePicture = File(getPath(uriBitmap))
                    binding.photoThumb.visibility = View.GONE
                    binding.bgPhoto.setImageBitmap(imageBitmap)
                }
                REQUEST_PICK_IMAGE -> {
                    val pathPhoto = getPath(data?.data)
                    profilePicture = File(pathPhoto)
                    binding.photoThumb.visibility = View.GONE
                    binding.bgPhoto.setImageURI(data?.data)
                }
            }
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when(requestCode) {
            CODE_REQUEST_CAMERA_PERMISSION -> {
                if(grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    dispatchTakePicture()
                }else{
                    Toast.makeText(this, "Permission Camera Denied", Toast.LENGTH_SHORT).show()
                }
            }
            CODE_REQUEST_EXTERNAL_STORAGE -> {
                if(grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    dispatchPickPicture()
                }else{
                    Toast.makeText(this, "Permission Gallery Denied", Toast.LENGTH_SHORT).show()
                }
            }
        }
    }

    private fun dataListener() {
        avatarUploadVM.updateResponse.observe(this) {
            when(it.status) {
                Status.LOADING -> binding.btnContinue.showLoading()
                Status.SUCCESS -> {
                    startActivity(BaseRouter.gotToMainPage(this))
                }
                Status.ERROR -> {
                    binding.btnContinue.hideLoading()
                    Toast.makeText(this, "Failed Update Avatar", Toast.LENGTH_LONG).show()
                }
            }
        }
    }

    private fun getImageUri(inContext: Context, inImage: Bitmap): Uri? {
        val bytes = ByteArrayOutputStream()
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes)
        val path: String =
            MediaStore.Images.Media.insertImage(inContext.contentResolver, inImage, "pictureProfile", null)
        return Uri.parse(path)
    }

    private fun getPath(uri: Uri?): String? {
        // just some safety built in
        if (uri == null) {
            // TODO perform some logging or show user feedback
            return null
        }
        // try to retrieve the image from the media store first
        // this will only work for images selected from gallery
        val projection = arrayOf(MediaStore.Images.Media.DATA)
        val cursor = contentResolver.query(uri, projection, null, null, null)
        if (cursor != null) {
            val columnIndex = cursor
                .getColumnIndexOrThrow(MediaStore.Images.Media.DATA)
            cursor.moveToFirst()
            val path = cursor.getString(columnIndex)
            cursor.close()
            return path
        }
        // this is our fallback here
        return uri.path
    }

    private fun loadFromUri(photoUri: Uri?): Bitmap? {
        var image: Bitmap? = null
        try {
            // check version of Android on device
            image = if (Build.VERSION.SDK_INT > 27) {
                // on newer versions of Android, use the new decodeBitmap method
                val source: ImageDecoder.Source? =
                    photoUri?.let { ImageDecoder.createSource(this.contentResolver, it) }
                source?.let { ImageDecoder.decodeBitmap(it) }
            } else {
                // support older versions of Android by using getBitmap
                MediaStore.Images.Media.getBitmap(this.contentResolver, photoUri)
            }
        } catch (e: IOException) {
            e.printStackTrace()
        }
        return image
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item.itemId) {
            android.R.id.home -> finish()
        }
        return super.onOptionsItemSelected(item)
    }
}