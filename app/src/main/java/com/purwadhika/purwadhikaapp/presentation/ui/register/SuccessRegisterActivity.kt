package com.purwadhika.purwadhikaapp.presentation.ui.register

import android.content.Intent
import android.os.Handler
import android.os.Looper
import android.view.View
import com.purwadhika.purwadhikaapp.R
import com.purwadhika.purwadhikaapp.databinding.ActivitySuccessRegisterBinding
import com.purwadhika.purwadhikaapp.presentation.routers.BaseRouter
import com.purwadhika.purwadhikaapp.presentation.ui.base.BaseActivity
import com.purwadhika.purwadhikaapp.utils.Variables
import kotlin.properties.Delegates

class SuccessRegisterActivity : BaseActivity() {
    lateinit var binding: ActivitySuccessRegisterBinding
    var isRegister by Delegates.notNull<Boolean>()
    override fun setupDepedencies() {
        binding = ActivitySuccessRegisterBinding.inflate(layoutInflater)
        isRegister = intent.getBooleanExtra(Variables.REGISTER, false)
        setContentView(binding.root)
    }

    override fun observe() {
        if (isRegister)
            Handler(Looper.getMainLooper()).postDelayed({
                val genderPage = BaseRouter.goToGenrePickPage(this, isRegister)
                genderPage.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                startActivity(genderPage)
                finish()
            }, 2000)
        if (!isRegister) {
            binding.btnContinue.visibility = View.VISIBLE
            binding.infoSuccess.setText(R.string.info_success_reset)
        }

        binding.btnContinue.setOnClickListener {
            val loginPage = BaseRouter.goToLoginPage(this)
            loginPage.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
            startActivity(loginPage)
        }
    }

    override fun setupToolbar() {
        setSupportActionBar(binding.toolbarSuccess)
        supportActionBar?.setDisplayShowTitleEnabled(false)
        binding.title.text = if (isRegister) "Your Account is Verified" else "Password Reset"
    }
}