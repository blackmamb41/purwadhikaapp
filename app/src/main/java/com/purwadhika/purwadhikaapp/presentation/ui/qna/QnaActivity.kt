package com.purwadhika.purwadhikaapp.presentation.ui.qna

import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.activity.viewModels
import com.google.android.material.snackbar.Snackbar
import com.purwadhika.purwadhikaapp.R
import com.purwadhika.purwadhikaapp.data.models.request.QnaRequest
import com.purwadhika.purwadhikaapp.data.models.response.Status
import com.purwadhika.purwadhikaapp.databinding.ActivityQnaBinding
import com.purwadhika.purwadhikaapp.presentation.routers.BaseRouter
import com.purwadhika.purwadhikaapp.presentation.ui.base.BaseActivity
import com.purwadhika.purwadhikaapp.utils.Config
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class QnaActivity : BaseActivity() {
    lateinit var binding: ActivityQnaBinding
    private val qnaVM by viewModels<QnaVM>()
    var userId: Int = 0

    override fun setupDepedencies() {
        binding = ActivityQnaBinding.inflate(layoutInflater)
        setContentView(binding.root)
    }

    override fun observe() {
        btnActionListener()
    }

    override fun setupToolbar() {
        setSupportActionBar(binding.toolbarQna)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.qna_menu, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item.itemId) {
            android.R.id.home -> finish()
            R.id.send_qna -> {
                Config.getIdUser(this).let {
                    userId = it
                }
                qnaVM.createNewQna(
                    QnaRequest(
                        userId,
                        binding.questionForm.text.toString()
                    )
                )
            }
            R.id.attachment -> Toast.makeText(this, "Attachment", Toast.LENGTH_SHORT).show()
        }
        return super.onOptionsItemSelected(item)
    }

    private fun btnActionListener() {
        qnaVM.qnaDataResponse.observe(this){
            when(it.status) {
                Status.LOADING -> binding.loadSend.visibility = View.VISIBLE
                Status.ERROR -> {
                    binding.loadSend.visibility = View.GONE
                    Snackbar.make(binding.root, "Cannot Send Question", Snackbar.LENGTH_SHORT).show()
                }
                Status.SUCCESS -> {
                    binding.loadSend.visibility = View.GONE
                    application?.applicationContext?.let { context ->
                        startActivity(BaseRouter.goToQnaSuccessSend(context))
                    }
                }
            }
        }
    }
}