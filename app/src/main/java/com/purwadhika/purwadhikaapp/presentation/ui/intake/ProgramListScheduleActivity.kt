package com.purwadhika.purwadhikaapp.presentation.ui.intake

import android.annotation.SuppressLint
import android.graphics.PorterDuff
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.activity.viewModels
import androidx.core.content.ContentProviderCompat.requireContext
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.chip.Chip
import com.google.android.material.chip.ChipGroup
import com.purwadhika.purwadhikaapp.R
import com.purwadhika.purwadhikaapp.data.models.response.Program
import com.purwadhika.purwadhikaapp.data.models.response.Status
import com.purwadhika.purwadhikaapp.databinding.ActivityProgramListScheduleBinding
import com.purwadhika.purwadhikaapp.presentation.external.ProgramClickListener
import com.purwadhika.purwadhikaapp.presentation.external.ProgramScheduleAdapter
import com.purwadhika.purwadhikaapp.presentation.routers.BaseRouter
import com.purwadhika.purwadhikaapp.presentation.ui.base.BaseActivity
import com.purwadhika.purwadhikaapp.utils.Variables
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class ProgramListScheduleActivity : BaseActivity(), ProgramClickListener {
    lateinit var binding: ActivityProgramListScheduleBinding
    lateinit var programScheduleAdapter: ProgramScheduleAdapter
    lateinit var chipGroup: ChipGroup
    private val programListScheduleVM by viewModels<ProgramListScheduleVM>()
    private var selectProgram: Program? = null
    private var programCategoryId: Int? = null
    private var programTopicId: Int? = null
    override fun setupDepedencies() {
        binding = ActivityProgramListScheduleBinding.inflate(layoutInflater)
        setContentView(binding.root)
    }

    override fun observe() {
        chipGroup = binding.listFilterProgram
        initLoadData()
        setupRVProgram()
        btnAction()
        dataListener()
        programCategoryId?.let { categoryId ->
            programTopicId?.let { topicId ->
                programListScheduleVM.getDataProgramByFilter(categoryId, topicId)
            }
        }
    }

    override fun setupToolbar() {
        setSupportActionBar(binding.toolbarProgramList)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        binding.toolbarProgramList.navigationIcon?.setColorFilter(resources.getColor(R.color.white), PorterDuff.Mode.SRC_ATOP)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.intake_menu, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item.itemId) {
            android.R.id.home -> finish()
        }
        return super.onOptionsItemSelected(item)
    }

    private fun setupRVProgram() {
        programScheduleAdapter = ProgramScheduleAdapter(this)
        binding.rvProgramSchedule.adapter = programScheduleAdapter
        binding.rvProgramSchedule.layoutManager = LinearLayoutManager(applicationContext)
    }

    override fun onClickProgram(program: Program) {
        selectProgram = program
    }

    private fun btnAction() {
        binding.btnContinue.setOnClickListener {
            startActivity(BaseRouter.goToSuccessRegisterProgram(this))
        }
    }

    @SuppressLint("UseCompatLoadingForColorStateLists")
    private fun dataListener() {
        programListScheduleVM.programList.observe(this) {
            when(it.status) {
                Status.LOADING -> binding.loadingData.visibility = View.VISIBLE
                Status.SUCCESS -> {
                    binding.loadingData.visibility = View.GONE
                    it.data?.let { programs ->
                        programScheduleAdapter.updateDataProgram(programs)
                        if (programs.isEmpty())
                            binding.noSchedule.visibility = View.VISIBLE
                        else
                            binding.noSchedule.visibility = View.GONE

                        programs.forEach { programItem ->
                            val chip = Chip(this)
                            chip.text = programItem.programCategory?.title
                            chip.setTextColor(resources.getColor(R.color.primary))
                            chip.setBackgroundColor(resources.getColor(R.color.transparent))
                            chip.chipBackgroundColor = resources.getColorStateList(R.color.transparent)
                            chip.chipStrokeColor = resources.getColorStateList(R.color.primary)
                            chip.chipStrokeWidth = 1F
                            chip.isCheckable = true
                            chip.isFocusable = true
                            chip.isClickable = true

                            chipGroup.addView(chip)
                        }
                    }
                }
                Status.ERROR -> {
                    binding.loadingData.visibility = View.GONE
                    Toast.makeText(this, "Failed Loading Data", Toast.LENGTH_LONG).show()
                }
            }
        }
    }

    private fun initLoadData() {
        programCategoryId = intent.getIntExtra(Variables.PROGRAM_CATEGORY_ID, 0)
        programTopicId = intent.getIntExtra(Variables.PROGRAM_TOPIC_ID, 0)
    }
}