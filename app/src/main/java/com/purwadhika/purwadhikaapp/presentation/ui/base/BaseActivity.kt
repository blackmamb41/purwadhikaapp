package com.purwadhika.purwadhikaapp.presentation.ui.base

import android.os.Bundle
import android.os.PersistableBundle
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity

abstract class BaseActivity: AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setupDepedencies()
        observe()
        setupToolbar()
    }
    abstract fun setupDepedencies()
    abstract fun observe()
    abstract fun setupToolbar()
}