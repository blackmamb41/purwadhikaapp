package com.purwadhika.purwadhikaapp.presentation.ui.register

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Patterns
import android.view.MenuItem
import android.widget.Button
import android.widget.TextView
import com.kusu.loadingbutton.LoadingButton
import com.purwadhika.purwadhikaapp.R
import com.purwadhika.purwadhikaapp.databinding.ActivityRegisterAccountBinding
import com.purwadhika.purwadhikaapp.presentation.routers.BaseRouter
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class RegisterAccountActivity : AppCompatActivity() {
    lateinit var binding: ActivityRegisterAccountBinding
    lateinit var tvFirstName: TextView
    lateinit var tvLastName: TextView
    lateinit var tvEmail: TextView
    lateinit var tvPhoneNumber: TextView
    lateinit var btnContinue: Button
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityRegisterAccountBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setupToolbar()
        tvFirstName = binding.formFirstName
        tvLastName = binding.formLastName
        tvEmail = binding.formEmail
        tvPhoneNumber = binding.formPhoneNumber
        btnContinue = binding.btnContinue
        continueBtnHandler()
    }

    private fun setupToolbar() {
        setSupportActionBar(binding.toolbarRegisterInfo)
        supportActionBar?.setTitle(R.string.register)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item.itemId) {
            android.R.id.home -> finish()
        }
        return super.onOptionsItemSelected(item)
    }

    private fun continueBtnHandler() {
        btnContinue.setOnClickListener {
            when {
                tvFirstName.text.isEmpty() -> binding.formFirstName.error = getString(R.string.error_firstName)
                tvLastName.text.isEmpty() -> binding.formLastName.error = getString(R.string.error_lastName)
                !emailValid(tvEmail.text.toString()) -> binding.formEmail.error = getString(R.string.error_email)
                tvPhoneNumber.text.length < 7 -> binding.formPhoneNumber.error = getString(R.string.error_mobile)
                else -> {
                    startActivity(
                        BaseRouter.goToRegisterPasswordPage(
                            this,
                            tvFirstName.text.toString(),
                            tvLastName.text.toString(),
                            tvPhoneNumber.text.toString(),
                            tvEmail.text.toString(),
                            true
                        )
                    )
                }
            }
        }
    }

    private fun emailValid(email :String) :Boolean {
        return Patterns.EMAIL_ADDRESS.matcher(email).matches()
    }
}