package com.purwadhika.purwadhikaapp.presentation.ui.notification

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.purwadhika.purwadhikaapp.data.models.response.Notification
import com.purwadhika.purwadhikaapp.databinding.FragmentNotificationBinding
import com.purwadhika.purwadhikaapp.presentation.external.NotificationAdapter
import com.purwadhika.purwadhikaapp.presentation.external.NotificationOnClickListener
import com.purwadhika.purwadhikaapp.presentation.routers.BaseRouter

class NotificationFragment: Fragment(), NotificationOnClickListener {
    lateinit var binding: FragmentNotificationBinding
    lateinit var notificationAdapter: NotificationAdapter
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentNotificationBinding.inflate(layoutInflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        setupToolbar()
        setupRVNotif()
        setupDataDump()
        super.onViewCreated(view, savedInstanceState)
    }

    private fun setupToolbar() {
        (activity as AppCompatActivity).setSupportActionBar(binding.toolbarNotification)
        (activity as AppCompatActivity).supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }

    private fun setupRVNotif() {
        notificationAdapter = NotificationAdapter(this)
        binding.rvNotif.adapter = notificationAdapter
        binding.rvNotif.layoutManager = LinearLayoutManager(requireContext())
    }

    override fun onClickNotification() {
        activity?.applicationContext?.let {
            startActivity(BaseRouter.goToMyProgramPage(it))
        }
    }

    private fun setupDataDump() {
        val notifA = Notification("Program kamu telah terdaftar!")
        val notifB = Notification("Selamat datang di Purwadhika")
        val listNotif = listOf(notifA, notifB)
        notificationAdapter.updateDataNotification(listNotif)
    }
}