package com.purwadhika.purwadhikaapp.presentation.ui.program

import android.annotation.SuppressLint
import android.graphics.PorterDuff
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.LinearLayoutManager
import com.purwadhika.purwadhikaapp.R
import com.purwadhika.purwadhikaapp.data.models.response.Program
import com.purwadhika.purwadhikaapp.data.models.response.Status
import com.purwadhika.purwadhikaapp.databinding.ActivityMyProgramBinding
import com.purwadhika.purwadhikaapp.databinding.DialogMyProgramBinding
import com.purwadhika.purwadhikaapp.presentation.external.MyProgramAdapter
import com.purwadhika.purwadhikaapp.presentation.external.ProgramClickListener
import com.purwadhika.purwadhikaapp.presentation.ui.base.BaseActivity
import dagger.hilt.android.AndroidEntryPoint
import java.text.SimpleDateFormat
import java.util.*

@AndroidEntryPoint
class MyProgramActivity : BaseActivity(), ProgramClickListener {
    lateinit var binding: ActivityMyProgramBinding
    lateinit var adapter: MyProgramAdapter
    private val myProgramVM by viewModels<MyProgramVM>()
    lateinit var layoutDialogProgram: View
    lateinit var dialogProgram: AlertDialog
    override fun setupDepedencies() {
        binding = ActivityMyProgramBinding.inflate(layoutInflater)
        setContentView(binding.root)
    }

    @SuppressLint("UseCompatLoadingForDrawables")
    override fun observe() {
        layoutDialogProgram = layoutInflater.inflate(R.layout.dialog_my_program, null)
        dialogProgram = AlertDialog.Builder(this)
            .setView(layoutDialogProgram)
            .create()
        dialogProgram.window?.setBackgroundDrawable(resources.getDrawable(R.drawable.dialog_bg))
        layoutDialogProgram.findViewById<Button>(R.id.btn_back).setOnClickListener {
            dialogProgram.dismiss()
        }
        setupRVMyProgram()
        myProgramVM.getListProgram()
        dataListener()
    }

    override fun setupToolbar() {
        setSupportActionBar(binding.toolbarMyProgram)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        binding.toolbarMyProgram.navigationIcon?.setColorFilter(resources.getColor(R.color.white), PorterDuff.Mode.SRC_ATOP)
    }

    private fun setupRVMyProgram() {
        adapter = MyProgramAdapter(this)
        binding.rvMyProgram.adapter = adapter
        binding.rvMyProgram.layoutManager = LinearLayoutManager(this)
    }

    private fun dataListener() {
        myProgramVM.dataProgram.observe(this) {
            when(it.status) {
                Status.LOADING -> binding.loadProgram.visibility = View.VISIBLE
                Status.ERROR -> {
                    binding.loadProgram.visibility = View.GONE
                    Toast.makeText(this, "Failed Load Data", Toast.LENGTH_LONG).show()
                }
                Status.SUCCESS -> {
                    binding.loadProgram.visibility = View.GONE
                    it.data?.let { programs ->
                        adapter.updateProgramData(programs)
                    }
                }
            }
        }
    }

    @SuppressLint("SetTextI18n")
    override fun onClickProgram(program: Program) {
        dialogProgram.show()
        layoutDialogProgram.findViewById<TextView>(R.id.title_program).text = program.title
        layoutDialogProgram.findViewById<TextView>(R.id.topic_program).text = program.programTopic?.title
        layoutDialogProgram.findViewById<TextView>(R.id.schedule).text = "${convertDate(program.startDate)} - ${convertDate(program.endDate)}"
    }
    @SuppressLint("SimpleDateFormat")
    private fun convertDate(dateString: String): String {
        val inputFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
        val outputFormat = SimpleDateFormat("dd MMMM yyyy")
        val date: Date = inputFormat.parse(dateString)
        return outputFormat.format(date)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item.itemId) {
            android.R.id.home -> finish()
        }
        return super.onOptionsItemSelected(item)
    }

}