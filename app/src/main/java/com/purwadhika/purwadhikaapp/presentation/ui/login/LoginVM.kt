package com.purwadhika.purwadhikaapp.presentation.ui.login

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.purwadhika.purwadhikaapp.data.models.request.LoginRequest
import com.purwadhika.purwadhikaapp.data.models.response.LoginResponse
import com.purwadhika.purwadhikaapp.data.models.response.Status
import com.purwadhika.purwadhikaapp.domain.usecases.LoginUseCase
import com.purwadhika.purwadhikaapp.utils.EspressoIdlingResource
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import javax.inject.Inject

@HiltViewModel
class LoginVM @Inject constructor(var loginUseCase: LoginUseCase): ViewModel() {
    val loginData = MutableLiveData<LoginResponse>()
    fun doLogin(loginRequest: LoginRequest){
        EspressoIdlingResource.idling()
        loginData.postValue(
            LoginResponse(
                Status.LOADING,
                null,
                null
            )
        )
        viewModelScope.launch(Dispatchers.IO) {
            loginUseCase.invokeLogin(loginRequest).enqueue(object: Callback<LoginResponse> {
                override fun onResponse(
                    call: Call<LoginResponse>,
                    response: Response<LoginResponse>
                ) {
                    EspressoIdlingResource.execute()
                    if (response.isSuccessful) {
                        loginData.postValue(
                            LoginResponse(
                                Status.SUCCESS,
                                response.body()?.message,
                                response.body()?.data
                            )
                        )
                    }
                    else {
                        loginData.postValue(
                            LoginResponse(
                                Status.ERROR,
                                response.body()?.message,
                                null
                            )
                        )
                    }
                }

                override fun onFailure(call: Call<LoginResponse>, t: Throwable) {
                    EspressoIdlingResource.execute()
                    loginData.postValue(
                        LoginResponse(
                            Status.ERROR,
                            null,
                            null
                        )
                    )
                    t.printStackTrace()
                }

            })
        }
    }

    fun getLoginLiveData(): LiveData<LoginResponse> {
        return loginData
    }
}