package com.purwadhika.purwadhikaapp.presentation.ui.splash

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Log
import com.purwadhika.purwadhikaapp.databinding.ActivitySplashBinding
import com.purwadhika.purwadhikaapp.presentation.ui.main.MainActivity
import com.purwadhika.purwadhikaapp.presentation.ui.onboarding.OnBoardingActivity
import com.purwadhika.purwadhikaapp.utils.Config

class SplashActivity : AppCompatActivity() {
    lateinit var binding: ActivitySplashBinding
    lateinit var runner: Runnable
    lateinit var handler: Handler
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySplashBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val token = Config.getToken(this)
        delayToMainActivity(token)
    }

    private fun delayToMainActivity(token: String?) {
        val mainIntent = Intent(this, MainActivity::class.java)
        val onBoardIntent = Intent(this, OnBoardingActivity::class.java)
        runner = Runnable {
            kotlin.run {
                startActivity(if(token == null) onBoardIntent else mainIntent)
                finish()
            }
        }
        handler = Handler(Looper.getMainLooper())
        handler.postDelayed(runner, 3000)
    }

    override fun onPause() {
        handler.removeCallbacks(runner)
        super.onPause()
    }

    override fun onResume() {
        handler.postDelayed(runner, 300)
        super.onResume()
    }
}