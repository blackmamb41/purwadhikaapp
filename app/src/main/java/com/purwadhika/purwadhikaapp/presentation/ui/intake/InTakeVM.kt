package com.purwadhika.purwadhikaapp.presentation.ui.intake

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.purwadhika.purwadhikaapp.data.models.response.ProgramCategoryResponse
import com.purwadhika.purwadhikaapp.data.models.response.ProgramTopicResponse
import com.purwadhika.purwadhikaapp.data.models.response.Status
import com.purwadhika.purwadhikaapp.domain.usecases.GetProgramCategoryUseCase
import com.purwadhika.purwadhikaapp.domain.usecases.GetProgramTopicUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import javax.inject.Inject

@HiltViewModel
class InTakeVM @Inject constructor(
    var getProgramCategoryUseCase: GetProgramCategoryUseCase,
    var getProgramTopicUseCase: GetProgramTopicUseCase
): ViewModel() {
    var programCategoryData = MutableLiveData<ProgramCategoryResponse>()
    var programTopicData = MutableLiveData<ProgramTopicResponse>()
    fun getProgramCategory() {
        viewModelScope.launch(Dispatchers.IO) {
            getProgramCategoryUseCase.getListProgramCategory().enqueue(object: Callback<ProgramCategoryResponse> {
                override fun onResponse(
                    call: Call<ProgramCategoryResponse>,
                    response: Response<ProgramCategoryResponse>
                ) {
                    if (response.isSuccessful)
                        programCategoryData.postValue(
                            ProgramCategoryResponse(
                                Status.SUCCESS,
                                response.body()?.message,
                                response.body()?.data
                            )
                        )
                    else
                        programCategoryData.postValue(
                            ProgramCategoryResponse(
                                Status.ERROR,
                                response.body()?.message,
                                null
                            )
                        )
                }

                override fun onFailure(call: Call<ProgramCategoryResponse>, t: Throwable) {
                    programCategoryData.postValue(
                        ProgramCategoryResponse(
                            Status.ERROR,
                            null,
                            null
                        )
                    )
                }

            })
        }
    }

    fun getProgramTopic() {
        viewModelScope.launch(Dispatchers.IO) {
            getProgramTopicUseCase.getListProgramTopic().enqueue(object: Callback<ProgramTopicResponse> {
                override fun onResponse(
                    call: Call<ProgramTopicResponse>,
                    response: Response<ProgramTopicResponse>
                ) {
                    if (response.isSuccessful)
                        programTopicData.postValue(
                            ProgramTopicResponse(
                                Status.SUCCESS,
                                response.body()?.message,
                                response.body()?.data
                            )
                        )
                    else
                        programTopicData.postValue(
                            ProgramTopicResponse(
                                Status.ERROR,
                                response.body()?.message,
                                null
                            )
                        )
                }

                override fun onFailure(call: Call<ProgramTopicResponse>, t: Throwable) {
                    programTopicData.postValue(
                        ProgramTopicResponse(
                            Status.ERROR,
                            null,
                            null
                        )
                    )
                }

            })
        }
    }
}