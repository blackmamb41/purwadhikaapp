package com.purwadhika.purwadhikaapp.presentation.ui.register

import android.graphics.Color
import android.util.Log
import android.view.MenuItem
import android.view.View
import androidx.activity.viewModels
import com.google.android.material.snackbar.Snackbar
import com.purwadhika.purwadhikaapp.R
import com.purwadhika.purwadhikaapp.data.models.response.Status
import com.purwadhika.purwadhikaapp.databinding.ActivityGenreVerificationBinding
import com.purwadhika.purwadhikaapp.presentation.routers.BaseRouter
import com.purwadhika.purwadhikaapp.presentation.ui.base.BaseActivity
import com.purwadhika.purwadhikaapp.utils.Config
import com.purwadhika.purwadhikaapp.utils.Variables
import dagger.hilt.android.AndroidEntryPoint
import kotlin.properties.Delegates

@AndroidEntryPoint
class GenreVerificationActivity : BaseActivity() {
    lateinit var binding: ActivityGenreVerificationBinding
    private var selectGenre: Boolean? = null
    private val genreVerificationVM by viewModels<GenderVerificationVM>()
    private var isRegister by Delegates.notNull<Boolean>()
    private var userId: Int? = null
    override fun setupDepedencies() {
        binding = ActivityGenreVerificationBinding.inflate(layoutInflater)
        isRegister = intent.getBooleanExtra(Variables.REGISTER, false)
        setContentView(binding.root)
    }

    override fun observe() {
        application?.applicationContext?.let {
            userId = Config.getIdUser(it)
        }
        dataListener()
        binding.maleChoice.setOnClickListener {
            selectGenre = true
            binding.bgMale.setImageResource(R.drawable.rectangle_green)
            binding.imageMale.setImageResource(R.drawable.male_selected)
            binding.bgFemale.setImageResource(R.drawable.rectangle)
            binding.imageFemale.setImageResource(R.drawable.women_unselected)
        }
        binding.femaleChoice.setOnClickListener {
            selectGenre = false
            binding.bgMale.setImageResource(R.drawable.rectangle)
            binding.imageMale.setImageResource(R.drawable.man_noselected)
            binding.bgFemale.setImageResource(R.drawable.rectangle_green)
            binding.imageFemale.setImageResource(R.drawable.female_selected)
        }
        binding.btnContinue.setOnClickListener {
            if (selectGenre == null)
                Snackbar.make(binding.root, "Please Select One Off Gender", Snackbar.LENGTH_LONG)
                    .setBackgroundTint(Color.RED)
                    .show()
            else
                userId.let { id ->
                    Log.i("TAG", "userId: $id")
                    if (id != null) {
                        genreVerificationVM.doUpdateGender(
                            id,
                            gender = if (selectGenre == true) "male" else "female"
                        )
                    }
                }
        }
        if (isRegister) {
            binding.btnBackToHome.visibility = View.VISIBLE
            binding.btnBackToHome.setOnClickListener {
                startActivity(BaseRouter.gotToMainPage(this))
            }
        }else
            binding.btnBackToHome.visibility = View.GONE

        binding.btnBackToHome.setOnClickListener {
            startActivity(BaseRouter.gotToMainPage(this))
        }
    }

    override fun setupToolbar() {
        setSupportActionBar(binding.toolbarGenre)
        supportActionBar?.setDisplayShowTitleEnabled(false)
        supportActionBar?.setDisplayHomeAsUpEnabled(!isRegister)
    }

    private fun dataListener() {
        genreVerificationVM.genderData.observe(this) {
            when(it.status) {
                Status.LOADING -> {
                    binding.btnContinue.text = ""
                    binding.loadGender.visibility = View.VISIBLE
                }
                Status.SUCCESS -> {
                    binding.btnContinue.setText(R.string.continue_text)
                    binding.loadGender.visibility = View.GONE
                    if (isRegister)
                        startActivity(BaseRouter.goToBirthPage(this, isRegister))
                    else
                        finish()
                }
                Status.ERROR -> {
                    binding.btnContinue.setText(R.string.continue_text)
                    binding.loadGender.visibility = View.GONE
                    Snackbar.make(binding.root, "Cannot Update Something Wrong", Snackbar.LENGTH_LONG)
                        .setBackgroundTint(Color.RED)
                        .show()
                }
            }
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item.itemId) {
            android.R.id.home -> finish()
        }
        return super.onOptionsItemSelected(item)
    }

}