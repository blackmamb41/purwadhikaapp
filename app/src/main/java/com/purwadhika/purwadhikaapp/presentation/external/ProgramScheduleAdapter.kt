package com.purwadhika.purwadhikaapp.presentation.external

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.purwadhika.purwadhikaapp.data.models.response.Program
import com.purwadhika.purwadhikaapp.databinding.CardProgramScheduleItemBinding
import java.text.SimpleDateFormat
import java.util.*

class ProgramScheduleAdapter (var programClickListener: ProgramClickListener): RecyclerView.Adapter<ProgramScheduleAdapter.ProgramScheduleViewHolder>() {
    var programListSchedule = listOf<Program>()
    inner class ProgramScheduleViewHolder(var binding: CardProgramScheduleItemBinding): RecyclerView.ViewHolder(binding.root) {
        @SuppressLint("SetTextI18n")
        fun bind(program: Program) {
            binding.titleProgram.text = program.title
            binding.topicProgram.text = program.programTopic?.title
            binding.location.text = " | ${program.location}"
            binding.date.text = "${convertDate(program.startDate)} - ${convertDate(program.endDate)}"
            binding.schedule.text = program.schedule
            itemView.setOnClickListener {
                programClickListener.onClickProgram(program)
            }
        }
    }

    @SuppressLint("SimpleDateFormat")
    private fun convertDate(dateInput: String): String {
        val inputFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
        val outputFormat = SimpleDateFormat("dd MMM yyyy")
        val date: Date = inputFormat.parse(dateInput)
        return outputFormat.format(date)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProgramScheduleViewHolder {
        val binding = CardProgramScheduleItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ProgramScheduleViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ProgramScheduleViewHolder, position: Int) {
        return holder.bind(programListSchedule[position])
    }

    override fun getItemCount(): Int =programListSchedule.size

    fun updateDataProgram(listProgram: List<Program>) {
        programListSchedule = listProgram
        notifyDataSetChanged()
    }
}