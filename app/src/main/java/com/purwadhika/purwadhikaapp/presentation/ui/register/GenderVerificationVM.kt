package com.purwadhika.purwadhikaapp.presentation.ui.register

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.purwadhika.purwadhikaapp.data.models.request.ProfileRequest
import com.purwadhika.purwadhikaapp.data.models.response.ProfileResponse
import com.purwadhika.purwadhikaapp.data.models.response.Status
import com.purwadhika.purwadhikaapp.domain.usecases.UpdateGenderUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import javax.inject.Inject

@HiltViewModel
class GenderVerificationVM @Inject constructor(var updateGenderUseCase: UpdateGenderUseCase): ViewModel() {
    var genderData = MutableLiveData<ProfileResponse>()
    fun doUpdateGender(id: Int, gender: String) {
        genderData.postValue(
            ProfileResponse(
                Status.LOADING,
                null,
                null
            )
        )
        viewModelScope.launch(Dispatchers.IO) {
            updateGenderUseCase.updateGenderProfile(id, gender).enqueue(object: Callback<ProfileResponse> {
                override fun onResponse(
                    call: Call<ProfileResponse>,
                    response: Response<ProfileResponse>
                ) {
                    Log.i("TAG", "onResponse: ${response.headers()}")
                    if (response.isSuccessful)
                        genderData.postValue(
                            ProfileResponse(
                                Status.SUCCESS,
                                response.body()?.message,
                                null
                            )
                        )
                    else
                        genderData.postValue(
                            ProfileResponse(
                                Status.ERROR,
                                response.body()?.message,
                                response.body()?.error
                            )
                        )
                }

                override fun onFailure(call: Call<ProfileResponse>, t: Throwable) {
                    genderData.postValue(
                        ProfileResponse(
                            Status.ERROR,
                            null,
                            null
                        )
                    )
                    t.printStackTrace()
                }

            })
        }
    }
}