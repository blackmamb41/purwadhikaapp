package com.purwadhika.purwadhikaapp.presentation.routers

import android.content.Context
import android.content.Intent
import com.purwadhika.purwadhikaapp.data.models.response.Program
import com.purwadhika.purwadhikaapp.data.models.response.ProgramCategoryData
import com.purwadhika.purwadhikaapp.data.models.response.ProgramTopicData
import com.purwadhika.purwadhikaapp.presentation.ui.forget_password.ForgetPasswordActivity
import com.purwadhika.purwadhikaapp.presentation.ui.intake.InTakeActivity
import com.purwadhika.purwadhikaapp.presentation.ui.intake.ProgramListScheduleActivity
import com.purwadhika.purwadhikaapp.presentation.ui.intake.SuccessRegisterInTakeActivity
import com.purwadhika.purwadhikaapp.presentation.ui.main.MainActivity
import com.purwadhika.purwadhikaapp.presentation.ui.login.LoginActivity
import com.purwadhika.purwadhikaapp.presentation.ui.profile.ProfileActivity
import com.purwadhika.purwadhikaapp.presentation.ui.program.MyProgramActivity
import com.purwadhika.purwadhikaapp.presentation.ui.program.ProgramActivity
import com.purwadhika.purwadhikaapp.presentation.ui.qna.QnaActivity
import com.purwadhika.purwadhikaapp.presentation.ui.qna.QnaSuccessActivity
import com.purwadhika.purwadhikaapp.presentation.ui.register.*
import com.purwadhika.purwadhikaapp.presentation.ui.topic.TopicDetailActivity
import com.purwadhika.purwadhikaapp.utils.Config
import com.purwadhika.purwadhikaapp.utils.Variables

object BaseRouter {
    fun gotToMainPage(context: Context): Intent {
        val goToMainPage = Intent(context, MainActivity::class.java)
        goToMainPage.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        return goToMainPage
    }

    fun goToLoginPage(context: Context): Intent {
        return Intent(context, LoginActivity::class.java)
    }

    fun goToRegisterPage(context: Context): Intent {
        return Intent(context, RegisterAccountActivity::class.java)
    }

    fun goToRegisterPasswordPage(
        context: Context,
        firstName: String?,
        lastName: String?,
        phoneNumber: String?,
        email: String,
        isRegister: Boolean)
    : Intent {
        val registerPasswordPage = Intent(context, RegisterPasswordActivity::class.java)
        registerPasswordPage.putExtra(Variables.FIRST_NAME, firstName)
        registerPasswordPage.putExtra(Variables.LAST_NAME, lastName)
        registerPasswordPage.putExtra(Variables.PHONE_NUMBER, phoneNumber)
        registerPasswordPage.putExtra(Variables.EMAIL, email)
        registerPasswordPage.putExtra(Variables.REGISTER, isRegister)

        return registerPasswordPage
    }

    fun goToVerifyPage(context: Context, email: String, password: String, isRegister: Boolean): Intent {
        val verifyPage = Intent(context, RegisterVerificationActivity::class.java)
        verifyPage.putExtra(Variables.EMAIL, email)
        verifyPage.putExtra(Variables.PASSWORD, password)
        verifyPage.putExtra(Variables.REGISTER, isRegister)
        verifyPage.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK

        return verifyPage
    }

    fun goToGenrePickPage(context: Context, isRegister: Boolean): Intent {
        val genrePage = Intent(context, GenreVerificationActivity::class.java)
        genrePage.putExtra(Variables.REGISTER, isRegister)
        return genrePage
    }

    fun goToSuccessPage(context: Context, isRegister: Boolean): Intent {
        val successPage = Intent(context, SuccessRegisterActivity::class.java)
        successPage.putExtra(Variables.REGISTER, isRegister)
        successPage.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        return successPage
    }

    fun goToMapLocation(context: Context, isRegister: Boolean): Intent {
        val mapPage = Intent(context, MapSelectorActivity::class.java)
        mapPage.putExtra(Variables.REGISTER, isRegister)
        return mapPage
    }

    fun goToForgotPasswordPage(context: Context): Intent {
        return Intent(context, ForgetPasswordActivity::class.java)
    }

    fun goToBirthPage(context: Context, isRegister: Boolean): Intent {
        val calendarPage = Intent(context, BirthVerificationActivity::class.java)
        calendarPage.putExtra(Variables.REGISTER, isRegister)
        return calendarPage
    }

    fun goToQnaPage(context: Context): Intent {
        return Intent(context, QnaActivity::class.java)
    }

    fun goToQnaPageWithoutBack(context: Context): Intent {
        val qnaPage = Intent(context, QnaActivity::class.java)
        qnaPage.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK
        return qnaPage
    }

    fun goToQnaSuccessSend(context: Context): Intent {
        val qnaSuccessIntent = Intent(context, QnaSuccessActivity::class.java)
        qnaSuccessIntent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
        return qnaSuccessIntent
    }

    fun goToAvatarUpload(context: Context, isRegister: Boolean): Intent {
        val avatarPage = Intent(context, AvatarUploadActivity::class.java)
        avatarPage.putExtra(Variables.REGISTER, isRegister)
        return avatarPage
    }

    fun goToInTakePage(context: Context): Intent {
        return Intent(context, InTakeActivity::class.java)
    }

    fun goToSuccessRegisterProgram(context: Context): Intent {
        val successRegisterProgram = Intent(context, SuccessRegisterInTakeActivity::class.java)
        successRegisterProgram.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        return successRegisterProgram
    }

    fun goToProgramSchedule(context: Context, programCategoryId: Int, programTopicId: Int): Intent {
        val programSchedulePage = Intent(context, ProgramListScheduleActivity::class.java)
        programSchedulePage.putExtra(Variables.PROGRAM_CATEGORY_ID, programCategoryId)
        programSchedulePage.putExtra(Variables.PROGRAM_TOPIC_ID, programTopicId)
        return programSchedulePage
    }

    fun goToProgramPage(context: Context, programData: Program?, isCategory: Boolean?): Intent {
        val programPage = Intent(context, ProgramActivity::class.java)
        programPage.putExtra(Variables.PROGRAM_DATA, programData)
        programPage.putExtra(Variables.IS_CATEGORY, isCategory)
        return programPage
    }

    fun goToProgramTopicPage(context: Context, topic: ProgramTopicData): Intent {
        val topicPage = Intent(context, TopicDetailActivity::class.java)
        topicPage.putExtra(Variables.PROGRAM_TOPIC, topic)
        return topicPage
    }

    fun goToProfilePage(context: Context): Intent {
        return Intent(context, ProfileActivity::class.java)
    }

    fun goToProgramCategoryPage(context: Context, programCategoryData: ProgramCategoryData): Intent {
        val programPage = Intent(context, ProgramActivity::class.java)
        programPage.putExtra(Variables.PROGRAM_CATEGORY_DATA, programCategoryData)
        return programPage
    }

    fun goToMyProgramPage(context: Context): Intent {
        return Intent(context, MyProgramActivity::class.java)
    }
}