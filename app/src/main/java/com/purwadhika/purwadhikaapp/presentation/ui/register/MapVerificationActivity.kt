package com.purwadhika.purwadhikaapp.presentation.ui.register

import `in`.madapps.placesautocomplete.PlaceAPI
import `in`.madapps.placesautocomplete.adapter.PlacesAutoCompleteAdapter
import `in`.madapps.placesautocomplete.listener.OnPlacesDetailsListener
import `in`.madapps.placesautocomplete.model.PlaceDetails
import android.util.Log
import android.view.MenuItem
import android.widget.AdapterView
import android.widget.Button
import com.google.android.libraries.places.api.model.Place
import com.purwadhika.purwadhikaapp.R
import com.purwadhika.purwadhikaapp.databinding.ActivityMapVerificationBinding
import com.purwadhika.purwadhikaapp.presentation.routers.BaseRouter
import com.purwadhika.purwadhikaapp.presentation.ui.base.BaseActivity
import com.purwadhika.purwadhikaapp.utils.Variables
import kotlin.properties.Delegates

class MapVerificationActivity : BaseActivity() {
    lateinit var binding: ActivityMapVerificationBinding
    lateinit var btnSelectMap: Button
    var isRegister by Delegates.notNull<Boolean>()

    override fun setupDepedencies() {
        binding = ActivityMapVerificationBinding.inflate(layoutInflater)
        setContentView(binding.root)
        btnSelectMap = binding.selectMapBtn
        isRegister = intent.getBooleanExtra(Variables.REGISTER, false)
    }

    override fun observe() {
//        initAutoCompletePlace()
        btnSelectMap.setOnClickListener {
            startActivity(BaseRouter.goToMapLocation(this, isRegister))
        }
    }

    override fun setupToolbar() {
        setSupportActionBar(binding.toolbarMap)
        supportActionBar?.setTitle(R.string.complete_profile)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item.itemId) {
            android.R.id.home -> finish()
        }
        return super.onOptionsItemSelected(item)
    }

    private fun initAutoCompletePlace() {
        val searchLocation = PlaceAPI.Builder().apiKey(getString(R.string.map_api_key)).build(this)
        binding.inputLocation.setAdapter(PlacesAutoCompleteAdapter(this, searchLocation))
        binding.inputLocation.setOnClickListener {
            AdapterView.OnItemClickListener { parent, view, position, id ->
                val place = parent.getItemAtPosition(position) as Place
                binding.inputLocation.setText(place.address)
            }
        }
        searchLocation.fetchPlaceDetails("placeId", object: OnPlacesDetailsListener {
            override fun onError(errorMessage: String) {
                Log.i("TAG", "onError: $errorMessage")
            }

            override fun onPlaceDetailsFetched(placeDetails: PlaceDetails) {
                TODO("Not yet implemented")
            }

        })
    }
}