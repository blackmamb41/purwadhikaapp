package com.purwadhika.purwadhikaapp.presentation.ui.allprogram

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.Navigation
import androidx.navigation.fragment.NavHostFragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.purwadhika.purwadhikaapp.R
import com.purwadhika.purwadhikaapp.data.models.response.ProgramCategoryData
import com.purwadhika.purwadhikaapp.databinding.FragmentAllProgramBinding
import com.purwadhika.purwadhikaapp.presentation.external.ProgramCategoryAdapter
import com.purwadhika.purwadhikaapp.presentation.external.ProgramCategoryOnClickListener
import com.purwadhika.purwadhikaapp.presentation.routers.BaseRouter
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class AllProgramFragment : Fragment(), ProgramCategoryOnClickListener {
    lateinit var binding: FragmentAllProgramBinding
    lateinit var programCategoryAdapter: ProgramCategoryAdapter
    private val allProgramVM by viewModels<AllProgramVM>()
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentAllProgramBinding.inflate(layoutInflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        programCategoryAdapter = ProgramCategoryAdapter(this)
        binding.rvProgramCategory.adapter = programCategoryAdapter
        binding.rvProgramCategory.layoutManager = LinearLayoutManager(requireContext())
        allProgramVM.getCategoryProgram()
        dataListener()
        setupToolbar()
        super.onViewCreated(view, savedInstanceState)
    }

    override fun onClickProgramCategory(programCategoryData: ProgramCategoryData) {
        activity?.applicationContext?.let {
            startActivity(BaseRouter.goToProgramCategoryPage(it, programCategoryData))
        }
    }

    private fun dataListener() {
        allProgramVM.programCategoryList.observe(viewLifecycleOwner) {
            it.data?.let { category ->
                programCategoryAdapter.updateDataProgramCategory(category)
            }
        }
    }

    private fun setupToolbar() {
        (activity as AppCompatActivity).setSupportActionBar(binding.toolbarAllProgram)
        (activity as AppCompatActivity).supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }

}