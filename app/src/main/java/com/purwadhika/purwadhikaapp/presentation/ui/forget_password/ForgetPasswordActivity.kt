package com.purwadhika.purwadhikaapp.presentation.ui.forget_password

import android.graphics.Color
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.widget.Button
import android.widget.TextView
import androidx.activity.viewModels
import com.google.android.material.snackbar.Snackbar
import com.kusu.loadingbutton.LoadingButton
import com.purwadhika.purwadhikaapp.data.models.request.ForgotPasswordRequest
import com.purwadhika.purwadhikaapp.data.models.response.Status
import com.purwadhika.purwadhikaapp.databinding.ActivityForgetPasswordBinding
import com.purwadhika.purwadhikaapp.presentation.routers.BaseRouter
import com.purwadhika.purwadhikaapp.presentation.ui.base.BaseActivity
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class ForgetPasswordActivity : BaseActivity() {
    private lateinit var binding: ActivityForgetPasswordBinding
    private lateinit var inputEmail: TextView
    private lateinit var btnReset: Button
    private val forgotPasswordVM by viewModels<ForgotPasswordVM>()
    override fun setupDepedencies() {
        binding = ActivityForgetPasswordBinding.inflate(layoutInflater)
        setContentView(binding.root)
        inputEmail = binding.formEmail
        btnReset = binding.btnForgotPass
        btnForgetListener()
    }

    override fun observe() {
        dataListener()
    }

    override fun setupToolbar() {
        setSupportActionBar(binding.toolbar2)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }

    private fun btnForgetListener() {
        btnReset.setOnClickListener {
            forgotPasswordVM.doForgotPassword(
                ForgotPasswordRequest(
                    inputEmail.text.toString()
                )
            )
        }
    }

    private fun dataListener() {
        forgotPasswordVM.forgotPasswordData.observe(this) {
            when(it.status) {
                Status.LOADING -> binding.loadForgotPass.visibility = View.VISIBLE
                Status.ERROR -> {
                    binding.loadForgotPass.visibility = View.GONE
                    Snackbar.make(binding.root, "Something Wrong", Snackbar.LENGTH_SHORT)
                        .setBackgroundTint(Color.RED)
                        .show()
                }
                Status.SUCCESS -> startActivity(BaseRouter.goToRegisterPasswordPage(
                    this,
                    null,
                    null,
                    null,
                    inputEmail.text.toString(),
                    false))
            }
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item.itemId) {
            android.R.id.home -> finish()
        }
        return super.onOptionsItemSelected(item)
    }

}