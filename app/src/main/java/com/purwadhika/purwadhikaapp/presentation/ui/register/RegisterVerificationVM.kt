package com.purwadhika.purwadhikaapp.presentation.ui.register

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.purwadhika.purwadhikaapp.data.models.request.LoginRequest
import com.purwadhika.purwadhikaapp.data.models.request.VerifyForgotPasswordRequest
import com.purwadhika.purwadhikaapp.data.models.request.VerifyRequest
import com.purwadhika.purwadhikaapp.data.models.response.LoginResponse
import com.purwadhika.purwadhikaapp.data.models.response.Status
import com.purwadhika.purwadhikaapp.data.models.response.VerifyResponse
import com.purwadhika.purwadhikaapp.domain.usecases.ForgetPasswordVerifyUseCase
import com.purwadhika.purwadhikaapp.domain.usecases.LoginAfterVerifyUseCase
import com.purwadhika.purwadhikaapp.domain.usecases.VerificationAccountUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import javax.inject.Inject

@HiltViewModel
class RegisterVerificationVM @Inject constructor(
    var verificationAccountUseCase: VerificationAccountUseCase,
    var loginAfterVerifyUseCase: LoginAfterVerifyUseCase,
    var forgetPasswordVerifyUseCase: ForgetPasswordVerifyUseCase
): ViewModel() {
    var verificationDataResponse = MutableLiveData<VerifyResponse>()
    var loginDataResponse = MutableLiveData<LoginResponse>()
    var verifificationForgetPasswordResponse = MutableLiveData<VerifyResponse>()

    fun doVerifyForgotPassword(verifyForgotPasswordRequest: VerifyForgotPasswordRequest) {
        verifificationForgetPasswordResponse.postValue(
            VerifyResponse(
                Status.LOADING,
                null
            )
        )
        viewModelScope.launch(Dispatchers.IO) {
            forgetPasswordVerifyUseCase.verifyForgotPassword(verifyForgotPasswordRequest).enqueue(object: Callback<VerifyResponse> {
                override fun onResponse(
                    call: Call<VerifyResponse>,
                    response: Response<VerifyResponse>
                ) {
                    if (response.isSuccessful)
                        verifificationForgetPasswordResponse.postValue(
                            VerifyResponse(
                                Status.SUCCESS,
                                response.body()?.message
                            )
                        )
                    else
                        verifificationForgetPasswordResponse.postValue(
                            VerifyResponse(
                                Status.ERROR,
                                response.body()?.message
                            )
                        )
                }

                override fun onFailure(call: Call<VerifyResponse>, t: Throwable) {
                    verifificationForgetPasswordResponse.postValue(
                        VerifyResponse(
                            Status.ERROR,
                            null
                        )
                    )
                }
            })
        }
    }

    fun doVerify(verifyRequest: VerifyRequest) {
        verificationDataResponse.postValue(
            VerifyResponse(
                Status.LOADING,
                null,
            )
        )
        viewModelScope.launch(Dispatchers.IO) {
            verificationAccountUseCase.invokeVerification(verifyRequest).enqueue(object: Callback<VerifyResponse> {
                override fun onResponse(
                    call: Call<VerifyResponse>,
                    response: Response<VerifyResponse>
                ) {
                    if (response.isSuccessful)
                        verificationDataResponse.postValue(
                            VerifyResponse(
                                Status.SUCCESS,
                                response.body()?.message
                            )
                        )
                    else
                        verificationDataResponse.postValue(
                            VerifyResponse(
                                Status.ERROR,
                                response.body()?.message
                            )
                        )
                }

                override fun onFailure(call: Call<VerifyResponse>, t: Throwable) {
                    verificationDataResponse.postValue(
                        VerifyResponse(
                            Status.ERROR,
                            null
                        )
                    )
                    t.printStackTrace()
                }

            })
        }
    }
    fun doLoginAfterVerify(loginRequest: LoginRequest) {
        loginDataResponse.postValue(
            LoginResponse(
                Status.LOADING,
                null,
                null
            )
        )
        viewModelScope.launch(Dispatchers.IO) {
            loginAfterVerifyUseCase.invokeLoginAfterVerify(loginRequest).enqueue(object: Callback<LoginResponse> {
                override fun onResponse(
                    call: Call<LoginResponse>,
                    response: Response<LoginResponse>
                ) {
                    if (response.isSuccessful)
                        loginDataResponse.postValue(
                            LoginResponse(
                                Status.SUCCESS,
                                response.body()?.message,
                                response.body()?.data
                            )
                        )
                    else
                        loginDataResponse.postValue(
                            LoginResponse(
                                Status.ERROR,
                                response.body()?.message,
                                null
                            )
                        )
                }

                override fun onFailure(call: Call<LoginResponse>, t: Throwable) {
                    loginDataResponse.postValue(
                        LoginResponse(
                            Status.ERROR,
                            null,
                            null
                        )
                    )
                }

            })
        }
    }
}