package com.purwadhika.purwadhikaapp.presentation.ui.qna

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.purwadhika.purwadhikaapp.data.models.request.QnaRequest
import com.purwadhika.purwadhikaapp.data.models.response.QnaResponse
import com.purwadhika.purwadhikaapp.data.models.response.Status
import com.purwadhika.purwadhikaapp.domain.usecases.CreateQnaUseCase
import com.purwadhika.purwadhikaapp.utils.EspressoIdlingResource
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import javax.inject.Inject

@HiltViewModel
class QnaVM @Inject constructor(var createQnaUseCase: CreateQnaUseCase): ViewModel() {
    val qnaDataResponse = MutableLiveData<QnaResponse>()
    fun createNewQna(qnaRequest: QnaRequest) {
        EspressoIdlingResource.idling()
        qnaDataResponse.postValue(
            QnaResponse(
                Status.LOADING,
                null,
                null
            )
        )
        viewModelScope.launch(Dispatchers.IO) {
            createQnaUseCase.createQna(qnaRequest).enqueue(object: Callback<QnaResponse> {
                override fun onResponse(call: Call<QnaResponse>, response: Response<QnaResponse>) {
                    EspressoIdlingResource.execute()
                    if(response.isSuccessful)
                        qnaDataResponse.postValue(
                            QnaResponse(
                                Status.SUCCESS,
                                response.body()?.message,
                                response.body()?.data
                            )
                        )
                    else
                        qnaDataResponse.postValue(
                            QnaResponse(
                                Status.ERROR,
                                response.body()?.message,
                                null
                            )
                        )
                }

                override fun onFailure(call: Call<QnaResponse>, t: Throwable) {
                    EspressoIdlingResource.execute()
                    qnaDataResponse.postValue(
                        QnaResponse(
                            Status.ERROR,
                            null,
                            null
                        )
                    )
                    t.printStackTrace()
                }

            })
        }
    }
}