package com.purwadhika.purwadhikaapp.presentation.ui.register

import android.graphics.Color
import android.util.Log
import android.view.MenuItem
import androidx.activity.viewModels
import com.google.android.material.snackbar.Snackbar
import com.mukesh.OtpView
import com.purwadhika.purwadhikaapp.R
import com.purwadhika.purwadhikaapp.data.models.request.LoginRequest
import com.purwadhika.purwadhikaapp.data.models.request.VerifyForgotPasswordRequest
import com.purwadhika.purwadhikaapp.data.models.request.VerifyRequest
import com.purwadhika.purwadhikaapp.data.models.response.Status
import com.purwadhika.purwadhikaapp.databinding.ActivityRegisterVerificationBinding
import com.purwadhika.purwadhikaapp.presentation.routers.BaseRouter
import com.purwadhika.purwadhikaapp.presentation.ui.base.BaseActivity
import com.purwadhika.purwadhikaapp.utils.Config
import com.purwadhika.purwadhikaapp.utils.Variables
import dagger.hilt.android.AndroidEntryPoint
import kotlin.properties.Delegates

@AndroidEntryPoint
class RegisterVerificationActivity : BaseActivity() {
    lateinit var binding: ActivityRegisterVerificationBinding
    lateinit var otpView: OtpView
    var isRegister by Delegates.notNull<Boolean>()
    private var email: String? = null
    private var password: String? = null
    private var otpNumber: String? = null
    private val registerVerificationVM by viewModels<RegisterVerificationVM>()
    override fun setupDepedencies() {
        binding = ActivityRegisterVerificationBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setupToolbar()
        email = intent.getStringExtra(Variables.EMAIL)
        password = intent.getStringExtra(Variables.PASSWORD)
        otpView = binding.otpView
        isRegister = intent.getBooleanExtra(Variables.REGISTER, false)
    }

    override fun observe() {
        otpListener()
        dataListener()
    }

    override fun setupToolbar() {
        setSupportActionBar(binding.toolbarRegisterVerification)
        supportActionBar?.setTitle(R.string.verification)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item.itemId) {
            android.R.id.home -> finish()
        }
        return super.onOptionsItemSelected(item)
    }

    private fun otpListener() {
        otpView.setOtpCompletionListener { otp ->
            otpNumber = otp.toString()
            doVerify()
            Log.i("TAG", "otpListener: $email dan $otpNumber")
        }

        binding.btnVerify.setOnClickListener {
            if (isRegister) doVerify() else doVerifyForgotPassword()
        }
    }

    private fun doVerify() {
        registerVerificationVM.doVerify(
            VerifyRequest(
                email.toString(),
                otpNumber.toString()
            )
        )
    }

    private fun doVerifyForgotPassword() {
        registerVerificationVM.doVerifyForgotPassword(
            VerifyForgotPasswordRequest(
                email.toString(),
                otpNumber.toString()
            )
        )
    }

    private fun dataListener() {
        registerVerificationVM.verificationDataResponse.observe(this) {
            when(it.status) {
                Status.LOADING -> binding.btnVerify.showLoading()
                Status.ERROR -> {
                    binding.btnVerify.hideLoading()
                    Snackbar.make(binding.root, "Verify Not Valid", 2000)
                        .setBackgroundTint(Color.RED)
                        .show()
                }
                Status.SUCCESS -> {
                    registerVerificationVM.doLoginAfterVerify(
                        LoginRequest(
                            email.toString(),
                            password.toString()
                        )
                    )
                }
            }
        }
        registerVerificationVM.loginDataResponse.observe(this) { login ->
            if (login.status == Status.ERROR) {
                binding.btnVerify.hideLoading()
                Snackbar.make(binding.root, "Verify Not Valid", 2000)
                    .setBackgroundTint(Color.RED)
                    .show()
            }
            else if (login.status == Status.SUCCESS) {
                application?.applicationContext?.let { context ->
                    login.data?.let {
                        Config.setToken(context, it.token, it.userId)
                    }
                }
                startActivity(BaseRouter.goToSuccessPage(this, isRegister))
            }
        }
        registerVerificationVM.verifificationForgetPasswordResponse.observe(this) {
            when(it.status) {
                Status.LOADING -> binding.btnVerify.showLoading()
                Status.ERROR -> {
                    binding.btnVerify.hideLoading()
                    Snackbar.make(binding.root, "Verify Not Valid", 2000)
                        .setBackgroundTint(Color.RED)
                        .show()
                }
                Status.SUCCESS -> {
                    startActivity(BaseRouter.goToSuccessPage(this, isRegister))
                }
            }
        }
    }

}