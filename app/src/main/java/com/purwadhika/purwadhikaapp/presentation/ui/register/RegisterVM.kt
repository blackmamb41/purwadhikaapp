package com.purwadhika.purwadhikaapp.presentation.ui.register

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.purwadhika.purwadhikaapp.data.models.request.RegisterRequest
import com.purwadhika.purwadhikaapp.data.models.response.RegisterResponse
import com.purwadhika.purwadhikaapp.data.models.response.Status
import com.purwadhika.purwadhikaapp.domain.usecases.CreateNewAccountUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import javax.inject.Inject

@HiltViewModel
class RegisterVM @Inject constructor(var createNewAccountUseCase: CreateNewAccountUseCase): ViewModel() {
    var registerData = MutableLiveData<RegisterResponse>()
    fun doRegisterAccount(registerRequest: RegisterRequest) {
        registerData.postValue(
            RegisterResponse(
                Status.LOADING,
                null,
                null
            )
        )
        viewModelScope.launch(Dispatchers.IO) {
            createNewAccountUseCase.createAccount(registerRequest).enqueue(object: Callback<RegisterResponse> {
                override fun onResponse(
                    call: Call<RegisterResponse>,
                    response: Response<RegisterResponse>
                ) {
                    if (response.isSuccessful)
                        registerData.postValue(
                            RegisterResponse(
                                Status.SUCCESS,
                                response.body()?.message,
                                response.body()?.data
                            )
                        )
                    else
                        registerData.postValue(
                            RegisterResponse(
                                Status.ERROR,
                                response.body()?.message,
                                null
                            )
                        )
                }

                override fun onFailure(call: Call<RegisterResponse>, t: Throwable) {
                    registerData.postValue(
                        RegisterResponse(
                            Status.ERROR,
                            null,
                            null
                        )
                    )
                }

            })
        }
    }
}