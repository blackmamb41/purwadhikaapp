package com.purwadhika.purwadhikaapp.presentation.ui.register

import android.annotation.SuppressLint
import android.graphics.Color
import android.view.MenuItem
import android.view.View
import android.widget.TextView
import android.widget.Toast
import androidx.activity.viewModels
import com.github.florent37.singledateandtimepicker.dialog.SingleDateAndTimePickerDialog
import com.kizitonwose.calendarview.model.CalendarDay
import com.kizitonwose.calendarview.model.CalendarMonth
import com.kizitonwose.calendarview.model.DayOwner
import com.kizitonwose.calendarview.ui.DayBinder
import com.kizitonwose.calendarview.ui.MonthHeaderFooterBinder
import com.kizitonwose.calendarview.ui.ViewContainer
import com.purwadhika.purwadhikaapp.R
import com.purwadhika.purwadhikaapp.data.models.response.Status
import com.purwadhika.purwadhikaapp.databinding.ActivityBirthVerificationBinding
import com.purwadhika.purwadhikaapp.databinding.CalendarHeaderLayoutBinding
import com.purwadhika.purwadhikaapp.presentation.routers.BaseRouter
import com.purwadhika.purwadhikaapp.presentation.ui.base.BaseActivity
import com.purwadhika.purwadhikaapp.utils.Config
import com.purwadhika.purwadhikaapp.utils.Variables
import dagger.hilt.android.AndroidEntryPoint
import java.text.SimpleDateFormat
import java.time.LocalDate
import java.time.YearMonth
import java.time.temporal.WeekFields
import java.util.*

@AndroidEntryPoint
class BirthVerificationActivity : BaseActivity() {
    lateinit var binding: ActivityBirthVerificationBinding
    lateinit var calendar: Calendar
    lateinit var monthFormat: SimpleDateFormat
    lateinit var yearFormat: SimpleDateFormat
    private var month: String? = null
    private var year: String? = null
    private var selectedDate: LocalDate? = null
    private val birthVerificationVM by viewModels<BirthVerificationVM>()
    private var monthNumber: Int = 0

    override fun setupDepedencies() {
        binding = ActivityBirthVerificationBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setupToolbar()
        calendar = Calendar.getInstance()
        monthFormat = SimpleDateFormat("MMMM", Locale.getDefault())
        yearFormat = SimpleDateFormat("yyyy", Locale.getDefault())
        monthNumber = calendar.get(Calendar.MONTH)
        month = monthFormat.format(calendar.time)
        year = yearFormat.format(calendar.time)
        setupCalendar(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH) + 1)
    }

    override fun observe() {
        btnAction()
        dataListener()
        binding.calendarView.dayBinder = object: DayBinder<DayViewContainer> {
            override fun bind(container: DayViewContainer, day: CalendarDay) {
                container.day = day
                container.textView.text = day.date.dayOfMonth.toString()
                val textView = container.textView
                if(day.owner == DayOwner.THIS_MONTH) {
                    textView.visibility = View.VISIBLE
                    when(day.date) {
                        selectedDate -> {
                            textView.setTextColor(Color.WHITE)
                            textView.setBackgroundResource(R.drawable.green_circle)
                        }
                        else -> {
                            textView.setTextColor(Color.BLACK)
                            textView.setBackgroundColor(Color.WHITE)
                        }
                    }
                }
            }
            override fun create(view: View): DayViewContainer = DayViewContainer(view)
        }
        binding.calendarView.monthHeaderBinder = object: MonthHeaderFooterBinder<MonthViewContainer> {
            override fun bind(container: MonthViewContainer, month: CalendarMonth) {
                if(container.legendLayout.tag == null) container.legendLayout.tag = month.yearMonth
            }

            override fun create(view: View): MonthViewContainer = MonthViewContainer(view)
        }
        binding.btnBackToHome.setOnClickListener {
            startActivity(BaseRouter.gotToMainPage(this))
        }

    }

    private fun btnAction() {
        binding.btnPickMonth.text = month
        binding.btnPickMonth.setOnClickListener {
            SingleDateAndTimePickerDialog.Builder(this)
                .bottomSheet()
                .displayYears(false)
                .displayMonth(true)
                .displayDays(false)
                .displayHours(false)
                .displayMinutes(false)
                .displayAmPm(false)
                .listener {
                    month = Variables.MONTH[it.month + 1]
                    monthNumber = it.month + 1
                    binding.btnPickMonth.text = Variables.MONTH[it.month]
                    setupCalendar(convertLongToTime(it.time).toInt(), it.month + 1)
                }
                .dimBackground()
                .dimAmount(.25f)
                .title(getString(R.string.Month))
                .display()
        }
        binding.btnPickYear.text = year
        binding.btnPickYear.setOnClickListener {
            SingleDateAndTimePickerDialog.Builder(this)
                .bottomSheet()
                .displayYears(true)
                .displayMonth(false)
                .displayDays(false)
                .displayHours(false)
                .displayMinutes(false)
                .displayAmPm(false)
                .listener {
                    year = convertLongToTime(it.time)
                    binding.btnPickYear.text = convertLongToTime(it.time)
                    setupCalendar(convertLongToTime(it.time).toInt(), it.month + 1)
                }
                .dimBackground()
                .dimAmount(.25f)
                .title(getString(R.string.year))
                .display()
        }
        binding.btnContinue.setOnClickListener {
            if (selectedDate == null)
                Toast.makeText(this, "Date Not Selected", Toast.LENGTH_LONG).show()
            else
                Config.getIdUser(this).let { userId ->
                    val monthChoice: String = if (monthNumber < 10) {
                        "0$monthNumber"
                    }else{
                        monthNumber.toString()
                    }
                    val dayChoice: String = if (selectedDate?.dayOfMonth!! < 10) {
                        "0${selectedDate?.dayOfMonth}"
                    }else{
                        selectedDate?.dayOfMonth.toString()
                    }
                    val dateChoice = "$year-$monthChoice-$dayChoice"
                    birthVerificationVM.doUpdateDateBirth(userId, dateChoice)
                }
        }
    }

    override fun setupToolbar() {
        setSupportActionBar(binding.toolbarBirth)
        supportActionBar?.setDisplayShowTitleEnabled(false)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item.itemId) {
            android.R.id.home -> finish()
        }
        return super.onOptionsItemSelected(item)
    }

    @SuppressLint("SimpleDateFormat")
    private fun convertLongToTime(time: Long): String {
        val date = Date(time)
        val format = SimpleDateFormat("yyyy")
        return format.format(date)
    }

    inner class DayViewContainer(view: View): ViewContainer(view) {
        lateinit var day: CalendarDay
        val textView: TextView = view.findViewById(R.id.calendarDayText)
        init {
            view.setOnClickListener {
                if(day.owner == DayOwner.THIS_MONTH) {
                    selectDate(day.date)
                }
            }
        }
    }

    inner class MonthViewContainer(view: View): ViewContainer(view) {
        val legendLayout = CalendarHeaderLayoutBinding.bind(view).root
    }

    private fun selectDate(date: LocalDate) {
        if(selectedDate != date) {
            val oldDate = selectedDate
            selectedDate = date
            oldDate?.let {
                binding.calendarView.notifyDateChanged(it)
            }
            binding.calendarView.notifyDateChanged(date)
        }
    }

    private fun setupCalendar(year: Int, month: Int) {
        val currentMonth = YearMonth.of(year,month)
        val firstMonth = currentMonth.minusMonths(0)
        val lastMonth = currentMonth.plusMonths(0)
        val firstDayOfWeek = WeekFields.of(Locale.getDefault()).firstDayOfWeek
        binding.calendarView.setup(firstMonth, lastMonth, firstDayOfWeek)
    }

    private fun dataListener() {
        birthVerificationVM.responseUpdate.observe(this) {
            when(it.status) {
                Status.LOADING -> binding.btnContinue.showLoading()
                Status.SUCCESS -> startActivity(BaseRouter.goToMapLocation(this, true))
                Status.ERROR -> {
                    binding.btnContinue.hideLoading()
                    Toast.makeText(this, "Failed Update Birth Date", Toast.LENGTH_LONG).show()
                }
            }
        }
    }
}