package com.purwadhika.purwadhikaapp.presentation.ui.intake

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.purwadhika.purwadhikaapp.data.models.response.ProgramResponse
import com.purwadhika.purwadhikaapp.data.models.response.Status
import com.purwadhika.purwadhikaapp.domain.usecases.GetProgramByFilterUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import javax.inject.Inject

@HiltViewModel
class ProgramListScheduleVM @Inject constructor(var getProgramByFilterUseCase: GetProgramByFilterUseCase): ViewModel() {
    var programList = MutableLiveData<ProgramResponse>()
    fun getDataProgramByFilter(programCategoryId: Int, programTopicId: Int) {
        programList.postValue(
            ProgramResponse(
                Status.LOADING,
                null,
                null
            )
        )
        getProgramByFilterUseCase.getListProgramFilter(programCategoryId, programTopicId).enqueue(object: Callback<ProgramResponse> {
            override fun onResponse(
                call: Call<ProgramResponse>,
                response: Response<ProgramResponse>
            ) {
                if (response.isSuccessful)
                    programList.postValue(
                        ProgramResponse(
                            Status.SUCCESS,
                            response.body()?.message,
                            response.body()?.data
                        )
                    )
                else
                    programList.postValue(
                        ProgramResponse(
                            Status.ERROR,
                            response.body()?.message,
                            null
                        )
                    )
            }

            override fun onFailure(call: Call<ProgramResponse>, t: Throwable) {
                programList.postValue(
                    ProgramResponse(
                        Status.ERROR,
                        null,
                        null
                    )
                )
            }

        })
    }
}