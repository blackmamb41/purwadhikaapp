package com.purwadhika.purwadhikaapp.presentation.ui.register

import android.graphics.Color
import android.text.Editable
import android.text.method.HideReturnsTransformationMethod
import android.text.method.PasswordTransformationMethod
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.widget.EditText
import androidx.activity.viewModels
import com.purwadhika.purwadhikaapp.R
import com.purwadhika.purwadhikaapp.data.models.request.RegisterRequest
import com.purwadhika.purwadhikaapp.data.models.response.Status
import com.purwadhika.purwadhikaapp.databinding.ActivityRegisterPasswordBinding
import com.purwadhika.purwadhikaapp.presentation.routers.BaseRouter
import com.purwadhika.purwadhikaapp.presentation.ui.base.BaseActivity
import com.purwadhika.purwadhikaapp.utils.Variables
import dagger.hilt.android.AndroidEntryPoint
import kotlin.properties.Delegates

@AndroidEntryPoint
class RegisterPasswordActivity : BaseActivity() {
    lateinit var binding: ActivityRegisterPasswordBinding
    private lateinit var firstName: String
    private lateinit var lastName: String
    lateinit var email: String
    lateinit var phoneNumber: String
    private lateinit var tvPassword: EditText
    private lateinit var tvConfirmPassword: EditText
    private val minLength: Int = 6
    private var levelPasswordStrength = 0
    private var isRegister by Delegates.notNull<Boolean>()
    private val registerVM by viewModels<RegisterVM>()
    private var seePassword: Boolean = false

    override fun setupDepedencies() {
        binding = ActivityRegisterPasswordBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setupToolbar()
        firstName = intent.getStringExtra(Variables.FIRST_NAME).toString()
        lastName = intent.getStringExtra(Variables.LAST_NAME).toString()
        email = intent.getStringExtra(Variables.EMAIL).toString()
        phoneNumber = intent.getStringExtra(Variables.PHONE_NUMBER).toString()
        tvPassword = binding.passForm
        tvConfirmPassword = binding.passwordConfirmForm
        isRegister = intent.getBooleanExtra(Variables.REGISTER, false)
    }

    override fun observe() {
        dataListener()
        btnAction()
        tvPassword.addTextChangedListener(object: PasswordListener<EditText>(tvPassword) {
            override fun onTextChanged(target: EditText, s: Editable?) {
                val checkPassword = checkStrengthPassword(target.text.toString())
                levelPasswordStrength = checkPassword
                when (levelPasswordStrength) {
                    0 -> {
                        binding.textStrength.setText(R.string.weak)
                        binding.textStrength.setTextColor(Color.RED)
                        binding.lv1.progress = 0
                        binding.lv2.progress = 0
                        binding.lv3.progress = 0
                        binding.lv4.progress = 0
                    }
                    1 -> {
                        binding.textStrength.setText(R.string.weak)
                        binding.textStrength.setTextColor(Color.RED)
                        binding.lv1.progress = 100
                        binding.lv2.progress = 0
                        binding.lv3.progress = 0
                        binding.lv4.progress = 0
                    }
                    2 -> {
                        binding.textStrength.setText(R.string.medium)
                        binding.textStrength.setTextColor(Color.parseColor("#f58d42"))
                        binding.lv1.progress = 100
                        binding.lv2.progress = 100
                        binding.lv3.progress = 0
                        binding.lv4.progress = 0
                    }
                    3 -> {
                        binding.textStrength.setText(R.string.good)
                        binding.lv1.progress = 100
                        binding.lv2.progress = 100
                        binding.lv3.progress = 100
                        binding.lv4.progress = 0
                    }
                    4 -> {
                        binding.textStrength.setText(R.string.strong)
                        binding.lv1.progress = 100
                        binding.lv2.progress = 100
                        binding.lv3.progress = 100
                        binding.lv4.progress = 100
                    }
                }
            }
        })
    }

    private fun checkStrengthPassword(password: String): Int {
        var score = 0
        var upper = false
        var lower = false
        var digit = false
        var specialChar = false

        for (i in password.indices) {
            val c: Char = password[i]
            if (!specialChar && !Character.isLetterOrDigit(c)) {
                score++
                specialChar = true
            } else {
                if (!digit && Character.isDigit(c)) {
                    score++
                    digit = true
                } else {
                    if (!upper || !lower) {
                        if (Character.isUpperCase(c)) {
                            upper = true
                        } else {
                            lower = true
                        }
                        if (upper && lower) {
                            score++
                        }
                    }
                }
            }
        }

        val length: Int = password.length

        if(length > minLength) score++ else score = 0

        // return enum following the score

        return score
    }

    private fun btnAction() {
        binding.btnRegister.setOnClickListener {
             if(levelPasswordStrength < 2) {
                 binding.errorInput.visibility = View.VISIBLE
                 binding.errorInput.setText(R.string.password_not_requirement)
             }else if(tvPassword.text.toString() != tvConfirmPassword.text.toString()){
                     binding.errorInput.visibility = View.VISIBLE
                     binding.errorInput.setText(R.string.password_not_match)
                 } else {
                     if(isRegister)
                         registerVM.doRegisterAccount(
                             RegisterRequest(
                                 firstName = firstName,
                                 lastName = lastName,
                                 phoneNumber = phoneNumber,
                                 email = email,
                                 password = tvPassword.text.toString()
                             )
                         )
                     else
                         startActivity(BaseRouter.goToVerifyPage(this, email, tvPassword.text.toString(), isRegister))
             }
        }
        binding.inputPass.setEndIconOnClickListener {
            if(seePassword) {
                binding.passForm.transformationMethod = PasswordTransformationMethod.getInstance()
                seePassword = false
            } else {
                binding.passForm.transformationMethod =
                    HideReturnsTransformationMethod.getInstance()
                seePassword = true
            }
        }
    }

    private fun dataListener() {
        registerVM.registerData.observe(this) {
            when(it.status) {
                Status.LOADING -> binding.btnRegister.showLoading()
                Status.ERROR -> {
                    binding.btnRegister.hideLoading()
                    binding.errorInput.visibility = View.VISIBLE
                    binding.errorInput.text = "Register Failed, Something Error"
                }
                Status.SUCCESS -> {
                    startActivity(BaseRouter.goToVerifyPage(this, email, tvPassword.text.toString(), isRegister))
                }
            }
        }
    }

    override fun setupToolbar() {
        setSupportActionBar(binding.toolbarRegisterPass)
        supportActionBar?.setTitle(R.string.password)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item.itemId) {
            android.R.id.home -> finish()
        }
        return super.onOptionsItemSelected(item)
    }
}