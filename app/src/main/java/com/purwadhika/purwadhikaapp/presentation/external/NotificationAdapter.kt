package com.purwadhika.purwadhikaapp.presentation.external

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.lifecycle.MutableLiveData
import androidx.recyclerview.widget.RecyclerView
import com.purwadhika.purwadhikaapp.data.models.response.Notification
import com.purwadhika.purwadhikaapp.databinding.ListNotificationBinding

class NotificationAdapter(var notificationOnClickListener: NotificationOnClickListener): RecyclerView.Adapter<NotificationAdapter.NotificationViewHolder>() {
    var listNotification = listOf<Notification>()
    inner class NotificationViewHolder(var binding: ListNotificationBinding): RecyclerView.ViewHolder(binding.root) {
        fun bind(notification: Notification) {
            binding.titleNotification.text = notification.title
            itemView.setOnClickListener {
                notificationOnClickListener.onClickNotification()
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NotificationViewHolder {
        val binding = ListNotificationBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return NotificationViewHolder(binding)
    }

    override fun onBindViewHolder(holder: NotificationViewHolder, position: Int) {
        return holder.bind(listNotification[position])
    }

    override fun getItemCount(): Int = listNotification.size

    fun updateDataNotification(listNotif: List<Notification>) {
        listNotification = listNotif
    }
}