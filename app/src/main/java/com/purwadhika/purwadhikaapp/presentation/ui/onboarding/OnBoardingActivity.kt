package com.purwadhika.purwadhikaapp.presentation.ui.onboarding

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.purwadhika.purwadhikaapp.databinding.ActivityOnBoardingBinding
import com.purwadhika.purwadhikaapp.presentation.routers.BaseRouter

class OnBoardingActivity : AppCompatActivity() {
    lateinit var binding: ActivityOnBoardingBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityOnBoardingBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.btnSignIn.setOnClickListener {
            startActivity(BaseRouter.goToLoginPage(this))
        }

        binding.btnRegister.setOnClickListener {
            startActivity(BaseRouter.goToRegisterPage(this))
        }
    }
}