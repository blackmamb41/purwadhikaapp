package com.purwadhika.purwadhikaapp.presentation.ui.register

import android.Manifest
import android.annotation.SuppressLint
import android.app.AlertDialog
import android.content.Intent
import android.content.IntentSender
import android.content.pm.PackageManager
import android.location.Geocoder
import android.net.Uri
import android.provider.Settings
import android.util.Log
import android.view.MenuItem
import android.widget.Toast
import androidx.activity.viewModels
import androidx.core.app.ActivityCompat
import com.google.android.gms.common.api.ResolvableApiException
import com.google.android.gms.location.*
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.gms.tasks.Task
import com.purwadhika.purwadhikaapp.R
import com.purwadhika.purwadhikaapp.data.models.response.Status
import com.purwadhika.purwadhikaapp.databinding.ActivityMapSelectorBinding
import com.purwadhika.purwadhikaapp.presentation.routers.BaseRouter
import com.purwadhika.purwadhikaapp.presentation.ui.base.BaseActivity
import com.purwadhika.purwadhikaapp.utils.Config
import com.purwadhika.purwadhikaapp.utils.Variables
import dagger.hilt.android.AndroidEntryPoint
import java.util.*
import kotlin.properties.Delegates

@AndroidEntryPoint
class MapSelectorActivity : BaseActivity(), OnMapReadyCallback {
    lateinit var binding: ActivityMapSelectorBinding
    lateinit var fusedLocationProviderClient: FusedLocationProviderClient
    lateinit var locationCallback: LocationCallback
    private lateinit var gMap: GoogleMap
    lateinit var geocoder: Geocoder
    private var locationRequest: LocationRequest? = null
    private var latLng: LatLng = LatLng(-2.4153163,108.8499559)
    private var isMapReady: Boolean = false
    private var requestingLocation: Boolean = false
    private var address: String? = null
    private val mapSelectorVM by viewModels<MapSelectorVM>()
    var isRegister by Delegates.notNull<Boolean>()
    override fun setupDepedencies() {
        binding = ActivityMapSelectorBinding.inflate(layoutInflater)
        setContentView(binding.root)
        isRegister = intent.getBooleanExtra(Variables.REGISTER, false)
    }

    override fun observe() {
        initMap()
        responseListener()
        binding.btnLocationNow.setOnClickListener {
            gMap.clear()
            loadLastLocation()
        }
        binding.btnContinue.setOnClickListener {
            val userId = Config.getIdUser(this)
            address?.let { addressString ->
                mapSelectorVM.updateAddressLocation(userId,
                    addressString, latLng.latitude, latLng.longitude)
            }
        }
    }

    override fun setupToolbar() {
        setSupportActionBar(binding.toolbarMap)
        supportActionBar?.title = "Search Location"
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item.itemId) {
            android.R.id.home -> finish()
        }
        return super.onOptionsItemSelected(item)
    }

    private fun initMap() {
        val mapFragment = supportFragmentManager.findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)
        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this)
        locationRequest = LocationRequest.create().apply {
            interval = 1000
            fastestInterval = 5000
            priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        }
        geocoder = Geocoder(this, Locale.getDefault())

         val builder = locationRequest?.let {
             LocationSettingsRequest.Builder().addLocationRequest(it)
         }
        val client: SettingsClient = LocationServices.getSettingsClient(this)
        val task: Task<LocationSettingsResponse> = client.checkLocationSettings(builder?.build())

        task.addOnFailureListener {
            if (it is ResolvableApiException) {
                try {
                    it.startResolutionForResult(this, 20)
                }catch (x: IntentSender.SendIntentException) {
                    x.printStackTrace()
                }
            }
        }

        locationCallback = object: LocationCallback() {
            override fun onLocationResult(p0: LocationResult?) {
                super.onLocationResult(p0)
                if (p0 != null) {
                    for (location in p0.locations) {
                        latLng = LatLng(location.latitude, location.longitude)
                        if (isMapReady) updateMap()
                    }
                    requestingLocation = false
                }
            }
        }

        val permissionCoarseLocation = ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)
        val permissionFineLocation = ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
        if((permissionCoarseLocation != PackageManager.PERMISSION_GRANTED) && (permissionFineLocation != PackageManager.PERMISSION_GRANTED)) {
            ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION), Variables.LOCATION_REQUEST)
            return
        }

        loadLastLocation()
    }

    @SuppressLint("MissingPermission")
    private fun loadLastLocation() {
        fusedLocationProviderClient.lastLocation
            .addOnSuccessListener {
                Log.i("TAG", "loadLastLocation: $it")
                latLng = LatLng(it.latitude, it.longitude)
                if (isMapReady) updateMap()
                convertToAddress(it.latitude, it.longitude)
            }
    }

    @SuppressLint("MissingPermission")
    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when(requestCode) {
            Variables.LOCATION_REQUEST -> {
                if(grantResults.isNotEmpty() && grantResults.find { it == PackageManager.PERMISSION_DENIED } == null) {
                    loadLastLocation()
                }else{
                    val permissionDenied = AlertDialog.Builder(this)
                        .setTitle("Permission Required")
                        .setPositiveButton("Go To Permission Setting"
                        ) { dialog, which ->
                            val intentPermission =
                                Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
                            val uri: Uri = Uri.fromParts("package", packageName, null)
                            intentPermission.data = uri
                            dialog.dismiss()
                            startActivity(intentPermission)
                        }
                    permissionDenied.create().show()
                }
            }
        }
    }

    override fun onMapReady(p0: GoogleMap) {
        isMapReady = true
        gMap = p0
        updateMap()
        gMap.setOnMapClickListener {
            latLng = it
            gMap.clear()
            gMap.addMarker(MarkerOptions().position(it))
            gMap.animateCamera(CameraUpdateFactory.newLatLngZoom(it, 14F))
            convertToAddress(it.latitude, it.longitude)
        }
    }

    private fun updateMap() {
        gMap.addMarker(MarkerOptions().position(latLng))
        gMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 14F))
    }

    private fun convertToAddress(lat: Double, lon: Double) {
        val addresses = geocoder.getFromLocation(lat, lon, 1)
        address = addresses[0].getAddressLine(0)
        Log.i("TAG", "convertToAddress: $address")
        binding.textAddress.text = address
    }

    private fun responseListener() {
        mapSelectorVM.updateLocationResponse.observe(this){
            when(it.status) {
                Status.LOADING -> binding.btnContinue.showLoading()
                Status.ERROR -> {
                    binding.btnContinue.hideLoading()
                    Toast.makeText(this, "Failed Update", Toast.LENGTH_LONG).show()
                }
                Status.SUCCESS -> {
                    if (isRegister) {
                        binding.btnContinue.hideLoading()
                        startActivity(BaseRouter.goToAvatarUpload(this, isRegister))
                    }else{
                        binding.btnContinue.hideLoading()
                        finish()
                    }
                }
            }
        }
    }
}