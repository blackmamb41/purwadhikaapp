package com.purwadhika.purwadhikaapp.presentation.external

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.purwadhika.purwadhikaapp.data.models.response.ProgramTopicData
import com.purwadhika.purwadhikaapp.databinding.TopicListItemBinding

class ProgramTopicAdapter(var topicClickListener: TopicClickListener): RecyclerView.Adapter<ProgramTopicAdapter.ProgramTopicViewHolder>() {
    var listTopicProgram = listOf<ProgramTopicData>()
    inner class ProgramTopicViewHolder(var binding: TopicListItemBinding): RecyclerView.ViewHolder(binding.root) {
        fun bind(programTopicData: ProgramTopicData) {
            Glide.with(binding.root.context).load(programTopicData.icon).into(binding.iconTopic)
            binding.titleTopic.text = programTopicData.title
            itemView.setOnClickListener {
                topicClickListener.onTopicClick(programTopicData)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProgramTopicViewHolder {
        val binding = TopicListItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ProgramTopicViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ProgramTopicViewHolder, position: Int) {
        return holder.bind(listTopicProgram[position])
    }

    override fun getItemCount(): Int = listTopicProgram.size

    fun updateDataTopic(listTopic: List<ProgramTopicData>) {
        listTopicProgram = listTopic
        notifyDataSetChanged()
    }
}